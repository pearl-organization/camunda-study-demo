package com.pearl.study04.demo;

import com.pearl.study04.demo.common.R;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.StartFormData;
import org.camunda.bpm.engine.form.TaskFormData;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 */
@RestController
@RequestMapping("/process")
public class ProcessController {

    @Autowired
    FormService formService;

    @GetMapping("/getStartFormData")
    public R<List<FormField>> getStartFormData(String processDefinitionId) {
        StartFormData startFormData = formService.getStartFormData(processDefinitionId);
        List<FormField> fieldList = startFormData.getFormFields();
        return R.success(fieldList);
    }

    @GetMapping("/getRenderedStartForm")
    public R<Object> getRenderedStartForm(String processDefinitionId) {
        Object renderedStartForm = formService.getRenderedStartForm(processDefinitionId);
        return R.success(renderedStartForm);
    }

    @PostMapping("/getTaskFormData")
    public R<List<FormField>> getTaskFormData(String processDefinitionId) {
        TaskFormData taskFormData = formService.getTaskFormData(processDefinitionId);
        List<FormField> fieldList = taskFormData.getFormFields();
        return R.success(fieldList);
    }

    @PostMapping("/submitStartForm")
    public R<String> submitStartForm(String processDefinitionId, String businessKey, Map<String, Object> properties) {
        ProcessInstance processInstance = formService.submitStartForm(processDefinitionId, businessKey, properties);
        return R.success(processInstance.getProcessInstanceId());
    }

    @PostMapping("/submitTaskForm")
    public R<?> submitTaskForm(String taskId, Map<String, Object> properties) {
        formService.submitTaskForm(taskId, properties);
        // 最近几篇文档，我们了解了`Camunda`中的表单的相关知识，
        /**
         * 判断表单类型，0：表单设计器；1：自定义表单，
         * 如果是表单设计器则动态建表，如果是自定义表单则存储表单路径
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *
         *   不用给流程绑定表单，而是业务系统自己开发业务表单，启动流程时，传递
         *
         *
         *         2.2.2 自定义业务Form
         *         这种方式应该是大家用的最多的了，因为一般的业务系统业务逻辑都会比较复杂，而且数据库中很多表都会有依赖关系，表单中有很多状态判断。
         *
         *         例如我们的系统适用jQuery UI作为UI，有很多javascript代码，页面的很多操作需要特殊处理（例如：多个选项的互斥、每个节点根据类型和操作人显示不同的按钮）；
         *         基本每个公司都有一套自己的UI风格，要保持多个系统的操作习惯一致只能使用自定义表单才能满足。
         *
         *         ![在这里插入图片描述](https://img-blog.csdnimg.cn/ce771785d06a42359608ffa16007f8ca.png)

         */

        return R.success();
    }

}
