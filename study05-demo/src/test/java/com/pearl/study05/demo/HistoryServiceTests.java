package com.pearl.study05.demo;

import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.history.*;
import org.camunda.bpm.engine.repository.DiagramElement;
import org.camunda.bpm.engine.repository.DiagramLayout;
import org.camunda.bpm.engine.runtime.Job;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author TD
 * @version 1.0
 * @date 2023/9/6
 */
@SpringBootTest
public class HistoryServiceTests {

    @Autowired
    HistoryService historyService;

    @Test
    @DisplayName("查询流程实例历史数据")
    void createHistoricProcessInstanceQuery() {
        // 构建查询对象，并执行列表查询
        //
        List<HistoricProcessInstance> instanceList = historyService.createHistoricProcessInstanceQuery()
                //.processInstanceBusinessKey(businessKey) // 业务KEY
                //.processInstanceId(processInstanceId) // 流程实例ID
                .finished() // 已完成的（默认也会查询正在运行的实例）
                .startedBy("admin") // 流程发起人
                .list();
        for (HistoricProcessInstance historicProcessInstance : instanceList) {
            System.out.println("ID："+historicProcessInstance.getId());
            System.out.println("开始时间："+historicProcessInstance.getStartTime());
            System.out.println("结束时间："+historicProcessInstance.getEndTime());
        }
    }

    @Test
    @DisplayName("查询历史活动节点")
    void createHistoricActivityInstanceQuery() {
        // 已完成
        String processInstanceId = "3347ae41-4b95-11ee-a21b-00ff045b1bb6";
        List<HistoricActivityInstance> historicActivities = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(processInstanceId) // 流程实例ID
                .orderByHistoricActivityInstanceEndTime().asc() // 完成时间升序
                .finished() // 查询已完成的活动实例
                .list();
        for (HistoricActivityInstance historicActivity : historicActivities) {
            System.out.println("活动节点ID：" + historicActivity.getActivityId());
            System.out.println("活动节点名称：" + historicActivity.getActivityName());
        }
    }


    @Test
    @DisplayName("查询历史任务")
    void createHistoricTaskInstanceQuery() {
        String processInstanceId = "3347ae41-4b95-11ee-a21b-00ff045b1bb6";
        List<HistoricTaskInstance> taskInstanceList = historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(processInstanceId) // 流程实例ID
                .finished()  // 已完成的
                .list();
    }

    @Test
    @DisplayName("查询历史变量")
    void createHistoricDetailQuery() {
        // 查询历史变量详情
        String processInstanceId = "3347ae41-4b95-11ee-a21b-00ff045b1bb6";
        List<HistoricDetail> details = historyService.createHistoricDetailQuery()
                .processInstanceId(processInstanceId) // 流程实例ID
                .list();
        // 历史变量
        List<HistoricVariableInstance> list = historyService.createHistoricVariableInstanceQuery()
                .processInstanceId(processInstanceId)
                .list();
    }

    @Test
    @DisplayName("查询历史变量")
    void cleanUpHistoryAsync() {
        Job job = historyService.cleanUpHistoryAsync();
    }


}
