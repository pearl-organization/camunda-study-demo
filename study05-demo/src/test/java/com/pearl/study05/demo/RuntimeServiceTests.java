package com.pearl.study05.demo;

import org.camunda.bpm.application.ProcessApplicationContext;
import org.camunda.bpm.engine.ManagementService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.IdentityLink;
import org.camunda.bpm.engine.task.Task;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author TD
 * @version 1.0
 * @date 2023/9/6
 */
@SpringBootTest
public class RuntimeServiceTests {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    ManagementService managementService;


    @Test
    @DisplayName("查询执行对象")
    void createExecutionQuery() {
        String executionId = "1698932745109102592";
        // 普通查询
        runtimeService.createExecutionQuery()
                .executionId(executionId) // 执行ID
                .active();// 激活状态
        // 原生查询
        String sql = "SELECT * FROM " + managementService.getTableName(Execution.class) +
                " E WHERE E.ID_ ='" + executionId + "'";
        List<Execution> executionList = runtimeService.createNativeExecutionQuery().sql(sql).list();

    }

    @Test
    @DisplayName("根据ID启动流程实例")
    void startProcessInstanceById() {
        String processDefinitionId = "study01-demo-process:1:8c99165f-934a-11ee-a12d-00ff045b1bb6";
        String biz="biz-ssssssssss";
        Map<String, Object> variables = new HashMap<>();
        variables.put("name", "wangwu");
        variables.put("day", 3);

        ProcessInstance processInstance = runtimeService.startProcessInstanceById(processDefinitionId, biz,variables);
        System.out.println("流程实例ID：" + processInstance.getId());
    }

    @Test
    @DisplayName("查询流程实例")
    void createProcessInstanceQuery() {
        String processInstanceId = "1698932745109102592";
        // 普通查询
        runtimeService.createProcessInstanceQuery()
                .processInstanceId(processInstanceId) // ID
                //.processInstanceIds() // ID集合
                .orderByProcessInstanceId().desc() // 排序
                .active();// 激活状态
        // 原生查询
        String sql = "SELECT * FROM " + managementService.getTableName(ProcessInstance.class) +
                " WHERE ID_ ='" + processInstanceId + "'";
        List<ProcessInstance> processInstances = runtimeService.createNativeProcessInstanceQuery().sql(sql).list();
    }


}
