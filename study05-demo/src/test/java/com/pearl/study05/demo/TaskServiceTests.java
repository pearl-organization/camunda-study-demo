package com.pearl.study05.demo;

import cn.hutool.core.collection.CollUtil;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.task.IdentityLink;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/9/6
 */
@SpringBootTest
public class TaskServiceTests {

    @Autowired
    TaskService taskService;

    @Test
    @DisplayName("加签")
    void delegateTask() {
        String taskId = "1699246602574639105";
        // 委派任务
        taskService.delegateTask(taskId, "zhangsan");
        // 解析任务
        taskService.resolveTask(taskId);
    }

    @Test
    @DisplayName("查询我的待办")
    void createMyTaskQuery() {
        TaskQuery taskQuery = taskService.createTaskQuery().taskAssignee("admin");
        List<Task> taskList = taskQuery.list();
        if (CollUtil.isNotEmpty(taskList)){
            for (Task task : taskList) {
                System.out.println(task.getId());
                System.out.println(task.getName());
                System.out.println(task.getAssignee());
            }

        }
    }

    @Test
    @DisplayName("查询任务关联的参与者信息")
    void getIdentityLinksForTask() {
        String taskId = "1699318112458911746";
        List<IdentityLink> identityLinksForTask = taskService.getIdentityLinksForTask(taskId);
        for (IdentityLink identityLink : identityLinksForTask) {
            System.out.println("类型：" + identityLink.getType());
            System.out.println("候选用户：" + identityLink.getUserId());
            System.out.println("候选用户组：" + identityLink.getGroupId());
        }
    }

    @Test
    @DisplayName("查询候选任务")
    void createTaskQuery() {
        String processInstanceId = "1699318112454717440"; // 流程实例ID
        String candidateUser = "zhangsan";// 候选用户
        List<Task> taskList = taskService.createTaskQuery().processInstanceId(processInstanceId)
                .taskCandidateUser(candidateUser)
                .taskCandidateGroup("") // 候选用户组
                .list();
        for (Task task : taskList) {
            System.out.println("任务ID：" + task.getId());
            System.out.println("任务名称：" + task.getName());
        }
    }


    @Test
    @DisplayName("拾取任务")
    void claim() {
        // 查询当前任务是否存在
        String taskId = "1699318112458911746";
        Task task= taskService.createTaskQuery().taskId(taskId).singleResult();
        if (task != null) {
            // 拾取任务
            taskService.claim(taskId, "zhangsan");
        }
    }
}
