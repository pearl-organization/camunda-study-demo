package com.pearl.study05.demo;

import cn.hutool.core.collection.CollUtil;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/11/30
 */
@SpringBootTest
public class ProcessDefinitionTests {

    @Autowired
    RepositoryService repositoryService;

    @Test
    @DisplayName("查询")
    void select() {
        // 1. 最后一个版本
/*        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                .latestVersion();
*/
        // 2. 名称
 /*         ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                //.processDefinitionNameLike("%流程%") // 模糊
                .processDefinitionName("流程"); // 精确
*/
        // 3. 激活、挂起
/*        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                //.suspended() // 挂起
                .active(); // 激活
*/
        // 4. 发布日期
/*        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                //.deployedAt(new Date()) // 指定日期发布的
                .deployedAfter(DateUtil.beginOfWeek(new Date())); // 指定日期之后发布的
*/
        // 5. 部署ID
/*
        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                .deploymentId("17544731-8f32-11ee-83b2-00ff045b1bb6");
*/
        // 6. 突发事件（例如作业、外部任务被卡住）
/*        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                //.incidentType() // 事件类型
                //.incidentMessageLike() // 事件信息模糊
                //.incidentMessage() // 事件信息精确
                .incidentId("123456"); //  事件ID*/
        // 7. 租户
/*        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                // .tenantIdIn() // 指定多个租户ID
                // .withoutTenantId() // 只包含没有租户ID（可用于只查询所有租户共享的）
                .includeProcessDefinitionsWithoutTenantId(); // 包含没有租户ID*/
        // 8. 版本标签
/*        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                //.versionTag() // 精确
                // .versionTagLike() // 模糊
                .withoutVersionTag(); // 只包含没有版本标签的*/
        // 9. 类别
/*        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                //.processDefinitionCategoryLike() // 模糊
                .processDefinitionCategory("http://activiti.org/bpmn"); // 精确*/
        // 10. 流程定义ID
/*        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                // .processDefinitionIdIn() // 多个ID
                .processDefinitionId("oneTaskProcess:1:17666fa3-8f32-11ee-83b2-00ff045b1bb6"); // 单个ID*/
        // 11. 流程定义Key
/*        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                //.processDefinitionKeyLike() // 模糊
                //.processDefinitionKeysIn() // 多个KEY
                .processDefinitionKey("oneTaskProcess"); // 单个KEY*/
        // 12. 资源名称
/*        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                //.processDefinitionResourceNameLike() // 模糊
                .processDefinitionResourceName("%oneTaskProcess.bpmn%"); // 精确*/
        // 13. 启动相关
/*        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                // .startableByUser("admin"); // 传入用户ID或用户分组ID（camunda中的用户），查询可以启动的流程定义
                .startableInTasklist(); // 可启动的
                //.startablePermissionCheck(); // 可启动权限检查
                //.notStartableInTasklist(); // 不可启动的*/
        // 14. 消息启动流程
/*        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                .messageEventSubscriptionName("");*/
        // 15. 排序
        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery()
                // .orderByProcessDefinitionId().asc() // 流程定义ID
                // .orderByProcessDefinitionName().asc() // 名称
                // .orderByProcessDefinitionCategory().asc() //类别
                // .orderByProcessDefinitionKey().asc() //KEY
                // .orderByDeploymentTime().asc() //  部署时间
                // .orderByTenantId().asc() // 租户ID
                // .orderByVersionTag().asc() //  版本标记
                // .orderByProcessDefinitionVersion().asc() // 版本
                .orderByDeploymentId() // 部署ID
                .desc();  // 降序
                 // .orderByDeploymentId() // 部署ID
                 // .asc();  // 升序
        // 执行列表查询
        List<ProcessDefinition> processDefinitionList = query.list();
        if (CollUtil.isNotEmpty(processDefinitionList)) {
            for (ProcessDefinition processDefinition : processDefinitionList) {
                ProcessDefinitionEntity entity = (ProcessDefinitionEntity) processDefinition; // 实例为：ProcessDefinitionEntity

                System.out.println("ID：" + entity.getId());
                System.out.println("KEY：" + entity.getKey());
                System.out.println("乐观锁版本：" + entity.getRevision());
                System.out.println("流程模型版本：" + entity.getVersion());
                System.out.println("类别：" + entity.getCategory());
                System.out.println("部署ID：" + entity.getDeploymentId());
                System.out.println("资源名称:" + entity.getResourceName());
                System.out.println("历史记录保存时间（单位天）：" + entity.getHistoryTimeToLive());
                System.out.println("历史记录级别：" + entity.getHistoryLevel()); //  none、activity、audit、full（默认），级别越高，历史数据越详细
                System.out.println("是否存在开始表单：" + entity.getHasStartFormKey());
                System.out.println("挂起状态：" + entity.getSuspensionState()); // 1 激活 2 挂起
                System.out.println("租户ID：" + entity.getTenantId());
                System.out.println("是否可启动的：" + entity.isStartableInTasklist()); // 0 不可 1可
                System.out.println("名称：" + processDefinition.getName());
                System.out.println("描述：" + processDefinition.getDescription());
                System.out.println("版本标记：" + processDefinition.getVersionTag());
                System.out.println("流程模型图名称：：" + processDefinition.getDiagramResourceName());
                System.out.println("---------------------------------------------------------------");
            }
        }
    }
}
