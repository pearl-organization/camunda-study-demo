package com.pearl.study05.demo;

import org.camunda.bpm.engine.ManagementService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.management.JobDefinition;
import org.camunda.bpm.engine.task.IdentityLink;
import org.camunda.bpm.engine.task.Task;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/9/6
 */
@SpringBootTest
public class ManagementServiceTests {

    @Autowired
    ManagementService managementService;

    @Test
    void createJobDefinitionQuery() {
        // 查询
        JobDefinition jobDefinition = managementService
                .createJobDefinitionQuery()
                .activityIdIn("ServiceTask_1")
                .singleResult();
        // 设置覆盖优先级
        managementService.setOverridingJobPriorityForJobDefinition(jobDefinition.getId(), 0L,true);

        // 清除覆盖
        managementService.clearOverridingJobPriorityForJobDefinition(jobDefinition.getId());
    }
}
