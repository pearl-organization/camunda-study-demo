package com.pearl.study05.demo;

import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.history.HistoricActivityInstance;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.runtime.ActivityInstance;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceQuery;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
class Study05DemoApplicationTests {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    HistoryService historyService;

    @Test
    @DisplayName("启动流程：会签")
    void multipleUserTasks () {
        // 会签人集合
        List<String> assigneeList=new ArrayList<>();
        assigneeList.add("zhangsan");
        assigneeList.add("lisi");
        assigneeList.add("wangwu");
        Map<String, Object> variables = new HashMap<>();
        variables.put("assigneeList", assigneeList);
        // 启动流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("Process_09hbi8i", variables);
        System.out.println("流程实例ID：" + processInstance.getId());
    }


    @Test
    @DisplayName("回退到指定节点")
    void createProcessInstanceModification() {
        String processInstanceId = "2c319c66-4b8b-11ee-b6ee-00ff045b1bb6";
        // 回退
/*        runtimeService.createProcessInstanceModification(processInstanceId)
                .cancelAllForActivity("Activity_03g17n4") // 取消的活动实例
                .startBeforeActivity("Activity_0nc4bs3") // 当前要回到的活动实例
                .setVariable("var","value")// 可以设置变量
                .execute();*/
        // 子流程
        runtimeService.createProcessInstanceModification(processInstanceId)
                .cancelAllForActivity("Activity_0tsc67p")
                .startBeforeActivity("Activity_0ychi8d")
                .startBeforeActivity("Activity_0jhbij3")
                .execute();
/*        runtimeService.createProcessInstanceModification(processInstanceId)
                .cancelAllForActivity("Activity_1p3ilsx#multiInstanceBody")
                .startBeforeActivity("Activity_1p3ilsx")
                .setVariableLocal("assignee", "kunji")
                .execute();*/
        // 同时修改多个
/*        ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
        runtimeService.createModification("exampleProcessDefinitionId")
                .cancelAllForActivity("exampleActivityId:1")
                .startBeforeActivity("exampleActivityId:2")
                .processInstanceIds("processInstanceId:1", "processInstanceId:2")
                .executeAsync();*/
    }

    @Test
    @DisplayName("启动流程")
    void startProcessInstanceById() {
        ProcessInstance processInstance = runtimeService.startProcessInstanceById("form_leave:2:14d0b883-46fc-11ee-94a6-00ff045b1bb6");
        System.out.println("流程实例ID：" + processInstance.getId());
    }

    @Test
    @DisplayName("查询活动实例树")
    void getActivityInstance() {
        String processInstanceId = "2c319c66-4b8b-11ee-b6ee-00ff045b1bb6";
        ActivityInstance activityInstance = runtimeService.getActivityInstance(processInstanceId);
    }


    @Test
    @DisplayName("查询流程活动节点")
    void createHistoricActivityInstanceQuery() {
        // 已完成
        String processInstanceId = "3347ae41-4b95-11ee-a21b-00ff045b1bb6";
        List<HistoricActivityInstance> historicActivities = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(processInstanceId) // 流程实例ID
                .orderByHistoricActivityInstanceEndTime().asc() // 完成事件升序
                .finished() // 查询已完成的活动实例
                .list();
        for (HistoricActivityInstance historicActivity : historicActivities) {
            System.out.println("活动节点ID：" + historicActivity.getActivityId());
            System.out.println("活动节点名称：" + historicActivity.getActivityName());
        }
        System.out.println("-----------------------");

        // 未完成
        List<HistoricActivityInstance> historicActivitiesUnfinished = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(processInstanceId)
                .orderByHistoricActivityInstanceEndTime().asc()
                .unfinished()
                .list();
        for (HistoricActivityInstance historicActivity : historicActivitiesUnfinished) {
            System.out.println("活动节点ID：" + historicActivity.getActivityId());
            System.out.println("活动节点名称：" + historicActivity.getActivityName());
        }
    }


    @Autowired
    RepositoryService repositoryService;

    @Test
    @DisplayName("查询流程定义")
    void createProcessDefinitionQuery() {
        // 激活状态
        List<ProcessDefinition> activeDefinitionList = repositoryService.createProcessDefinitionQuery()
                .active()
                .list();
        for (ProcessDefinition processDefinition : activeDefinitionList) {
            System.out.println("ID:" + processDefinition.getId());
            System.out.println("Key:" + processDefinition.getKey());
            System.out.println("Name:" + processDefinition.getName());
            System.out.println("isSuspended：" + processDefinition.isSuspended()); // 是否被挂起
        }
        System.out.println("-----------------------");
        // 挂起状态
        List<ProcessDefinition> suspendedDefinitionList = repositoryService.createProcessDefinitionQuery()
                .suspended()
                .list();
        for (ProcessDefinition processDefinition : suspendedDefinitionList) {
            System.out.println("ID:" + processDefinition.getId());
            System.out.println("Key:" + processDefinition.getKey());
            System.out.println("Name:" + processDefinition.getName());
            System.out.println("isSuspended：" + processDefinition.isSuspended()); // 是否被挂起
        }
    }

    @Test
    @DisplayName("使用ID挂起流程定义")
    void suspendProcessDefinitionById() {
        // 使用ID挂起流程定义，如果流程定义处于挂起状态，则无法根据流程定义启动新的流程实例。
        String processDefinitionId = "form_leave:2:14d0b883-46fc-11ee-94a6-00ff045b1bb6"; // 流程定义ID
        repositoryService.suspendProcessDefinitionById(processDefinitionId);

        boolean suspendProcessInstances = true;// 是否将当前定义下的的所有流程实例也被挂起
        Date suspensionDate = new Date();//  流程定义将被挂起的日期。如果为null，则进程定义将立即挂起。注意：作业执行器需要处于活动状态才能使用此项！
        repositoryService.suspendProcessDefinitionById(processDefinitionId,suspendProcessInstances,suspensionDate);
    }

    @Test
    @DisplayName("使用Key挂起流程定义")
    void suspendProcessDefinitionByKey() {
        String processDefinitionKey = "form_leave"; // 流程定义Key
        repositoryService.suspendProcessDefinitionByKey(processDefinitionKey);

        // void suspendProcessDefinitionByKey(String processDefinitionKey, boolean suspendProcessInstances, Date suspensionDate);
    }

    @Test
    @DisplayName("使用ID激活流程定义")
    void activateProcessDefinitionById() {
        String processDefinitionId = "form_leave:2:14d0b883-46fc-11ee-94a6-00ff045b1bb6"; // 流程定义ID
        repositoryService.activateProcessDefinitionById(processDefinitionId);

        boolean activateProcessInstances = true;// 是否将当前定义下的的所有流程实例也激活
        Date activationDate = new Date();//  流程定义将被激活的日期。如果为null，则进程定义将立即激活。注意：作业执行器需要处于活动状态才能使用此项！
        repositoryService.activateProcessDefinitionById(processDefinitionId,activateProcessInstances,activationDate);
    }

    @Test
    @DisplayName("根据ID挂起流程实例")
    void suspendProcessInstanceById() {
        runtimeService.suspendProcessInstanceById("2b1f809b-47a3-11ee-87d5-00ff045b1bb6");
    }

    @Test
    @DisplayName("激活流程实例")
    void activateProcessInstanceById() {
        runtimeService.activateProcessInstanceById("2b1f809b-47a3-11ee-87d5-00ff045b1bb6");
    }

    @Test
    @DisplayName("发送消息")
    void startProcessInstanceByMessage() {
        runtimeService.createMessageCorrelation("Message_16412fd") // 消息名称
                .correlate();
        //
    }



    @Test
    @DisplayName("删除流程")
    void deleteProcessInstance() {
        String processInstanceId = "7027662b-4b93-11ee-b6ee-00ff045b1bb6";
        runtimeService.deleteProcessInstance(processInstanceId, "撤销");
    }

    @Test
    @DisplayName("重启流程")
    void restartProcessInstances() {
        String processDefinitionId = "Process_0ob8qle:1:5b7935aa-4b93-11ee-b6ee-00ff045b1bb6"; // 流程定义ID
        String processInstanceId = "7027662b-4b93-11ee-b6ee-00ff045b1bb6"; // 流程实例ID
        runtimeService.restartProcessInstances(processDefinitionId)
                .processInstanceIds(processInstanceId)
                .startBeforeActivity("Activity_0mqdgzc") //启动节点
                .skipCustomListeners() // 跳过监听器
                .skipIoMappings() // 跳过输入输出映射
                .initialSetOfVariables() // 使用初始变量集，默认是最后一组变量
                .withoutBusinessKey() // 忽略历史流程实例的 Business Key
                .execute(); // 同步执行
                //.executeAsync() // 异步批量执行
    }


}
