package com.pearl.study05.demo;

import cn.hutool.core.collection.CollUtil;
import com.pearl.study05.demo.validation.ExclusiveGatewayValidator;
import com.pearl.study05.demo.validation.MultiInstanceLoopCharacteristicsValidator;
import com.pearl.study05.demo.validation.ProcessValidator;
import com.pearl.study05.demo.validation.UserTaskValidator;
import org.camunda.bpm.engine.Problem;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.impl.persistence.entity.ExecutionEntity;
import org.camunda.bpm.engine.impl.xml.ProblemImpl;
import org.camunda.bpm.engine.repository.DiagramElement;
import org.camunda.bpm.engine.repository.DiagramLayout;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.builder.ProcessBuilder;
import org.camunda.bpm.model.bpmn.instance.*;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.camunda.bpm.model.bpmn.instance.bpmndi.BpmnDiagram;
import org.camunda.bpm.model.bpmn.instance.bpmndi.BpmnPlane;
import org.camunda.bpm.model.bpmn.instance.bpmndi.BpmnShape;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaProperties;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaProperty;
import org.camunda.bpm.model.bpmn.instance.dc.Bounds;
import org.camunda.bpm.model.bpmn.instance.di.Shape;
import org.camunda.bpm.model.xml.Model;
import org.camunda.bpm.model.xml.ModelInstance;
import org.camunda.bpm.model.xml.instance.DomDocument;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import org.camunda.bpm.model.xml.type.ModelElementType;
import org.camunda.bpm.model.xml.validation.ModelElementValidator;
import org.camunda.bpm.model.xml.validation.ValidationResult;
import org.camunda.bpm.model.xml.validation.ValidationResults;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.transform.dom.DOMSource;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * @author TD
 * @version 1.0
 * @date 2023/9/6
 */
@SpringBootTest
public class RepositoryServiceTests {

    @Autowired
    RepositoryService repositoryService;

    @Test
    @DisplayName("部署")
    void deploy() {
        repositoryService.createDeployment()
                .name("测试部署")
                .addClasspathResource("bpmn/leave.bpmn")
                .addClasspathResource("bpmn/leave.png")
                .deploy();
    }

    @Test
    void getBpmnModelInstance() {
        String processDefinitionId = "Process_06a9asp:5:1699384975918383104"; // 流程定义ID
        BpmnModelInstance modelInstance = repositoryService.getBpmnModelInstance(processDefinitionId);
        Process process = (Process) modelInstance.getDefinitions().getUniqueChildElementByType(Process.class);
    }

    @Test
    void getProcessDiagramLayout() {
        String processDefinitionId = "Process_06a9asp:5:1699384975918383104"; // 流程定义ID
        DiagramLayout processDiagramLayout = repositoryService.getProcessDiagramLayout(processDefinitionId);
        Map<String, DiagramElement> elements = processDiagramLayout.getElements();
        Set<Map.Entry<String, DiagramElement>> entries = elements.entrySet();
        for (Map.Entry<String, DiagramElement> entry : entries) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }
    }

    //
    @Test
    void readModel() {
        // 从文件中读取一个模型
        BpmnModelInstance bpmnModelInstance = Bpmn.readModelFromFile(
                new File("E:\\TD\\project\\study\\camunda-study-demo\\camunda-study01-demo\\src\\main\\resources\\diagra我m_1.bpmn"));

        // 获取 java.xml.org.w3c.dom.Document 对象（java.xml：JDK中的XML工具包），可用于解析XML所有内容
        DomDocument document = bpmnModelInstance.getDocument();
        String nodeName = document.getDomSource().getNode().getChildNodes().item(0).getNodeName();  // 获取第一个节点的名称：bpmn:definitions

        //  获取Bpmn模型根元素（bpmn:definitions 标签）
        Definitions definitions = bpmnModelInstance.getDefinitions();
        String id = definitions.getId(); // ID
        String exporter = definitions.getExporter(); // 设计器

        // 根据ID获取模型元素
        ModelElementInstance process = bpmnModelInstance.getModelElementById("Process_01ra0f4"); // Process_01ra0f4=》bpmn:process 标签内的 ID
        String typeName = process.getElementType().getTypeName(); // 类型=》 process
        String isExecutable = process.getAttributeValue("isExecutable"); // 获取属性

        // 根据类型获取模型元素（这里是获取所有的用户任务）
        Collection<UserTask> userTasks = bpmnModelInstance.getModelElementsByType(UserTask.class);
        if (CollUtil.isNotEmpty(userTasks)) {
            for (UserTask userTask : userTasks) {
                String name = userTask.getName(); // 名称
                userTask.setName("修改名称"); // 可以直接修改模型
                String camundaDueDate = userTask.getCamundaDueDate(); // 到期时间
                String camundaAssignee = userTask.getCamundaAssignee(); // 审批人
            }
        }

        // 获取所有序列流
        Collection<SequenceFlow> sequenceFlows = bpmnModelInstance.getModelElementsByType(SequenceFlow.class);
        if (CollUtil.isNotEmpty(sequenceFlows)) {
            for (SequenceFlow sequenceFlow : sequenceFlows) {
                FlowNode source = sequenceFlow.getSource(); // 开始
                // sequenceFlow.setSource();
                // sequenceFlow.setName("XXX");
                FlowNode target = sequenceFlow.getTarget(); // 结束
                // sequenceFlow.setTarget();
            }
        }
    }

    @Test
    public void createModelByEmpty() throws IOException {
        // 创建空的BPMN模型对象
        BpmnModelInstance modelInstance = Bpmn.createEmptyModel();

        // 创建BPMN的定义元素（bpmn:definitions）
        Definitions definitions = modelInstance.newInstance(Definitions.class);
        definitions.setTargetNamespace("http://www.omg.org/spec/BPMN/20100524/MODEL"); // 设置 targetNamespace
        definitions.getDomElement().registerNamespace("camunda", "http://camunda.org/schema/1.0/bpmn"); // 设置自定义命名空间
        modelInstance.setDefinitions(definitions); // 添加到模型对象中

        // 创建BPMN的流程元素（bpmn:process）
        Process process = modelInstance.newInstance(Process.class);
        process.setId("process_id"); // 设置ID
        definitions.addChildElement(process); // 添加到定义元素中

        // 创建开始事件，用户任务，结束事件
        StartEvent startEvent = createElement(process, "start_id", StartEvent.class);
        UserTask task = createElement(process, "task_id", UserTask.class);
        task.setName("经理审批");
        EndEvent endEvent = createElement(process, "end_id", EndEvent.class);

        // 创建各个元素之间的序列流（连线）
        createSequenceFlow(process, startEvent, task);
        createSequenceFlow(process, task, endEvent);

        // 创建图表： <bpmndi:BPMNDiagram>...... </bpmndi:BPMNDiagram>
        BpmnDiagram bpmnDiagram = modelInstance.newInstance(BpmnDiagram.class);

        // 创建画板： <bpmndi:BPMNPlane>...... </bpmndi:BPMNPlane>
        BpmnPlane bpmnPlane = modelInstance.newInstance(BpmnPlane.class);
        bpmnPlane.setBpmnElement(process);
        bpmnDiagram.addChildElement(bpmnPlane);
        definitions.addChildElement(bpmnDiagram);

        // 设置元素的位置（这里只设置了开始节点，简单但是比较繁琐，就不演示了）
        Shape shape = modelInstance.newInstance(BpmnShape.class);
        shape.setId("shape_start_id");
        shape.setAttributeValue("bpmnElement", "start_id");
        Bounds bounds = modelInstance.newInstance(Bounds.class);
        bounds.setX(179);
        bounds.setY(242);
        bounds.setHeight(36);
        bounds.setWidth(36);
        shape.setBounds(bounds);
        bpmnPlane.addChildElement(shape);

        // 验证模型
        Bpmn.validateModel(modelInstance);

        // 转为字符串
        String xmlString = Bpmn.convertToString(modelInstance);

        // 写入到输出流
        //OutputStream outputStream = new OutputStream(...);
        //Bpmn.writeModelToStream(outputStream, modelInstance);

        // 写入到文件中
        File file = File.createTempFile("custom_", ".bpmn");
        Bpmn.writeModelToFile(file, modelInstance);
    }

    @Test
    public void createModelByFluent() {
        // Fluent 风格
        BpmnModelInstance modelInstance = Bpmn.createExecutableProcess()
                .startEvent()
                .userTask().name("经理审批")
                .endEvent()
                .done();
        Bpmn.validateModel(modelInstance);
        String xmlString = Bpmn.convertToString(modelInstance);
    }

    @Test
    public void validateModelTest() {
        // 创建模型
/*        BpmnModelInstance bpmnModelInstance = Bpmn.createExecutableProcess()
                .name("测试流程")
                .startEvent()
                .userTask().name("经理审批")
                .done();*/

        BpmnModelInstance bpmnModelInstance = Bpmn.readModelFromFile(new File("D:\\diagram_1.bpmn"));

        // XML简单校验
        Bpmn.validateModel(bpmnModelInstance);

        // 自定义校验器校验
        List<ModelElementValidator<?>> validators = new ArrayList<>();
        validators.add(new ProcessValidator());
        validators.add(new UserTaskValidator());
        validators.add(new ExclusiveGatewayValidator());
        validators.add(new MultiInstanceLoopCharacteristicsValidator());
        // 自定义校验
        ValidationResults results = bpmnModelInstance.validate(validators);
        if (results.hasErrors()) {
            // 处理错误信息
            StringBuilder msg = new StringBuilder();
            Map<ModelElementInstance, List<ValidationResult>> map = results.getResults();
            Set<Map.Entry<ModelElementInstance, List<ValidationResult>>> entries = map.entrySet();
            for (Map.Entry<ModelElementInstance, List<ValidationResult>> entry : entries) {
                ModelElementInstance key = entry.getKey();
                List<ValidationResult> validationResults = map.get(key);
                for (ValidationResult validationResult : validationResults) {
                    msg.append(validationResult.getMessage()).append("；");
                }
            }
            throw new RuntimeException(msg.toString());
        }
    }

    /**
     * 创建 BPMN 元素对象
     *
     * @param parentElement 上级元素
     * @param id            元素ID
     * @param elementClass  元素类型
     * @param <T>           元素类型
     * @return 创建的BPMN 元素对象
     */
    protected <T extends BpmnModelElementInstance> T createElement(BpmnModelElementInstance parentElement, String id, Class<T> elementClass) {
        T element = parentElement.getModelInstance().newInstance(elementClass); // 创建元素
        element.setAttributeValue("id", id, true);// 设置ID
        parentElement.addChildElement(element); // 添加到父元素下
        return element;
    }

    /**
     * 创建各个元素之间的序列流（连线）
     *
     * @param process 流程对象
     * @param from    开始元素
     * @param to      结束元素
     * @return 序列流对象
     */
    public SequenceFlow createSequenceFlow(Process process, FlowNode from, FlowNode to) {
        String identifier = from.getId() + "-" + to.getId();
        SequenceFlow sequenceFlow = createElement(process, identifier, SequenceFlow.class);
        process.addChildElement(sequenceFlow);
        sequenceFlow.setSource(from);
        from.getOutgoing().add(sequenceFlow);
        sequenceFlow.setTarget(to);
        to.getIncoming().add(sequenceFlow);
        return sequenceFlow;
    }
}
