package com.pearl.study05.demo.chain;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/4
 */
public class HandlerA extends Handler {
    @Override
    public boolean doHandle() {
        boolean handled = false;
        //...
        return handled;
    }
}

