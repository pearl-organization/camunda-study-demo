package com.pearl.study05.demo.cmd;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.impl.interceptor.Command;
import org.camunda.bpm.engine.impl.interceptor.CommandContext;
import org.camunda.bpm.engine.task.Task;

import java.io.Serializable;

/**
 * 根据任务ID删除任务命令
 *
 * @author TD
 * @version 1.0
 * @date 2023/12/15
 */
public class DeleteTaskCmd implements Command<String>, Serializable {

    public static final long serialVersionUID = 1L;

    private String taskId;

    private RuntimeService runtimeService;

    private TaskService taskService;

    public DeleteTaskCmd(String taskId,RuntimeService runtimeService) {
        this.taskId = taskId;
        this.runtimeService=runtimeService;
    }

    @Override
    public String execute(CommandContext commandContext) {
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        return task.getId();
    }
}
