package com.pearl.study05.demo.cmd;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/4
 */
public class RemoteControlInvoker {

    Command onCommand ;
    Command offCommand;

    public void on() {
        onCommand.execute();
    }

    public void off() {
        offCommand.execute();
    }

    public RemoteControlInvoker(Command onCommand, Command offCommand) {
        this.onCommand = onCommand;
        this.offCommand = offCommand;
    }
}
