package com.pearl.study05.demo.validation;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import org.camunda.bpm.model.bpmn.instance.CompletionCondition;
import org.camunda.bpm.model.bpmn.instance.MultiInstanceLoopCharacteristics;
import org.camunda.bpm.model.xml.validation.ModelElementValidator;
import org.camunda.bpm.model.xml.validation.ValidationResultCollector;

/**
 * 多实例任务校验器
 * <bpmn:multiInstanceLoopCharacteristics isSequential="true" camunda:collection="1" camunda:elementVariable="1">
 *      <bpmn:completionCondition xsi:type="bpmn:tFormalExpression">1</bpmn:completionCondition>
 * </bpmn:multiInstanceLoopCharacteristics>
 *
 * @author TD
 * @version 1.0
 * @date 2024/1/15
 */
public class MultiInstanceLoopCharacteristicsValidator implements ModelElementValidator<MultiInstanceLoopCharacteristics> {
    @Override
    public Class<MultiInstanceLoopCharacteristics> getElementType() {
        return MultiInstanceLoopCharacteristics.class;
    }

    @Override
    public void validate(MultiInstanceLoopCharacteristics multiInstanceLoopCharacteristics, ValidationResultCollector validationResultCollector) {
        CompletionCondition completionCondition = multiInstanceLoopCharacteristics.getCompletionCondition(); //完成表达式
        if (ObjectUtil.isNull(completionCondition) || StrUtil.isEmpty(completionCondition.getTextContent())){
            validationResultCollector.addError(500, StrUtil.format("多实例任务【{}】完成条件表达式不能为空", multiInstanceLoopCharacteristics.getId()));
        }
        //
        String camundaCollection = multiInstanceLoopCharacteristics.getCamundaCollection(); // 集合
        if (StrUtil.isEmpty(camundaCollection)){
            validationResultCollector.addError(500, StrUtil.format("多实例任务【{}】集合不能为空", multiInstanceLoopCharacteristics.getId()));
        }
        String camundaElementVariable = multiInstanceLoopCharacteristics.getCamundaElementVariable(); // 元素变量
        if (StrUtil.isEmpty(camundaElementVariable)){
            validationResultCollector.addError(500, StrUtil.format("多实例任务【{}】元素变量不能为空", multiInstanceLoopCharacteristics.getId()));
        }
    }
}
