package com.pearl.study05.demo.chain;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/4
 */
public class Test {
    public static void main(String[] args) {
        HandlerChain chain = new HandlerChain();
        chain.addHandler(new HandlerA());
        chain.addHandler(new HandlerB());
        chain.handle();
    }
}
