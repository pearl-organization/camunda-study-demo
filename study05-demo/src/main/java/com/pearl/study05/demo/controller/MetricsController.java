package com.pearl.study05.demo.controller;

import com.pearl.study05.demo.common.R;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.ManagementService;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.management.Metrics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/9/12
 */
@RequestMapping("/metrics")
@RestController
@Tag(name = "指标服务")
public class MetricsController {

    @Autowired
    private ManagementService managementService;

    @PostMapping("/createMetricsQuery")
    @Operation(summary = "根据指标名查询指标")
    public R<Long> createMetricsQuery(@RequestParam String metricsName) {
        long numCompletedActivityInstances = managementService
                .createMetricsQuery()
                .name(metricsName)
                //.startDate() 开始日期
                //.endDate() 结束日期
                //.name(Metrics.ACTIVTY_INSTANCE_START)
                .sum();
        // managementService.getUniqueTaskWorkerCount()
        return R.success(numCompletedActivityInstances);
    }
}
