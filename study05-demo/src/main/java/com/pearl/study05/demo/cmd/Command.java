package com.pearl.study05.demo.cmd;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/3
 */
public interface Command {

    void execute();
}
