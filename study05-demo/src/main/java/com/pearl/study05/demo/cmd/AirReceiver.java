package com.pearl.study05.demo.cmd;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/4
 */
public class AirReceiver implements Receiver{

    public void on(){
        System.out.println("打开空调");
    }

    public void off(){
        System.out.println("关闭空调");
    }
}
