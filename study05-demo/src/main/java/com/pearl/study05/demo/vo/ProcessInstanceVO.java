package com.pearl.study05.demo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/2
 */
@Data
public class ProcessInstanceVO  implements Serializable {

    protected boolean shouldQueryForSubprocessInstance = false;
    protected boolean shouldQueryForSubCaseInstance = false;
    protected int cachedEntityState;
    protected int suspensionState;
    protected int revision;
    protected String processDefinitionId;
    protected String activityId;
    protected String activityName;
    protected String processInstanceId;
    protected String parentId;
    protected String superExecutionId;
    protected String rootProcessInstanceId;
    protected String superCaseExecutionId;
}
