package com.pearl.study05.demo.cmd;

/**
 *
 ● Receive接收者角色 =》

 该角色就是干活的角色，命令传递到这里是应该被执行的，具体到我们上面的例子中就是Group的三个实现类。

 ● Command命令角色  Command=> GetDeployedTaskFormCmd

 需要执行的所有命令都在这里声明。

 ● Invoker调用者角色 接收到命令，并执行命令。在例子中，我（项目经理）就是这个角色。=》CommandExecutor

 + CommandContext =》 传递数据
 * @author TD
 * @version 1.0
 * @date 2024/1/4
 */
public class Client {

    public static void main(String[] args) {
        // https://baijiahao.baidu.com/s?id=1682041649009979104&wfr=spider&for=pc
        AirReceiver airReceiver=new AirReceiver();

        CommandOff commandOff = new CommandOff(airReceiver);
        CommandOn commandOn = new CommandOn(airReceiver);

        RemoteControlInvoker remoteControlInvoker=new RemoteControlInvoker(commandOn,commandOff);
        remoteControlInvoker.on();
        remoteControlInvoker.off();


    }
}
