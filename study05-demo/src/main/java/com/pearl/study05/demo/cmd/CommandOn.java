package com.pearl.study05.demo.cmd;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/4
 */
public class CommandOn implements Command{
    AirReceiver airReceiver;
    @Override
    public void execute() {
        this.airReceiver.off();
    }

    public CommandOn(AirReceiver airReceiver) {
        this.airReceiver = airReceiver;
    }
}
