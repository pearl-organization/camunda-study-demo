package com.pearl.study05.demo;

/**
 * @author TD
 * @version 1.0
 * @date 2023/9/5
 */

import cn.hutool.core.util.IdUtil;
import org.springframework.stereotype.Component;
import org.camunda.bpm.engine.impl.cfg.IdGenerator;

@Component
public class MyUuidGenerator implements IdGenerator {

    @Override
    public String getNextId() {
        return IdUtil.getSnowflakeNextIdStr();
    }
}
