package com.pearl.study05.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Study05DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Study05DemoApplication.class, args);
	}

}
