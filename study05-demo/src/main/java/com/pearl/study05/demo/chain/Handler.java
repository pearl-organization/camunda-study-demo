package com.pearl.study05.demo.chain;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/4
 */
public abstract class Handler {
    protected Handler successor = null;

    public final void handle() {
        boolean handled = doHandle();
        if (successor != null && !handled) {
            successor.handle();
        }
    }

    protected abstract boolean doHandle();

    public Handler getSuccessor() {
        return successor;
    }

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }
}