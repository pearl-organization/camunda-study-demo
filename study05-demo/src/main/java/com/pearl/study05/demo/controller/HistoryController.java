package com.pearl.study05.demo.controller;

import com.pearl.study05.demo.common.R;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 */
@RequestMapping("/history")
@RestController
@Tag(name = "历史服务")
public class HistoryController {

    @Autowired
    private HistoryService historyService;

    @Autowired
    private IdentityService identityService;


    @PostMapping("/createHistoricProcessInstanceQuery")
    @Operation(summary = "查询历史流程实例数据")
    public R<List<HistoricProcessInstance>> createHistoricProcessInstanceQuery(String businessKey, String processInstanceId) {
        // 构建查询对象，并执行列表查询
        List<HistoricProcessInstance> instanceList = historyService.createHistoricProcessInstanceQuery()
                .processInstanceBusinessKey(businessKey) // 业务KEY
                .processInstanceId(processInstanceId)
                .list();
        return R.success(instanceList);
    }

    @PostMapping("/myStart")
    @Operation(summary = "我发起的")
    public R<List<HistoricProcessInstance>> myStart(String userId) {
        List<HistoricProcessInstance> instanceList = historyService.createHistoricProcessInstanceQuery()
                .startedBy(userId)
                .list();
        return R.success(instanceList);
    }

    @PostMapping("/myFinished")
    @Operation(summary = "我的已办")
    public R<List<HistoricTaskInstance>> myFinished(String userId) {
        List<HistoricTaskInstance> instanceList = historyService.createHistoricTaskInstanceQuery()
                .taskAssignee(userId)
                .finished()
                .list();
        return R.success(instanceList);
    }
}
