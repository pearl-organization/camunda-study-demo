package com.pearl.study05.demo.plugin;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.cfg.ProcessEnginePlugin;
import org.camunda.bpm.engine.impl.metrics.reporter.DbMetricsReporter;
import org.springframework.context.annotation.Configuration;

/**
 * @author TD
 * @version 1.0
 * @date 2023/9/12
 */
@Configuration
public class MetricsConfigurationPlugin implements ProcessEnginePlugin {

    public void preInit(ProcessEngineConfigurationImpl processEngineConfiguration) {
    }

    public void postInit(ProcessEngineConfigurationImpl processEngineConfiguration) {
        DbMetricsReporter metricsReporter = new DbMetricsReporter(processEngineConfiguration.getMetricsRegistry(),
                processEngineConfiguration.getCommandExecutorTxRequired());
        metricsReporter.setReportingIntervalInSeconds(60); // 设置报告间隔，每分钟收集一次
        processEngineConfiguration.setDbMetricsReporter(metricsReporter);
    }

    public void postProcessEngineBuild(ProcessEngine processEngine) {
    }

}