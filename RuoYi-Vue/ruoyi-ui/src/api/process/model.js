import request from '@/utils/request'

// 查询流程模型扩展列表
export function listModel(query) {
  return request({
    url: '/workflow/model/list',
    method: 'get',
    params: query
  })
}
// 查询发起流程的模型
export function listCanBeStarted(query) {
  return request({
    url: '/workflow/model/listCanBeStarted',
    method: 'get',
    params: query
  })
}

// 查询流程模型扩展详细
export function getModel(id) {
  return request({
    url: '/workflow/model/' + id,
    method: 'get'
  })
}

// 新增流程模型扩展
export function addModel(data) {
  return request({
    url: '/workflow/model',
    method: 'post',
    data: data
  })
}

// 修改流程模型扩展
export function updateModel(data) {
  return request({
    url: '/workflow/model',
    method: 'put',
    data: data
  })
}

// 删除流程模型扩展
export function delModel(id) {
  return request({
    url: '/workflow/model/' + id,
    method: 'delete'
  })
}

// 发布模型
export function publishModel(id) {
  return request({
    url: '/workflow/model/publish/' + id,
    method: 'get'
  })
}

// 获取模型XML
export function getModelXML(id) {
  return request({
    url: '/workflow/model/getXML/' + id,
    method: 'get'
  })
}


// 挂起或激活
export function suspensionModelState(data) {
  return request({
    url: '/workflow/model/suspensionState',
    method: 'post',
    data: data
  })
}
export function getModelStartForm(id) {
  return request({
    url: '/workflow/model/getStartForm/' + id,
    method: 'get'
  })
}

