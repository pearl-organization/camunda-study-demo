import request from '@/utils/request'

// 查询自定义流程实例列表
export function listInstance(query) {
  return request({
    url: '/workflow/instance/list',
    method: 'get',
    params: query
  })
}

export function myListInstance(query) {
  return request({
    url: '/workflow/instance/myList',
    method: 'get',
    params: query
  })
}

// 查询自定义流程实例详细
export function getInstance(id) {
  return request({
    url: '/workflow/instance/' + id,
    method: 'get'
  })
}

// 新增自定义流程实例
export function addInstance(data) {
  return request({
    url: '/workflow/instance',
    method: 'post',
    data: data
  })
}

// 撤销流程
export function revokeInstance(data) {
  return request({
    url: '/workflow/instance/revoke',
    method: 'post',
    data: data
  })
}
// 修改自定义流程实例
export function updateInstance(data) {
  return request({
    url: '/workflow/instance',
    method: 'put',
    data: data
  })
}

// 删除自定义流程实例
export function delInstance(id) {
  return request({
    url: '/workflow/instance/' + id,
    method: 'delete'
  })
}

//
export function getFormByInsId(id) {
  return request({
    url: '/workflow/instance/getFormVarsByInsId/' + id,
    method: 'get'
  })
}

// 启动流程实例
export function startProcess(data) {
  return request({
    url: '/workflow/instance/start',
    method: 'post',
    data: data
  })
}
// 动态流程图
export function getDynamicModelPic(id) {
  return request({
    url: '/workflow/instance/getDynamicModelPic/' + id,
    method: 'get'
  })
}
