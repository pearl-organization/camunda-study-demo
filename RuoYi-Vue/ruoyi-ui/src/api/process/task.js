import request from '@/utils/request'

// 我的待办
export function listTaskMyTodo(query) {
  return request({
    url: '/workflow/task/list/myTodo',
    method: 'get',
    params: query
  })
}

// 我的已办
export function myCompleted(query) {
  return request({
    url: '/workflow/task/list/myCompleted',
    method: 'get',
    params: query
  })
}

export function getFormByTaskId(id,isHis) {
  return request({
    url: '/workflow/task/getFormByTaskId/' + id+"/"+ isHis,
    method: 'get'
  })
}


// 审批同意
export function agree(data) {
  return request({
    url: '/workflow/task/agree',
    method: 'post',
    data: data
  })
}

// 审批拒绝
export function refuseTask(data) {
  return request({
    url: '/workflow/task/refuse',
    method: 'post',
    data: data
  })
}

// 审批转办
export function transferTask(data) {
  return request({
    url: '/workflow/task/transfer',
    method: 'post',
    data: data
  })
}

// 审批委派
export function delegateTask(data) {
  return request({
    url: '/workflow/task/delegate',
    method: 'post',
    data: data
  })
}


// 查询【请填写功能名称】详细
export function getTask(id) {
  return request({
    url: '/workflow/task/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addTask(data) {
  return request({
    url: '/workflow/task',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateTask(data) {
  return request({
    url: '/workflow/task',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delTask(id) {
  return request({
    url: '/workflow/task/' + id,
    method: 'delete'
  })
}

// 查询流程流传记录列表
export function listRecord(query) {
  return request({
    url: '/workflow/record/list',
    method: 'get',
    params: query
  })
}

// 查询流程流传记录详细
export function getRecord(id) {
  return request({
    url: '/workflow/record/' + id,
    method: 'get'
  })
}

// 新增流程流传记录
export function addRecord(data) {
  return request({
    url: '/workflow/record',
    method: 'post',
    data: data
  })
}

// 修改流程流传记录
export function updateRecord(data) {
  return request({
    url: '/workflow/record',
    method: 'put',
    data: data
  })
}

// 删除流程流传记录
export function delRecord(id) {
  return request({
    url: '/workflow/record/' + id,
    method: 'delete'
  })
}
