import request from '@/utils/request'

// 查询流程分类列表
export function listCategory(query) {
  return request({
    url: '/workflow/category/list',
    method: 'get',
    params: query
  })
}

// 查询列表（排除节点）
export function listCategoryExcludeChild(categoryId) {
  return request({
    url: '/workflow/category/list/exclude/' + categoryId,
    method: 'get'
  })
}

// 查询流程分类详细
export function getCategory(id) {
  return request({
    url: '/workflow/category/' + id,
    method: 'get'
  })
}

// 新增流程分类
export function addCategory(data) {
  return request({
    url: '/workflow/category',
    method: 'post',
    data: data
  })
}

// 修改流程分类
export function updateCategory(data) {
  return request({
    url: '/workflow/category',
    method: 'put',
    data: data
  })
}

// 删除流程分类
export function delCategory(id) {
  return request({
    url: '/workflow/category/' + id,
    method: 'delete'
  })
}

// 查询下拉树结构
export function treeCategorySelect() {
  return request({
    url: '/workflow/category/tree',
    method: 'get'
  })
}
