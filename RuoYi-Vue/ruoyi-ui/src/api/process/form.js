import request from '@/utils/request'

// 查询流程表单列表
export function listForm(query) {
  return request({
    url: '/workflow/form/list',
    method: 'get',
    params: query
  })
}
export function listAllForm(query) {
  return request({
    url: '/workflow/form/formList',
    method: 'get',
    params: query
  })
}

// 查询流程表单详细
export function getForm(id) {
  return request({
    url: '/workflow/form/' + id,
    method: 'get'
  })
}

// 新增流程表单
export function addForm(data) {
  return request({
    url: '/workflow/form',
    method: 'post',
    data: data
  })
}

// 修改流程表单
export function updateForm(data) {
  return request({
    url: '/workflow/form',
    method: 'put',
    data: data
  })
}
// 挂载表单
export function addDeployForm(data) {
  return request({
    url: '/workflow/form/addDeployForm',
    method: 'post',
    data: data
  })
}

// 删除流程表单
export function delForm(id) {
  return request({
    url: '/workflow/form/' + id,
    method: 'delete'
  })
}

// 导出流程表单
export function exportForm(query) {
  return request({
    url: '/workflow/form/export',
    method: 'get',
    params: query
  })
}
