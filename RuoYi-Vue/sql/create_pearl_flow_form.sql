-- Table structure for sys_form
-- ----------------------------
DROP TABLE IF EXISTS `pearl_flow_form`;
CREATE TABLE `pearl_flow_form`
(
    `id`          bigint(20) NOT NULL AUTO_INCREMENT COMMENT '表单主键',
    `name`        varchar(50)  DEFAULT NULL COMMENT '表单名称',
    `content`     longtext COMMENT '表单内容',
    `create_time` datetime     DEFAULT NULL COMMENT '创建时间',
    `remark`      varchar(255) DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3171 DEFAULT CHARSET=utf8mb4 COMMENT='流程表单';