-- `ry-vue`.pearl_flow_category definition

CREATE TABLE `pearl_flow_category`
(
    `id`          bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `parent_id`   bigint                                                     DEFAULT NULL COMMENT '父ID',
    `ancestors`   varchar(100)                                               DEFAULT '' COMMENT '祖级列表',
    `name`        varchar(100) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
    `sort`        bigint                                                     DEFAULT NULL COMMENT '排序',
    `create_by`   varchar(64) CHARACTER SET utf8mb3 COLLATE utf8_general_ci  DEFAULT '' COMMENT '创建者',
    `create_time` datetime                                                   DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(64) CHARACTER SET utf8mb3 COLLATE utf8_general_ci  DEFAULT '' COMMENT '更新者',
    `update_time` datetime                                                   DEFAULT NULL COMMENT '更新时间',
    `remark`      varchar(500) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3 COMMENT='流程分类表';