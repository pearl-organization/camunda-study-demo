package com.ruoyi.workflow.service;

import com.ruoyi.workflow.pojo.entity.PearlFlowForm;

import java.util.List;

/**
 * 流程单Service接口
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
public interface IPearlFlowFormService 
{
    /**
     * 查询流程单
     * 
     * @param id 流程单主键
     * @return 流程单
     */
    public PearlFlowForm selectPearlFlowFormById(Long id);

    /**
     * 查询流程单列表
     * 
     * @param pearlFlowForm 流程单
     * @return 流程单集合
     */
    public List<PearlFlowForm> selectPearlFlowFormList(PearlFlowForm pearlFlowForm);

    /**
     * 新增流程单
     * 
     * @param pearlFlowForm 流程单
     * @return 结果
     */
    public int insertPearlFlowForm(PearlFlowForm pearlFlowForm);

    /**
     * 修改流程单
     * 
     * @param pearlFlowForm 流程单
     * @return 结果
     */
    public int updatePearlFlowForm(PearlFlowForm pearlFlowForm);

    /**
     * 批量删除流程单
     * 
     * @param ids 需要删除的流程单主键集合
     * @return 结果
     */
    public int deletePearlFlowFormByIds(Long[] ids);

    /**
     * 删除流程单信息
     * 
     * @param id 流程单主键
     * @return 结果
     */
    public int deletePearlFlowFormById(Long id);
}
