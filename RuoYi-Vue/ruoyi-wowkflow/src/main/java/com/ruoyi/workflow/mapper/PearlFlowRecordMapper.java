package com.ruoyi.workflow.mapper;

import com.ruoyi.workflow.pojo.entity.PearlFlowRecord;

import java.util.List;

/**
 * 流程流传记录Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface PearlFlowRecordMapper {
    /**
     * 查询流程流传记录
     *
     * @param id 流程流传记录主键
     * @return 流程流传记录
     */
    public PearlFlowRecord selectPearlFlowRecordById(Long id);

    /**
     * 查询流程流传记录列表
     *
     * @param pearlFlowRecord 流程流传记录
     * @return 流程流传记录集合
     */
    public List<PearlFlowRecord> selectPearlFlowRecordList(PearlFlowRecord pearlFlowRecord);

    /**
     * 新增流程流传记录
     *
     * @param pearlFlowRecord 流程流传记录
     * @return 结果
     */
    public int insertPearlFlowRecord(PearlFlowRecord pearlFlowRecord);

    /**
     * 修改流程流传记录
     *
     * @param pearlFlowRecord 流程流传记录
     * @return 结果
     */
    public int updatePearlFlowRecord(PearlFlowRecord pearlFlowRecord);

    /**
     * 删除流程流传记录
     *
     * @param id 流程流传记录主键
     * @return 结果
     */
    public int deletePearlFlowRecordById(Long id);

    /**
     * 批量删除流程流传记录
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePearlFlowRecordByIds(Long[] ids);
}
