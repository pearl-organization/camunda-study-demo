package com.ruoyi.workflow.controller;

import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.workflow.pojo.entity.FlowCategory;
import com.ruoyi.workflow.service.IFlowCategoryService;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.page.TableDataInfo;

import java.util.List;

/**
 * 流程分类Controller
 *
 * @author ruoyi
 * @date 2023-12-04
 */
@RestController
@RequestMapping("/workflow/category")
public class FlowCategoryController extends BaseController {
    @Autowired
    private IFlowCategoryService flowCategoryService;

    /**
     * 查询流程分类列表
     */
    @GetMapping("/list")
    public AjaxResult list(FlowCategory flowCategory) {
        List<FlowCategory> list = flowCategoryService.selectFlowCategoryList(flowCategory);
        return success(list);
    }

    /**
     * 查询分类列表（排除节点）
     */
    @GetMapping("/list/exclude/{id}")
    public AjaxResult excludeChild(@PathVariable(value = "id", required = false) Long id) {
        List<FlowCategory> list = flowCategoryService.selectFlowCategoryList(new FlowCategory());
        list.removeIf(d -> d.getId().intValue() == id || ArrayUtils.contains(StringUtils.split(d.getAncestors(), ","), String.valueOf(id)));
        return success(list);
    }


    /**
     * 获取部门树列表
     */
    @GetMapping("/tree")
    public AjaxResult tree(FlowCategory flowCategory)
    {
        return success(flowCategoryService.selectTreeList(flowCategory));
    }


    /**
     * 导出流程分类列表
     */
    @Log(title = "流程分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FlowCategory FlowCategory) {
        List<FlowCategory> list = flowCategoryService.selectFlowCategoryList(FlowCategory);
        ExcelUtil<FlowCategory> util = new ExcelUtil<FlowCategory>(FlowCategory.class);
        util.exportExcel(response, list, "流程分类数据");
    }

    /**
     * 获取流程分类详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(flowCategoryService.selectFlowCategoryById(id));
    }

    /**
     * 新增流程分类
     */
    @Log(title = "流程分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FlowCategory FlowCategory) {
        return toAjax(flowCategoryService.insertFlowCategory(FlowCategory));
    }

    /**
     * 修改流程分类
     */
    @Log(title = "流程分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FlowCategory FlowCategory) {
        return toAjax(flowCategoryService.updateFlowCategory(FlowCategory));
    }

    /**
     *
     */
    @Log(title = "流程分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(flowCategoryService.deleteFlowCategoryByIds(ids));
    }
}
