package com.ruoyi.workflow.pojo.param;

import lombok.Data;

/**
 * @author TD
 * @version 1.0
 * @date 2024/5/30
 */
@Data
public class MyStartInstanceParams {
    /**
     * 流程名称
     */
    private String modelName;
    /**
     * 流程状态
     */
    private Integer state;

    /**
     * 发起人
     */
    private Long initiator;
}
