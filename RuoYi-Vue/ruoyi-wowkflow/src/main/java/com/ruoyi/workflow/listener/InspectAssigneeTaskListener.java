package com.ruoyi.workflow.listener;

import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;

/**
 * 审批人检查全局监听器
 * @author TD
 * @version 1.0
 * @date 2024/1/12
 */
public class InspectAssigneeTaskListener implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        BpmnModelInstance bpmnModelInstance = delegateTask.getBpmnModelInstance();
        //bpmnModelInstance.validate()
        // 多实例时集合为空
        // 单实例查询不到人
    }
}
