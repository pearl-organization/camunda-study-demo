package com.ruoyi.workflow.listener;

import org.camunda.bpm.engine.impl.bpmn.parser.BpmnParseListener;
import org.camunda.bpm.engine.impl.cfg.AbstractProcessEnginePlugin;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 全局监听器插件
 *
 * @author TD
 * @version 1.0
 * @date 2024/1/9
 */
@Configuration
public class GlobalListenerPlugin extends AbstractProcessEnginePlugin {
    @Override
    public void preInit(ProcessEngineConfigurationImpl processEngineConfiguration) {
        // 添加全局监听器
        List<BpmnParseListener> preParseListeners = processEngineConfiguration.getCustomPreBPMNParseListeners();
        preParseListeners.add(new GlobalBpmnParseListener());
    }
}
