package com.ruoyi.workflow.service;


import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.workflow.pojo.entity.FlowInstance;
import com.ruoyi.workflow.pojo.param.MyStartInstanceParams;
import com.ruoyi.workflow.pojo.param.RevokeInstanceParams;
import com.ruoyi.workflow.pojo.param.StartInstanceParams;
import com.ruoyi.workflow.pojo.vo.MyStartInstanceVO;

import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * 自定义流程实例Service接口
 *
 * @author ruoyi
 * @date 2023-12-19
 */
public interface IFlowInstanceService {
    /**
     * 启动流程实例
     *
     * @param params 参数
     * @return
     */
    String start(StartInstanceParams params);

    /**
     * 查询自定义流程实例
     *
     * @param id 自定义流程实例主键
     * @return 自定义流程实例
     */
    public FlowInstance selectFlowInstanceById(Long id);

    /**
     * 查询自定义流程实例
     *
     * @param insId 流程实例主键
     * @return 自定义流程实例
     */
    public FlowInstance selectFlowByInstanceId(String insId);

    /**
     * 查询自定义流程实例列表
     *
     * @param pearlFlowInstance 自定义流程实例
     * @return 自定义流程实例集合
     */
    public List<FlowInstance> selectFlowInstanceList(FlowInstance pearlFlowInstance);

    /**
     * 新增自定义流程实例
     *
     * @param pearlFlowInstance 自定义流程实例
     * @return 结果
     */
    public int insertFlowInstance(FlowInstance pearlFlowInstance);

    /**
     * 修改自定义流程实例
     *
     * @param pearlFlowInstance 自定义流程实例
     * @return 结果
     */
    public int updateFlowInstance(FlowInstance pearlFlowInstance);

    /**
     * 批量删除自定义流程实例
     *
     * @param ids 需要删除的自定义流程实例主键集合
     * @return 结果
     */
    public int deleteFlowInstanceByIds(Long[] ids);

    /**
     * 删除自定义流程实例信息
     *
     * @param id 自定义流程实例主键
     * @return 结果
     */
    public int deleteFlowInstanceById(Long id);

    /**
     * 流程流转记录
     */
    AjaxResult transferRecords(String processInstanceId);

    /**
     * 撤销流程
     * @param params 请求参数
     */
    void revoke(RevokeInstanceParams params);

    /**
     * 我发起的
     *
     * @param params 参数
     * @return
     */
    List<MyStartInstanceVO> selectMyFlowInstanceList(MyStartInstanceParams params);

    /**
     * 查询填写的表单
     *
     * @param id 流程实例ID
     * @return 表单
     */
    Map<String, Object> getFormVarsByInsId(Long id);

    /**
     * 查询动态的流程图
     *
     * @param id 扩展表流程实例ID
     * @return
     * @throws IOException
     */
    Map<String, Object> getDynamicModelPic(Long id) throws IOException;
}
