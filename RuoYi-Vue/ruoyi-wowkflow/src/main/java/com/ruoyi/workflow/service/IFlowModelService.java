package com.ruoyi.workflow.service;

import com.ruoyi.workflow.pojo.entity.FlowModel;
import com.ruoyi.workflow.pojo.entity.PearlFlowForm;
import com.ruoyi.workflow.pojo.param.SuspensionModelParams;
import com.ruoyi.workflow.pojo.vo.CanBeStaredFlowModelVO;
import com.ruoyi.workflow.pojo.vo.FlowModelDetailVO;
import com.ruoyi.workflow.pojo.vo.FlowModelVO;

import java.io.IOException;
import java.util.List;

/**
 * 流程模型扩展Service接口
 *
 * @author ruoyi
 * @date 2023-12-11
 */
public interface IFlowModelService {
    /**
     * 查询流程模型扩展
     *
     * @param id 流程模型扩展主键
     * @return 流程模型扩展
     */
    FlowModel selectFlowModelById(Long id);

    /**
     * 查询流程模型扩展列表
     *
     * @param flowModel 流程模型扩展
     * @return 流程模型扩展集合
     */
    List<FlowModelVO> selectFlowModelList(FlowModel flowModel);

    /**
     * 新增流程模型扩展
     *
     * @param flowModel 流程模型扩展
     * @return 主键ID
     */
    Long insertFlowModel(FlowModel flowModel);

    /**
     * 修改流程模型扩展
     *
     * @param flowModel 流程模型扩展
     * @return 结果
     */
    int updateFlowModel(FlowModel flowModel);

    /**
     * 批量删除流程模型扩展
     *
     * @param ids 需要删除的流程模型扩展主键集合
     * @return 结果
     */
    int deleteFlowModelByIds(Long[] ids);

    /**
     * 删除流程模型扩展信息
     *
     * @param id 流程模型扩展主键
     * @return 结果
     */
    boolean deleteFlowModelById(Long id);

    /**
     * 发布模型
     *
     * @param id 模型ID
     */
    void publish(Long id);

    /**
     * 查看流程图
     *
     * @param id 模型ID
     */
    String getXML(Long id) throws IOException;

    /**
     * 挂起、激活流程定义
     *
     * @param params          请求参数
     */
    void suspension(SuspensionModelParams params);

    PearlFlowForm getStartFormById(Long id);

    void validate(FlowModel flowMode);

    /**
     * 获取流程模型詳細信息
     */
    FlowModelDetailVO selectFlowModelDetailById(Long id);

    /**
     * 查询可以发起的流程模型
     * @param name 名称
     * @param categoryId 分类
     */
    List<CanBeStaredFlowModelVO> listCanBeStarted(String name, Long categoryId);
}
