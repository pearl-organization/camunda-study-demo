package com.ruoyi.workflow.pojo.vo;

import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2024/5/7
 */
public class FlowModelDetailVO extends FlowModelVO {

    // 历史记录保存时间
    private Integer historyTimeToLive;

    public Integer getHistoryTimeToLive() {
        return historyTimeToLive;
    }

    public void setHistoryTimeToLive(Integer historyTimeToLive) {
        this.historyTimeToLive = historyTimeToLive;
    }
}
