package com.ruoyi.workflow.validation;

import cn.hutool.core.util.StrUtil;
import org.camunda.bpm.model.bpmn.instance.UserTask;
import org.camunda.bpm.model.xml.validation.ModelElementValidator;
import org.camunda.bpm.model.xml.validation.ValidationResultCollector;

/**
 * 用户任务校验器
 * @author TD
 * @version 1.0
 * @date 2024/1/15
 */
public class UserTaskValidator implements ModelElementValidator<UserTask> {
    @Override
    public Class<UserTask> getElementType() {
        return UserTask.class;
    }

    @Override
    public void validate(UserTask userTask, ValidationResultCollector validationResultCollector) {
        // 校验是否设置审批人
        String camundaAssignee = userTask.getCamundaAssignee();
        if (StrUtil.isEmpty(camundaAssignee)) {
            validationResultCollector.addError(500, StrUtil.format("用户任务【{}】处理人不能为空", userTask.getName()));
        }
    }
}
