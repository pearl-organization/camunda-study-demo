package com.ruoyi.workflow.enums;

/**
 * @author TD
 * @version 1.0
 * @date 2023/12/25
 */
public enum HandleActionEnum {

    /**
     * 审批通过（完成任务，继续流转）
     */
    AGREE(1, "同意"),

    /**
     * 审批拒绝（完成任务，但是流程可能被直接终止）
     */
    REFUSE(2, "拒绝"),

    /**
     * 委派，A委派给-》B 审完 -》A 审完后，继续流转
     */
    DELEGATE(3, "委派"),

    /**
     * 转办，转交给别人办理，也叫转签，A -》B 审完后，继续向后流转
     */
    TRANSFER(4, "转办"),

/*    *//**
     * A 指派给-》B 审完后，继续流转 在当前节点之前增加一个审批节点，当新增的节点同意后，再流转至当前节点。
     *//*
    FRONT_ADD_ASSIGNEE(5, "向前加签"), 并加签，在当前节点同步增加其他审批人。

    *//**
     * A 指派给-》B 审完后，继续流转 在当前节点之后增加一个审批节点，当前节点会默认同意，并流转至新增的节点。
     *//*
    BACK_ADD_ASSIGNEE(5, "向后加签"),*/

    ;

    // 流程实例


    private Integer code;
    private String msg;

    HandleActionEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
