package com.ruoyi.workflow.pojo.query;

import lombok.Data;

/**
 * @author TD
 * @version 1.0
 * @date 2023/12/22
 */
@Data
public class MyTodoTaskQuery {

    /**
     * 审批人
     */
    private String assignee;

    /**
     * 流程名称
     */
    private String modelName;
}
