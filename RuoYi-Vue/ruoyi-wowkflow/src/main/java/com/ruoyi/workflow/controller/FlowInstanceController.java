package com.ruoyi.workflow.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.workflow.pojo.entity.FlowInstance;
import com.ruoyi.workflow.pojo.param.MyStartInstanceParams;
import com.ruoyi.workflow.pojo.param.RevokeInstanceParams;
import com.ruoyi.workflow.pojo.param.StartInstanceParams;
import com.ruoyi.workflow.pojo.vo.MyStartInstanceVO;
import com.ruoyi.workflow.service.IFlowInstanceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.core.controller.BaseController;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 自定义流程实例Controller
 *
 * @author ruoyi
 * @date 2023-12-19
 */
@Tag(name = "流程实例")
@RestController
@RequestMapping("/workflow/instance")
public class FlowInstanceController extends BaseController {

    @Autowired
    private IFlowInstanceService flowInstanceService;

    @Operation(summary = "发起流程")
    @PostMapping("/start")
    public AjaxResult start(@RequestBody StartInstanceParams params) {
        String id = flowInstanceService.start(params);
        return AjaxResult.success("发起成功", id);
    }

    @Operation(summary = "查询流转记录")
    @GetMapping(value = "/transferRecords/{processInstanceId}")
    public AjaxResult transferRecords(@PathVariable String processInstanceId) {
        return flowInstanceService.transferRecords(processInstanceId);
    }

    @Operation(summary = "撤销流程")
    @PostMapping(value = "/revoke")
    public AjaxResult revoke(@RequestBody RevokeInstanceParams params) {
        flowInstanceService.revoke(params);
        return AjaxResult.success();
    }

    /**
     * 查询我发起的
     */
    @GetMapping("/myList")
    public TableDataInfo myList(MyStartInstanceParams params) {
        startPage();
        List<MyStartInstanceVO> list = flowInstanceService.selectMyFlowInstanceList(params);
        return getDataTable(list);
    }

    /**
     * 根据流程ID查询表单变量（之前都作为了流程变量存入了流程表中）
     */
    @GetMapping("/getFormVarsByInsId/{id}")
    public AjaxResult getFormVarsByInsId(@PathVariable Long id) {
        Map<String, Object> form = flowInstanceService.getFormVarsByInsId(id);
        return AjaxResult.success(form);
    }

    /**
     * 获取动态流程图
     */
    @GetMapping("/getDynamicModelPic/{id}")
    public AjaxResult getDynamicModelPic(@PathVariable Long id) throws IOException {
        Map<String, Object> dynamicModelPic = flowInstanceService.getDynamicModelPic(id);
        return AjaxResult.success(dynamicModelPic);
    }





    /**
     * 查询自定义流程实例列表
     */
    @GetMapping("/list")
    public TableDataInfo list(FlowInstance FlowInstance) {
        startPage();
        List<FlowInstance> list = flowInstanceService.selectFlowInstanceList(FlowInstance);
        return getDataTable(list);
    }

    /**
     * 获取自定义流程实例详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(flowInstanceService.selectFlowInstanceById(id));
    }

    /**
     * 新增自定义流程实例
     */
    @PostMapping
    public AjaxResult add(@RequestBody FlowInstance FlowInstance) {
        return toAjax(flowInstanceService.insertFlowInstance(FlowInstance));
    }

    /**
     * 修改自定义流程实例
     */
    @PutMapping
    public AjaxResult edit(@RequestBody FlowInstance FlowInstance) {
        return toAjax(flowInstanceService.updateFlowInstance(FlowInstance));
    }

    /**
     * 删除自定义流程实例
     */
    @Log(title = "自定义流程实例", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(flowInstanceService.deleteFlowInstanceByIds(ids));
    }
}
