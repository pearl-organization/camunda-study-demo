package com.ruoyi.workflow.pojo.param;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/10
 */
@Data
public class RevokeInstanceParams {

    /**
     * 流程实例ID
     */
    @NotNull
    Long id;

    /**
     * 撤销原因
     */
    String reason;
}
