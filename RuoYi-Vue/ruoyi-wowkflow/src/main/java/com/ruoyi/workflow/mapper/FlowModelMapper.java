package com.ruoyi.workflow.mapper;

import com.ruoyi.workflow.pojo.entity.FlowModel;
import com.ruoyi.workflow.pojo.vo.FlowModelVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 流程模型扩展Mapper接口
 *
 * @author ruoyi
 * @date 2023-12-11
 */
public interface FlowModelMapper {
    /**
     * 查询流程模型扩展
     *
     * @param id 流程模型扩展主键
     * @return 流程模型扩展
     */
    FlowModel selectFlowModelById(Long id);

    /**
     * 查询流程模型扩展列表
     *
     * @param pearlFlowModel 流程模型扩展
     * @return 流程模型扩展集合
     */
    List<FlowModelVO> selectFlowModelList(FlowModel pearlFlowModel);

    /**
     * 新增流程模型扩展
     *
     * @param pearlFlowModel 流程模型扩展
     * @return 结果
     */
    int insertFlowModel(FlowModel pearlFlowModel);

    /**
     * 修改流程模型扩展
     *
     * @param pearlFlowModel 流程模型扩展
     * @return 结果
     */
    int updateFlowModel(FlowModel pearlFlowModel);

    /**
     * 删除流程模型扩展
     *
     * @param id 流程模型扩展主键
     * @return 结果
     */
    int deleteFlowModelById(Long id);

    /**
     * 批量删除流程模型扩展
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteFlowModelByIds(Long[] ids);

    int deleteFlowModelByKey(String defKey);

    void updateFlowModelSuspensionStateByKey(@Param("defKey") String defKey, @Param("suspensionState")Integer suspensionState);
}
