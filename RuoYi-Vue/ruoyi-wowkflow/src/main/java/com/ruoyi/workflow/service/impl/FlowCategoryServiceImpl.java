package com.ruoyi.workflow.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.core.domain.TreeSelect;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.workflow.mapper.FlowCategoryMapper;
import com.ruoyi.workflow.pojo.entity.FlowCategory;
import com.ruoyi.workflow.pojo.entity.FlowCategoryTreeSelect;
import com.ruoyi.workflow.service.IFlowCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 流程分类Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-04
 */
@Service
public class FlowCategoryServiceImpl implements IFlowCategoryService {
    @Autowired
    private FlowCategoryMapper flowCategoryMapper;

    /**
     * 查询流程分类
     *
     * @param id 流程分类主键
     * @return 流程分类
     */
    @Override
    public FlowCategory selectFlowCategoryById(Long id) {
        return flowCategoryMapper.selectFlowCategoryById(id);
    }

    /**
     * 查询流程分类列表
     *
     * @param FlowCategory 流程分类
     * @return 流程分类
     */
    @Override
    public List<FlowCategory> selectFlowCategoryList(FlowCategory FlowCategory) {
        return flowCategoryMapper.selectFlowCategoryList(FlowCategory);
    }

    /**
     * 新增流程分类
     *
     * @param flowCategory 流程分类
     * @return 结果
     */
    @Override
    public int insertFlowCategory(FlowCategory flowCategory) {
        // 没有父级 默认顶级
        if (ObjectUtil.isNull(flowCategory.getParentId())) {
            flowCategory.setParentId(0L);
            flowCategory.setAncestors("0");
        } else {
            FlowCategory parent = flowCategoryMapper.selectFlowCategoryById(flowCategory.getParentId());
            flowCategory.setAncestors(parent.getAncestors() + StrUtil.COMMA + parent.getId());
            flowCategory.setParentId(parent.getId());
        }
        flowCategory.setCreateTime(DateUtils.getNowDate());
        flowCategory.setCreateBy(SecurityUtils.getUsername());
        return flowCategoryMapper.insertFlowCategory(flowCategory);
    }

    /**
     * 修改流程分类
     *
     * @param FlowCategory 流程分类
     * @return 结果
     */
    @Override
    public int updateFlowCategory(FlowCategory FlowCategory) {
        FlowCategory.setUpdateTime(DateUtils.getNowDate());
        return flowCategoryMapper.updateFlowCategory(FlowCategory);
    }

    /**
     * 批量删除流程分类
     *
     * @param ids 需要删除的流程分类主键
     * @return 结果
     */
    @Override
    public int deleteFlowCategoryByIds(Long[] ids) {
        return flowCategoryMapper.deleteFlowCategoryByIds(ids);
    }

    /**
     * 删除流程分类信息
     *
     * @param id 流程分类主键
     * @return 结果
     */
    @Override
    public int deleteFlowCategoryById(Long id) {
        return flowCategoryMapper.deleteFlowCategoryById(id);
    }

    @Override
    public List<FlowCategoryTreeSelect> selectTreeList(FlowCategory flowCategory) {
        List<FlowCategory> categoryList = selectFlowCategoryList(flowCategory);
        List<FlowCategory> returnList = new ArrayList<>();
        List<Long> tempList = categoryList.stream().map(FlowCategory::getId).collect(Collectors.toList());
        for (FlowCategory fc : categoryList) {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(fc.getParentId())) {
                recursionFn(categoryList, fc);
                returnList.add(fc);
            }
        }
        if (returnList.isEmpty()) {
            returnList = categoryList;
        }
        return returnList.stream().map(FlowCategoryTreeSelect::new).collect(Collectors.toList());
    }

    private void recursionFn(List<FlowCategory> list, FlowCategory t) {
        // 得到子节点列表
        List<FlowCategory> childList = getChildList(list, t);
        t.setChildren(childList);
        for (FlowCategory tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }


    /**
     * 得到子节点列表
     */
    private List<FlowCategory> getChildList(List<FlowCategory> list, FlowCategory t) {
        List<FlowCategory> tlist = new ArrayList<>();
        for (FlowCategory n : list) {
            if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getId().longValue()) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<FlowCategory> list, FlowCategory t) {
        return getChildList(list, t).size() > 0;
    }

}
