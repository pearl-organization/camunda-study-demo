package com.ruoyi.workflow.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.workflow.mapper.PearlFlowRecordMapper;
import com.ruoyi.workflow.pojo.entity.PearlFlowRecord;
import com.ruoyi.workflow.service.IPearlFlowRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 流程流传记录Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@Service
public class PearlFlowRecordServiceImpl implements IPearlFlowRecordService
{
    @Autowired
    private PearlFlowRecordMapper pearlFlowRecordMapper;

    /**
     * 查询流程流传记录
     *
     * @param id 流程流传记录主键
     * @return 流程流传记录
     */
    @Override
    public PearlFlowRecord selectPearlFlowRecordById(Long id)
    {
        return pearlFlowRecordMapper.selectPearlFlowRecordById(id);
    }

    /**
     * 查询流程流传记录列表
     *
     * @param pearlFlowRecord 流程流传记录
     * @return 流程流传记录
     */
    @Override
    public List<PearlFlowRecord> selectPearlFlowRecordList(PearlFlowRecord pearlFlowRecord)
    {
        return pearlFlowRecordMapper.selectPearlFlowRecordList(pearlFlowRecord);
    }

    /**
     * 新增流程流传记录
     *
     * @param pearlFlowRecord 流程流传记录
     * @return 结果
     */
    @Override
    public int insertPearlFlowRecord(PearlFlowRecord pearlFlowRecord)
    {
        pearlFlowRecord.setCreateTime(DateUtils.getNowDate());
        return pearlFlowRecordMapper.insertPearlFlowRecord(pearlFlowRecord);
    }

    /**
     * 修改流程流传记录
     *
     * @param pearlFlowRecord 流程流传记录
     * @return 结果
     */
    @Override
    public int updatePearlFlowRecord(PearlFlowRecord pearlFlowRecord)
    {
        pearlFlowRecord.setUpdateTime(DateUtils.getNowDate());
        return pearlFlowRecordMapper.updatePearlFlowRecord(pearlFlowRecord);
    }

    /**
     * 批量删除流程流传记录
     *
     * @param ids 需要删除的流程流传记录主键
     * @return 结果
     */
    @Override
    public int deletePearlFlowRecordByIds(Long[] ids)
    {
        return pearlFlowRecordMapper.deletePearlFlowRecordByIds(ids);
    }

    /**
     * 删除流程流传记录信息
     *
     * @param id 流程流传记录主键
     * @return 结果
     */
    @Override
    public int deletePearlFlowRecordById(Long id)
    {
        return pearlFlowRecordMapper.deletePearlFlowRecordById(id);
    }
}
