package com.ruoyi.workflow.pojo.param;

import lombok.Data;

/**
 * @author TD
 * @version 1.0
 * @date 2023/12/13
 */
@Data
public class SuspensionModelParams {

    private Long modelId;

    private Integer suspensionState;
}
