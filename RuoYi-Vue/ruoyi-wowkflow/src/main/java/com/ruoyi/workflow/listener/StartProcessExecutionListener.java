package com.ruoyi.workflow.listener;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.workflow.constant.VariablesConstants;
import com.ruoyi.workflow.enums.FlowInstanceStateEnum;
import com.ruoyi.workflow.mapper.FlowInstanceMapper;
import com.ruoyi.workflow.pojo.entity.FlowInstance;
import com.ruoyi.workflow.pojo.entity.PearlFlowRecord;
import com.ruoyi.workflow.service.IPearlFlowRecordService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.impl.persistence.entity.ExecutionEntity;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.impl.BpmnModelConstants;
import org.camunda.bpm.model.bpmn.instance.FlowElement;
import org.camunda.bpm.model.bpmn.instance.UserTask;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 启动流程全局监听
 *
 * @author TD
 * @version 1.0
 * @date 2024/1/2
 */
@Slf4j
public class StartProcessExecutionListener implements ExecutionListener {


    public void notify(DelegateExecution delegateExecution) throws Exception {
        // 1. 获取流程信息
        ExecutionEntity execution = (ExecutionEntity) delegateExecution;
        Map<String, Object> variables = execution.getVariables(); // 流程变量
        String processInstanceId = execution.getProcessInstanceId();// 流程实例ID
        Long initiator = MapUtil.getLong(variables, VariablesConstants.INITIATOR); // 发起人用户ID
        Long modelId = MapUtil.getLong(variables, VariablesConstants.MODEL_ID);
        Long formId = MapUtil.getLong(variables, VariablesConstants.FORM_ID);
        String title = MapUtil.getStr(variables, VariablesConstants.TITLE);

        // 2. 保存自定义流程信息
        FlowInstanceMapper flowInstanceMapper = SpringUtil.getBean(FlowInstanceMapper.class);
        FlowInstance flowInstance = new FlowInstance();
        flowInstance.setId(System.currentTimeMillis());
        flowInstance.setFormId(formId);
        flowInstance.setInstanceId(processInstanceId);
        flowInstance.setState(FlowInstanceStateEnum.PROCESSING.getCode());
        flowInstance.setBusinessKey(execution.getBusinessKey());
        flowInstance.setCreateTime(DateUtils.getNowDate());
        flowInstance.setCreateBy(SecurityUtils.getUsername());
        flowInstance.setModelId(modelId);
        flowInstance.setInitiator(initiator);
        flowInstance.setCreateTime(new Date());
        flowInstance.setTitle(title);
        flowInstanceMapper.insertFlowInstance(flowInstance);

        // 4. 新增流转记录
        PearlFlowRecord record = new PearlFlowRecord();
        record.setInsId(flowInstance.getId());
        record.setTaskName("发起");
        record.setHandlerUserName(SecurityUtils.getUsername());
        record.setHandlerAction("填写表单");
        IPearlFlowRecordService flowRecordService = SpringUtil.getBean(IPearlFlowRecordService.class);
        flowRecordService.insertPearlFlowRecord(record);
    }
}
