package com.ruoyi.workflow.pojo.vo;

import lombok.Data;

/**
 * @author TD
 * @version 1.0
 * @date 2023/12/25
 */
@Data
public class ActivityCompleteStatusVO {

    /**
     * 节点ID
     */
    private String key;

    /**
     * 是否完成
     */
    private boolean completed;
}
