package com.ruoyi.workflow.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.workflow.enums.FlowModelStateEnum;
import com.ruoyi.workflow.enums.SuspensionStateEnum;
import com.ruoyi.workflow.exception.FlowModelException;
import com.ruoyi.workflow.mapper.FlowModelMapper;
import com.ruoyi.workflow.pojo.entity.FlowModel;
import com.ruoyi.workflow.pojo.entity.PearlFlowForm;
import com.ruoyi.workflow.pojo.param.SuspensionModelParams;
import com.ruoyi.workflow.pojo.query.ModelQuery;
import com.ruoyi.workflow.pojo.vo.CanBeStaredFlowModelVO;
import com.ruoyi.workflow.pojo.vo.FlowModelDetailVO;
import com.ruoyi.workflow.pojo.vo.FlowModelVO;
import com.ruoyi.workflow.pojo.vo.ProcessDefinitionVO;
import com.ruoyi.workflow.service.IFlowModelService;
import com.ruoyi.workflow.service.IPearlFlowFormService;
import com.ruoyi.workflow.util.ModelElementValidateUtils;
import com.ruoyi.workflow.validation.UserTaskValidator;
import org.apache.commons.io.IOUtils;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.exception.NotFoundException;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.repository.UpdateProcessDefinitionSuspensionStateBuilder;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.xml.validation.ModelElementValidator;
import org.camunda.bpm.model.xml.validation.ValidationResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 流程模型扩展Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-11
 */
@Service
public class FlowModelServiceImpl implements IFlowModelService {

    @Autowired
    private FlowModelMapper flowModelMapper;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private IPearlFlowFormService flowFormService;


    public List<ProcessDefinitionVO> list(ModelQuery params) throws XMLStreamException {
        // 1. 定义条件
        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery();
        if (StrUtil.isNotEmpty(params.getName())) {
            query.processDefinitionName(params.getName()); // 名称
        }
        if (params.isLatestVersion()) {
            query.latestVersion();// 最后一个版本
        }
        // 2. 执行查询
        List<ProcessDefinition> list = query.list();
        // 3. 对象转换
        List<ProcessDefinitionVO> result = new ArrayList<>();
        if (CollUtil.isNotEmpty(list)) {
            for (ProcessDefinition processDefinition : list) {
                ProcessDefinitionVO vo = new ProcessDefinitionVO();
                vo.setKey(processDefinition.getKey());
                vo.setVersion(processDefinition.getVersion());
                vo.setHistoryLevel(processDefinition.getHistoryTimeToLive());
                vo.setHistoryTimeToLive(processDefinition.getHistoryTimeToLive());
                vo.setVersion(processDefinition.getVersion());
                result.add(vo);
            }
        }
        return result;
    }

    /**
     * 查询流程模型扩展
     *
     * @param id 流程模型扩展主键
     * @return 流程模型扩展
     */
    @Override
    public FlowModel selectFlowModelById(Long id) {
        return flowModelMapper.selectFlowModelById(id);
    }

    /**
     * 查询流程模型扩展列表
     *
     * @param flowModel 流程模型扩展
     * @return 流程模型扩展
     */
    @Override
    public List<FlowModelVO> selectFlowModelList(FlowModel flowModel) {
        return flowModelMapper.selectFlowModelList(flowModel);
    }

    /**
     * 新增流程模型扩展
     *
     * @param flowModel 流程模型扩展
     * @return 结果
     */
    @Override
    public Long insertFlowModel(FlowModel flowModel) {
        if (flowModel.getId() != null) {
            // 新增待发布模型
            FlowModel selectFlowModelById = flowModelMapper.selectFlowModelById(flowModel.getId());
            selectFlowModelById.setEditorXml(flowModel.getEditorXml());
            if (FlowModelStateEnum.PUBLISHED.getCode().equals(selectFlowModelById.getModelState())) {
                // 当前已发布，说明是新版（新增一条）
                selectFlowModelById.setId(null);
                selectFlowModelById.setDefVersion(selectFlowModelById.getDefVersion() + 1);
                selectFlowModelById.setModelState(FlowModelStateEnum.WAIT_PUBLISH.getCode());
                selectFlowModelById.setCreateBy(SecurityUtils.getUsername());
                selectFlowModelById.setCreateTime(DateUtils.getNowDate());
                flowModelMapper.insertFlowModel(selectFlowModelById);
            } else {
                // 未发布 直接修改
                selectFlowModelById.setModelState(FlowModelStateEnum.WAIT_PUBLISH.getCode());
                flowModelMapper.updateFlowModel(selectFlowModelById);
            }
        } else {
            // 新增草稿
            flowModel.setModelState(FlowModelStateEnum.DRAFT.getCode());
            flowModel.setCreateBy(SecurityUtils.getUsername());
            flowModel.setCreateTime(DateUtils.getNowDate());
            flowModel.setDefVersion(1);
            flowModel.setDefKey(UUID.fastUUID().toString()); // 设置一个临时的KEY，便于查询
            flowModelMapper.insertFlowModel(flowModel);
        }
        return flowModel.getId();
    }

    /**
     * 修改流程模型扩展
     *
     * @param flowModel 流程模型扩展
     * @return 结果
     */
    @Override
    public int updateFlowModel(FlowModel flowModel) {
        flowModel.setUpdateTime(DateUtils.getNowDate());
        return flowModelMapper.updateFlowModel(flowModel);
    }

    /**
     * 批量删除流程模型扩展
     *
     * @param ids 需要删除的流程模型扩展主键
     * @return 结果
     */
    @Override
    public int deleteFlowModelByIds(Long[] ids) {
        return flowModelMapper.deleteFlowModelByIds(ids);
    }

    /**
     * 删除流程模型扩展信息
     *
     * @param id 流程模型扩展主键
     * @return 结果
     */
    @Override
    @Transactional
    public boolean deleteFlowModelById(Long id) {
        // 1. ID 查询扩展表
        FlowModel flowModel = selectFlowModelById(id);
        if (null == flowModel) {
            throw new ServiceException("当前模型不存在");
        }
        // 2. 根据KEY删除，不是级联操作（存在运行时实例时会报错）
        // 2.1 定义存在 & 不存在流程实例 时执行删除
        // 查询定义
        long count = repositoryService.createProcessDefinitionQuery().processDefinitionKey(flowModel.getDefKey()).count();
        if (count > 0) {
            // 查询流程实例....（后续实现，会使用自定义流程实例表）
            // 删除定义
            repositoryService.deleteProcessDefinitions().byKey(flowModel.getDefKey()).delete();
        }
        //  public void deleteProcessDefinition(String processDefinitionId, boolean cascade, boolean skipCustomListeners, boolean skipIoMappings)
        //repositoryService.deleteProcessDefinition(flowModel.getDefId(),true,false,false);
        // 2.2 删除扩展表，可以做成逻辑删除
        flowModelMapper.deleteFlowModelByKey(flowModel.getDefKey());
        return true;
    }

    @Override
    public void publish(Long id) {
        // 1. 查询自定义模型表
        FlowModel flowModel = flowModelMapper.selectFlowModelById(id);

        // 2. 校验
        if (null == flowModel) {
            throw new ServiceException("当前模型不存在");
        }
        String editorXml = flowModel.getEditorXml(); // 未发布的在编模型
        if (StrUtil.isEmpty(editorXml)) {
            throw new ServiceException("模型文件不存在");
        }

        // 3. 读取为模型对象并校验
        InputStream inputStream = new ByteArrayInputStream(editorXml.getBytes());
        BpmnModelInstance bpmnModelInstance = Bpmn.readModelFromStream(inputStream);
        ModelElementValidateUtils.validate(bpmnModelInstance); // 自定义校验

        // 4. 部署
        Deployment deployment = repositoryService.createDeployment()
                .name(flowModel.getName())
                .tenantId(flowModel.getTenantId())
                .addModelInstance(flowModel.getDefKey() + ".bpmn", bpmnModelInstance)
                .deploy();

        // 5. 修改扩展表
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId()).singleResult();
        flowModel.setSuspensionState(SuspensionStateEnum.ACTIVATION.getCode());
        flowModel.setModelState(FlowModelStateEnum.PUBLISHED.getCode());
        flowModel.setDefId(processDefinition.getId());
        flowModel.setDefKey(processDefinition.getKey());
        flowModel.setUpdateTime(DateUtils.getNowDate());
        flowModel.setEditorXml("");
        flowModel.setDefVersion(processDefinition.getVersion());
        flowModel.setPublishTime(DateUtils.getNowDate());
        flowModel.setUpdateBy(SecurityUtils.getUsername());
        flowModelMapper.updateFlowModel(flowModel);
    }

    @Override
    public String getXML(Long id) throws IOException {
        FlowModel flowModel = flowModelMapper.selectFlowModelById(id);
        // 草稿
        Integer modelState = flowModel.getModelState();
        if (FlowModelStateEnum.DRAFT.getCode().equals(modelState)) {
            return StrUtil.EMPTY;
        }
        // 未发布
        String editorXml = flowModel.getEditorXml();
        if (StrUtil.isNotEmpty(editorXml)) {
            return editorXml;
        }
        // 已发布
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId(flowModel.getDefId())
                .singleResult();
        InputStream inputStream = repositoryService.getResourceAsStream(processDefinition.getDeploymentId(), processDefinition.getResourceName());
        return IOUtils.toString(inputStream, StandardCharsets.UTF_8);
    }

    @Override
    @Transactional
    public void suspension(SuspensionModelParams params) {
        FlowModel flowModel = flowModelMapper.selectFlowModelById(params.getModelId());
        if (null == flowModel) {
            throw new ServiceException("当前模型不存在");
        }
        // 根据Key，挂起或激活所有流程
        long count = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(flowModel.getDefKey())
                .count(); // 查询是否存在流程定义
        if (count > 0) {
            // 存在定义
            UpdateProcessDefinitionSuspensionStateBuilder stateBuilder = repositoryService.updateProcessDefinitionSuspensionState().
                    byProcessDefinitionKey(flowModel.getDefKey()).includeProcessInstances(true);
            // 挂起 or 激活
            if (SuspensionStateEnum.ACTIVATION.getCode().equals(params.getSuspensionState())) {
                stateBuilder.activate();
            } else if (SuspensionStateEnum.SUSPENSION.getCode().equals(params.getSuspensionState())) {
                stateBuilder.suspend();
            } else {
                throw new ServiceException("参数错误");
            }
        }
        // 扩展表
        flowModelMapper.updateFlowModelSuspensionStateByKey(flowModel.getDefKey(),params.getSuspensionState());
    }


    @Override
    public PearlFlowForm getStartFormById(Long id) {
        FlowModel flowModel = flowModelMapper.selectFlowModelById(id);
        if (null == flowModel) {
            throw new ServiceException("当前模型不存在");
        }

        Long formId = flowModel.getFormId();
        if (ObjectUtil.isNull(formId)) {
            throw new ServiceException("当前模型未绑定表单");
        }

        PearlFlowForm form = flowFormService.selectPearlFlowFormById(formId);
        if (ObjectUtil.isNull(form)) {
            throw new ServiceException("当前表单不存在");
        }

        return form;
    }

    @Override
    public void validate(FlowModel flowMode) {
        // 1. XML
        String editorXml = flowMode.getEditorXml();
        if (StrUtil.isEmpty(editorXml)) {
            throw new FlowModelException("XML不能为空");
        }

        // 2. 读取为模型对象并校验
        InputStream inputStream = new ByteArrayInputStream(editorXml.getBytes());
        BpmnModelInstance bpmnModelInstance = Bpmn.readModelFromStream(inputStream);
        ModelElementValidateUtils.validate(bpmnModelInstance); // 自定义校验
    }

    @Override
    public FlowModelDetailVO selectFlowModelDetailById(Long id) {
        // 1. 扩展表
        FlowModel flowModel = selectFlowModelById(id);
        FlowModelDetailVO detailVO = BeanUtil.copyProperties(flowModel, FlowModelDetailVO.class);

        // 2. 不是草稿或未发布的
        Integer modelState = flowModel.getModelState();
        if (!Objects.equals(modelState, FlowModelStateEnum.DRAFT.getCode())
                && !Objects.equals(modelState, FlowModelStateEnum.WAIT_PUBLISH.getCode())) {
            // 2.1 查询 act_re_procdef 表
            ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                    .processDefinitionId(flowModel.getDefId())
                    .singleResult();
            String resourceName = processDefinition.getResourceName();
            Integer historyTimeToLive = processDefinition.getHistoryTimeToLive();
            detailVO.setHistoryTimeToLive(historyTimeToLive);
            // 2.2 查詢部署表
            Deployment deployment = repositoryService.createDeploymentQuery()
                    .deploymentId(processDefinition.getDeploymentId())
                    .singleResult();
            Date deploymentTime = deployment.getDeploymentTime();
        }
        return detailVO;
    }

    @Override
    public List<CanBeStaredFlowModelVO> listCanBeStarted(String name, Long categoryId) {
        // 查询条件
        FlowModel flowModel = new FlowModel();
        flowModel.setCategoryId(categoryId);
        flowModel.setName(name);
        flowModel.setSuspensionState(SuspensionStateEnum.ACTIVATION.getCode()); // 激活
        flowModel.setModelState(FlowModelStateEnum.PUBLISHED.getCode()); // 已发布
        // 查询模型扩展表
        List<FlowModelVO> flowModelVOList = flowModelMapper.selectFlowModelList(flowModel);
        return BeanUtil.copyToList(flowModelVOList, CanBeStaredFlowModelVO.class);
    }
}
