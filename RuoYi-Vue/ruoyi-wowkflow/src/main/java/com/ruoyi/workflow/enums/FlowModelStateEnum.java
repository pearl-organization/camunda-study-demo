package com.ruoyi.workflow.enums;

/**
 * 流程模型状态
 *
 * @author TD
 * @version 1.0
 * @date 2023/11/29
 */
public enum FlowModelStateEnum {
    DRAFT(1, "草稿"), // 已建立流程信息，但未建流程模型（未设计流程图）
    WAIT_PUBLISH(2, "待发布"), // 已建立流程信息、流程模型（已设计流程图），但未部署
    PUBLISHED(3, "已发布"),// 已建立流程信息、流程模型，并已部署
    ENABLED(4, "停用"); // 已发布后，可设置停用、开启

    private Integer code;
    private String msg;

    FlowModelStateEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
