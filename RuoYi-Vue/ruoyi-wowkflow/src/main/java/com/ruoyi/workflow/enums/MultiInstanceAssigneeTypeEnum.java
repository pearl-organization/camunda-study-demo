package com.ruoyi.workflow.enums;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/3
 */
public enum MultiInstanceAssigneeTypeEnum {


    // 单例
    INITIATOR(1, "发起人自己");

    private Integer code;
    private String msg;

    MultiInstanceAssigneeTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
