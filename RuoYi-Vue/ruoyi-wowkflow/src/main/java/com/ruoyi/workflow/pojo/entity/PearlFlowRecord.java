package com.ruoyi.workflow.pojo.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 流程流传记录对象 pearl_flow_record
 * 
 * @author ruoyi
 * @date 2024-06-07
 */

public class PearlFlowRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 流程实例id */
    private Long insId;

    /** 任务名称 */
    @Excel(name = "任务名称")
    private String taskName;

    /** 处理人用户ID */
    @Excel(name = "处理人用户ID")
    private String handlerUserId;

    /** 处理人姓名 */
    @Excel(name = "处理人姓名")
    private String handlerUserName;

    /** 处理动作 */
    @Excel(name = "处理动作")
    private String handlerAction;

    /** 审批意见 */
    @Excel(name = "审批意见")
    private String comment;

    /** 租户ID */
    @Excel(name = "租户ID")
    private String tenantId;

    /** 删除标志 */
    private Integer delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setInsId(Long insId) 
    {
        this.insId = insId;
    }

    public Long getInsId() 
    {
        return insId;
    }
    public void setTaskName(String taskName) 
    {
        this.taskName = taskName;
    }

    public String getTaskName() 
    {
        return taskName;
    }
    public void setHandlerUserId(String handlerUserId) 
    {
        this.handlerUserId = handlerUserId;
    }

    public String getHandlerUserId() 
    {
        return handlerUserId;
    }
    public void setHandlerUserName(String handlerUserName) 
    {
        this.handlerUserName = handlerUserName;
    }

    public String getHandlerUserName() 
    {
        return handlerUserName;
    }
    public void setHandlerAction(String handlerAction) 
    {
        this.handlerAction = handlerAction;
    }

    public String getHandlerAction() 
    {
        return handlerAction;
    }
    public void setComment(String comment) 
    {
        this.comment = comment;
    }

    public String getComment() 
    {
        return comment;
    }
    public void setTenantId(String tenantId) 
    {
        this.tenantId = tenantId;
    }

    public String getTenantId() 
    {
        return tenantId;
    }
    public void setDelFlag(Integer delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("insId", getInsId())
            .append("taskName", getTaskName())
            .append("handlerUserId", getHandlerUserId())
            .append("handlerUserName", getHandlerUserName())
            .append("handlerAction", getHandlerAction())
            .append("comment", getComment())
            .append("tenantId", getTenantId())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
