package com.ruoyi.workflow.constant;

/**
 * 多实例节点流程变量（org.camunda.bpm.engine.impl.bpmn.behavior.MultiInstanceActivityBehavior）
 *
 * @author TD
 * @version 1.0
 * @date 2024/1/3
 */
public class MultiInstanceActivityConstants {

    /**
     * 处理人集合
     */
    public static final String ASSIGNEE_LIST = "assigneeList";

    /**
     * 处理人
     */
    public static final String ASSIGNEE = "assignee";

    /**
     * 实例总数
     */
    public static final String NUMBER_OF_INSTANCES = "nrOfInstances";

    /**
     * 当前未完成的实例数量
     */
    public static final String NUMBER_OF_ACTIVE_INSTANCES = "nrOfActiveInstances";

    /**
     * 当前已完成的实例数量
     */
    public static final String NUMBER_OF_COMPLETED_INSTANCES = "nrOfCompletedInstances";

    /**
     * 循环计数器
     */
    public static final String LOOP_COUNTER = "loopCounter";



    /**
     *
     * 使用说明	按照BPMN2.0规范的要求，用于为每个实例创建执行的父执行，会提供下列变量:
     * collection(集合变量)	传入List参数, 一般为用户ID集合
     * elementVariable(元素变量)	List中单个参数的名称
     * loopCardinality(基数)	List循环次数
     * isSequential(串并行)	Parallel: 并行多实例，Sequential: 串行多实例
     * completionCondition(完成条件)	任务出口条件
     * nrOfInstances(实例总数)	实例总数
     * nrOfActiveInstances(未完成数)	当前活动的（即未完成的），实例数量。对于顺序多实例，这个值总为1
     * nrOfCompletedInstances(已完成数)	已完成的实例数量
     * loopCounter	给定实例在for-each循环中的index
     * 会签一人通过表达式
     *
     * **会签：**在一个流程中的某一个 Task 上，这个 Task 需要多个用户审批，当多个用户全部审批通过，或者多个用户中的某几个用户审批通过，就算通过。
     * 例如：之前的请假流程，假设这个请假流程需要组长和经理都审批了，才算审批通过，那么就需要设置这个 Task 是会签节点。
     *
     * **或签：**意思就是 A 的请假流程提交给 B、C、D，但是并不需要 B/C/D 同时审批通过
     * ，只需要 B/C/D 中的任意一个审批即可，这就是或签，注意，我这里的表述，
     * 只需要 B/C/D 任意一个审批即可，这个审批即可以是审批通过，也可以是审批拒绝，反正只要审批，这个 UserTask 就算完成了。

     *
     * 1.默认为“或签”方式
     * 审批人无论选择一个人还是多人，只要有一个人审批即可，其他成员不需再审批。
     * 会签指一个事项需要多个审批人同时处理,并且需所有人审批同意。 或签指一个事项需要多个审批人同时处理,只需一人审批同意即可。
     * 2.会签(所有审批人通过)
     * 通过： 所有审批人必须都审批通过，才会执行下一节点
     * 驳回： 有任意一人驳回即全部驳回
     *
     * 3.会签(通过只需一人)
     * 通过： 只需一人审批即可，其他人无需再审批
     * 驳回： 一人驳回，即全部驳回
     *
     * 4.会签(半数通过)
     * 通过： 根据配置人员总数，有一半人审批通过即可 其他成员无需再审批
     * 驳回： 一人驳回，即全部驳回
     *
     * 会签(按比例投票)
     * 通过： 根据配置人员总数，达到配置的人员比例即可，其他成员无需再审批
     * 驳回： 一人驳回，即全部驳回
     *
     * 会签(自定义)：可使用后台表达式，返回会签人员
     */

    //    dic: [{label: '或签（一个通过即可）', value: "${nrOfCompletedInstances >= 1}"},
    //    {label: '会签（需全部通过）', value: "${nrOfInstances == nrOfCompletedInstances}"},
    //    {label: '会签（半数以上通过）', value: "${nrOfCompletedInstances/nrOfInstances >= 0.5 }"}],

    /**
     * 或签（一个通过即可）
     */
    public static final String COMPLETE_CONDITION_HUO_QIAN = "${nrOfCompletedInstances >= 1}";

    /**
     * 会签（需全部通过）
     */
    public static final String COMPLETE_CONDITION_HUI_QIAN_ALL_PASSED  = "${nrOfInstances == nrOfCompletedInstances}";

    /**
     * 会签（半数通过）
     */
    public static final String COMPLETE_CONDITION_HUI_QIAN_HALF = "${nrOfCompletedInstances/nrOfInstances >= 0.5}";

}
