package com.ruoyi.workflow.constant;

/**
 * 变量名常量
 *
 * @author TD
 * @version 1.0
 * @date 2024/1/15
 */
public class VariablesConstants {

    // 流程实例
    /**
     * 流程发起人
     */
    public static final String INITIATOR = "initiator";

    /**
     * 自定义模型ID
     */
    public static final String MODEL_ID = "modelId";

    /**
     * 表单ID
     */
    public static final String FORM_ID = "formId";

    /**
     * 主题
     */
    public static final String TITLE = "title";

    /**
     * 表单数据
     */
    public static final String FORM_VARS = "variables";

    // 用户任务
}
