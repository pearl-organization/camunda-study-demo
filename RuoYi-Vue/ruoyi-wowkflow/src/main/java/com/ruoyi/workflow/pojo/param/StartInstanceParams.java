package com.ruoyi.workflow.pojo.param;

import lombok.Data;

import java.util.Map;

/**
 * @author TD
 * @version 1.0
 * @date 2023/12/19
 */
@Data
public class StartInstanceParams {

    // 模型ID
    private Long modelId;

    // 变量
    private Map<String, Object> variables;

    // 业务唯一标识
    private String businessKey;

    // 业务主体
    private String title;
}
