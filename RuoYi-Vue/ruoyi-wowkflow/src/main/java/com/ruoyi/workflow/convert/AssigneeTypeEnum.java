package com.ruoyi.workflow.convert;

/**
 * 审批人类型
 *
 * @author TD
 * @version 1.0
 * @date 2023/11/29
 */
public enum AssigneeTypeEnum {
    // 指定单个成员、部门主管、上级领导、发起人自己、部门负责人
    // 指定多个成员、角色、部门

    // 单例
    INITIATOR(1, "发起人自己"),
    SINGLE_USER(1, "指定单个成员"),
    SUPERIOR_LEADER(1, "部门负责人"),
    DEPT_MANAGER(1, "部门主管"),

    // 多实例
    MULTIPLE_USER(1, "指定多个成员"),

    ROLE(1, "角色"),

    DEPT(3, "部门");

    private Integer code;
    private String msg;

    AssigneeTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
