package com.ruoyi.workflow.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.workflow.mapper.PearlFlowFormMapper;
import com.ruoyi.workflow.pojo.entity.PearlFlowForm;
import com.ruoyi.workflow.service.IPearlFlowFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 流程单Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
@Service
public class PearlFlowFormServiceImpl implements IPearlFlowFormService
{
    @Autowired
    private PearlFlowFormMapper pearlFlowFormMapper;

    /**
     * 查询流程单
     * 
     * @param id 流程单主键
     * @return 流程单
     */
    @Override
    public PearlFlowForm selectPearlFlowFormById(Long id)
    {
        return pearlFlowFormMapper.selectPearlFlowFormById(id);
    }

    /**
     * 查询流程单列表
     * 
     * @param pearlFlowForm 流程单
     * @return 流程单
     */
    @Override
    public List<PearlFlowForm> selectPearlFlowFormList(PearlFlowForm pearlFlowForm)
    {
        return pearlFlowFormMapper.selectPearlFlowFormList(pearlFlowForm);
    }

    /**
     * 新增流程单
     * 
     * @param pearlFlowForm 流程单
     * @return 结果
     */
    @Override
    public int insertPearlFlowForm(PearlFlowForm pearlFlowForm)
    {
        pearlFlowForm.setCreateTime(DateUtils.getNowDate());
        return pearlFlowFormMapper.insertPearlFlowForm(pearlFlowForm);
    }

    /**
     * 修改流程单
     * 
     * @param pearlFlowForm 流程单
     * @return 结果
     */
    @Override
    public int updatePearlFlowForm(PearlFlowForm pearlFlowForm)
    {
        return pearlFlowFormMapper.updatePearlFlowForm(pearlFlowForm);
    }

    /**
     * 批量删除流程单
     * 
     * @param ids 需要删除的流程单主键
     * @return 结果
     */
    @Override
    public int deletePearlFlowFormByIds(Long[] ids)
    {
        return pearlFlowFormMapper.deletePearlFlowFormByIds(ids);
    }

    /**
     * 删除流程单信息
     * 
     * @param id 流程单主键
     * @return 结果
     */
    @Override
    public int deletePearlFlowFormById(Long id)
    {
        return pearlFlowFormMapper.deletePearlFlowFormById(id);
    }
}
