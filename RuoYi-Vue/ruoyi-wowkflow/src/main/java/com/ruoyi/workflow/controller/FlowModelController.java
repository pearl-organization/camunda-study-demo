package com.ruoyi.workflow.controller;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.workflow.pojo.entity.FlowModel;
import com.ruoyi.workflow.pojo.entity.PearlFlowForm;
import com.ruoyi.workflow.pojo.param.SuspensionModelParams;
import com.ruoyi.workflow.pojo.vo.CanBeStaredFlowModelVO;
import com.ruoyi.workflow.pojo.vo.FlowModelVO;
import com.ruoyi.workflow.service.IFlowModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.page.TableDataInfo;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 流程模型扩展Controller
 *
 * @author ruoyi
 * @date 2023-12-11
 */
@RestController
@RequestMapping("/workflow/model")
public class FlowModelController extends BaseController {


    @Autowired
    private IFlowModelService flowModelService;

    /**
     * 查询流程模型扩展列表
     */
    @GetMapping("/list")
    public TableDataInfo list(FlowModel pearlFlowModel) {
        startPage();
        List<FlowModelVO> list = flowModelService.selectFlowModelList(pearlFlowModel);
        return getDataTable(list);
    }

    /**
     * 查询流程模型（发起流程）
     */
    @GetMapping("/listCanBeStarted")
    public TableDataInfo listCanBeStarted(String name,  Long categoryId) {
        startPage();
        List<CanBeStaredFlowModelVO> list = flowModelService.listCanBeStarted(name,categoryId);
        return getDataTable(list);
    }

    /**
     * 根据模型ID获取挂载表单数据
     */
    @GetMapping(value = "getStartForm/{id}")
    public AjaxResult getStartForm(@PathVariable("id") Long id) {
        PearlFlowForm form = flowModelService.getStartFormById(id);
        JSONObject jsonObject = JSONObject.parseObject(form.getContent());
        return AjaxResult.success(jsonObject);
    }


    /**
     * 获取流程模型扩展详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(flowModelService.selectFlowModelById(id));
    }

    /**
     * 获取流程模型詳細信息
     */
    @GetMapping(value = "detail/{id}")
    public AjaxResult getDetail(@PathVariable("id") Long id) {
        return success(flowModelService.selectFlowModelDetailById(id));
    }

    /**
     * 新增流程模型扩展
     */
    @PostMapping
    public AjaxResult add(@RequestBody FlowModel pearlFlowModel) {
        Long id = flowModelService.insertFlowModel(pearlFlowModel);
        return AjaxResult.success(id);
    }

    /**
     * 发布模型
     */
    @GetMapping("/publish/{id}")
    public AjaxResult publish(@PathVariable("id") Long id) {
        flowModelService.publish(id);
        return AjaxResult.success();
    }

    /**
     * 查看流程图
     */
    @GetMapping("/getXML/{id}")
    public AjaxResult getXML(@PathVariable("id") Long id) throws IOException {
        String xml = flowModelService.getXML(id);
        Map<String, Object> result = new HashMap();
        result.put("xmlData", xml);
        return AjaxResult.success(result);
    }

    /**
     * 校验XML模型
     */
    @PostMapping("/validate")
    public AjaxResult validate(@RequestBody FlowModel flowMode) throws IOException {
        flowModelService.validate(flowMode);
        return AjaxResult.success("校验通过");
    }


    /**
     * 修改流程模型扩展
     */
    @PutMapping
    public AjaxResult edit(@RequestBody FlowModel pearlFlowModel) {
        return toAjax(flowModelService.updateFlowModel(pearlFlowModel));
    }

    /**
     * 挂起 & 激活 流程模型扩展
     */
    @PostMapping("/suspensionState")
    public AjaxResult suspension(@RequestBody SuspensionModelParams params) {
        flowModelService.suspension(params);
        return AjaxResult.success();
    }

    /**
     * 删除流程模型
     */
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id) {
        return toAjax(flowModelService.deleteFlowModelById(id));
    }
}
