package com.ruoyi.workflow.exception;

import cn.hutool.core.util.StrUtil;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/9
 */
public class FlowTaskException extends RuntimeException {

    private static final long serialVersionUID = 1L;


    public FlowTaskException(String message) {
        super(StrUtil.format("流程任务异常：{}", message));
    }
}
