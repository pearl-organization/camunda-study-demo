package com.ruoyi.workflow.pojo.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 自定义流程实例对象 pearl_flow_instance
 *
 * @author ruoyi
 * @date 2023-12-19
 */
public class FlowInstance extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 发起人
     */
    private Long initiator;

    /**
     * 流程模型ID
     */
    @Excel(name = "流程模型ID")
    private Long modelId;

    /**
     * 表单ID
     */
    private Long formId;

    /**
     * BPMN流程实例ID
     */
    @Excel(name = "BPMN流程实例ID")
    private String instanceId;

    /**
     * 业务主题
     */
    @Excel(name = "业务主题")
    private String title;

    /**
     * 业务表KEY
     */
    @Excel(name = "业务表KEY")
    private String businessKey;

    /**
     * 流程实例状态
     */
    @Excel(name = "流程实例状态")
    private Integer state;

    /**
     * 完成日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "完成日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date completeDate;

    /**
     * 租户ID
     */
    @Excel(name = "租户ID")
    private String tenantId;

    /**
     * 删除标志
     */
    private Integer delFlag;

    public Long getInitiator() {
        return initiator;
    }

    public void setInitiator(Long initiator) {
        this.initiator = initiator;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getState() {
        return state;
    }

    public void setCompleteDate(Date completeDate) {
        this.completeDate = completeDate;
    }

    public Date getCompleteDate() {
        return completeDate;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("modelId", getModelId())
                .append("instanceId", getInstanceId())
                .append("title", getTitle())
                .append("businessKey", getBusinessKey())
                .append("state", getState())
                .append("completeDate", getCompleteDate())
                .append("tenantId", getTenantId())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
