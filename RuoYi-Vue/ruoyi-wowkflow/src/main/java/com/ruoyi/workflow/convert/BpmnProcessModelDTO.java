package com.ruoyi.workflow.convert;

import lombok.Data;

import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/12/26
 */
@Data
public class BpmnProcessModelDTO {

    /**
     * 模型ID
     */
    Long id;

    /**
     * 名称
     */
    String name;

    /**
     * 节点集合
     */
    List<BpmnNodeDTO> nodeList;

    /**
     * 自定义属性：是否允许撤销（保存到自定义模型表）
     */
    String allowRevoked;
}
