package com.ruoyi.workflow.service;

import com.ruoyi.workflow.pojo.entity.PearlFlowRecord;

import java.util.List;


/**
 * 流程流传记录Service接口
 *
 * @author ruoyi
 * @date 2024-01-11
 */
public interface IPearlFlowRecordService {
    /**
     * 查询流程流传记录
     *
     * @param id 流程流传记录主键
     * @return 流程流传记录
     */
    public PearlFlowRecord selectPearlFlowRecordById(Long id);

    /**
     * 查询流程流传记录列表
     *
     * @param pearlFlowRecord 流程流传记录
     * @return 流程流传记录集合
     */
    public List<PearlFlowRecord> selectPearlFlowRecordList(PearlFlowRecord pearlFlowRecord);

    /**
     * 新增流程流传记录
     *
     * @param pearlFlowRecord 流程流传记录
     * @return 结果
     */
    public int insertPearlFlowRecord(PearlFlowRecord pearlFlowRecord);

    /**
     * 修改流程流传记录
     *
     * @param pearlFlowRecord 流程流传记录
     * @return 结果
     */
    public int updatePearlFlowRecord(PearlFlowRecord pearlFlowRecord);

    /**
     * 批量删除流程流传记录
     *
     * @param ids 需要删除的流程流传记录主键集合
     * @return 结果
     */
    public int deletePearlFlowRecordByIds(Long[] ids);

    /**
     * 删除流程流传记录信息
     *
     * @param id 流程流传记录主键
     * @return 结果
     */
    public int deletePearlFlowRecordById(Long id);
}
