package com.ruoyi.workflow.controller;

import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.workflow.pojo.entity.PearlFlowForm;
import com.ruoyi.workflow.service.IPearlFlowFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.page.TableDataInfo;

import java.util.List;

/**
 * 流程单Controller
 *
 * @author ruoyi
 * @date 2023-11-29
 */
@RestController
@RequestMapping("/workflow/form")
public class FlowFormController extends BaseController
{
    @Autowired
    private IPearlFlowFormService pearlFlowFormService;

    /**
     * 查询流程单列表
     */
    @GetMapping("/list")
    public TableDataInfo list(PearlFlowForm pearlFlowForm)
    {
        startPage();
        List<PearlFlowForm> list = pearlFlowFormService.selectPearlFlowFormList(pearlFlowForm);
        return getDataTable(list);
    }

    /**
     * 导出流程单列表
     */
    @Log(title = "流程单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PearlFlowForm pearlFlowForm)
    {
        List<PearlFlowForm> list = pearlFlowFormService.selectPearlFlowFormList(pearlFlowForm);
        ExcelUtil<PearlFlowForm> util = new ExcelUtil<PearlFlowForm>(PearlFlowForm.class);
        util.exportExcel(response, list, "流程单数据");
    }

    /**
     * 获取流程单详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pearlFlowFormService.selectPearlFlowFormById(id));
    }

    /**
     * 新增流程单
     */
    @Log(title = "流程单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PearlFlowForm pearlFlowForm)
    {
        return toAjax(pearlFlowFormService.insertPearlFlowForm(pearlFlowForm));
    }

    /**
     * 修改流程单
     */
    @Log(title = "流程单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PearlFlowForm pearlFlowForm)
    {
        return toAjax(pearlFlowFormService.updatePearlFlowForm(pearlFlowForm));
    }

    /**
     * 删除流程单
     */
    @Log(title = "流程单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(pearlFlowFormService.deletePearlFlowFormById(id));
    }
}
