package com.ruoyi.workflow.enums;

/**
 * @author TD
 * @version 1.0
 * @date 2023/11/29
 */
public enum SuspensionStateEnum {
    ACTIVATION(1, "激活"),
    SUSPENSION(2, "挂起");

    private Integer code;
    private String msg;

    SuspensionStateEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
