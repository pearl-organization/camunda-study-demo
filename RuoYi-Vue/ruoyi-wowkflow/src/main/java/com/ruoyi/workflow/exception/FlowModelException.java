package com.ruoyi.workflow.exception;

import cn.hutool.core.util.StrUtil;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/9
 */
public class FlowModelException extends RuntimeException {

    private static final long serialVersionUID = 1L;


    public FlowModelException(String message) {
        super(StrUtil.format("流程模型异常：{}",message));
    }
}
