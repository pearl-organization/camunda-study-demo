package com.ruoyi.workflow.convert;

import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;

import java.util.Map;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/3
 */
public class MyTaskListener implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        String eventName = delegateTask.getEventName();

        String assignee = delegateTask.getAssignee();

        String processInstanceId = delegateTask.getProcessInstanceId();

        Map<String, Object> variables = delegateTask.getVariables();

    }
}
