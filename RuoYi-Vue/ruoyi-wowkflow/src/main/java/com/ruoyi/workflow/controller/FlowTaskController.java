package com.ruoyi.workflow.controller;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.workflow.pojo.entity.FlowModel;
import com.ruoyi.workflow.pojo.entity.PearlFlowForm;
import com.ruoyi.workflow.pojo.param.SuspensionModelParams;
import com.ruoyi.workflow.pojo.param.TaskHandleParams;
import com.ruoyi.workflow.pojo.query.MyTodoTaskQuery;
import com.ruoyi.workflow.pojo.vo.FlowModelVO;
import com.ruoyi.workflow.pojo.vo.MyTodoTaskVO;
import com.ruoyi.workflow.service.IFlowModelService;
import com.ruoyi.workflow.service.IFlowTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 流程模型扩展Controller
 *
 * @author ruoyi
 * @date 2023-12-11
 */
@RestController
@RequestMapping("/workflow/task")
public class FlowTaskController extends BaseController {


    @Autowired
    private IFlowModelService flowModelService;

    @Autowired
    private IFlowTaskService flowTaskService;

    /**
     * 我的待办
     */
    @GetMapping("/list/myTodo")
    public TableDataInfo myTodo(MyTodoTaskQuery myTodoTaskQuery) {
        startPage();
        List<MyTodoTaskVO> myTodoTaskVOS = flowTaskService.myTodo(myTodoTaskQuery);
        return getDataTable(myTodoTaskVOS);
    }

    /**
     * 我的已办
     */
    @GetMapping("/list/myCompleted")
    public TableDataInfo myCompleted(MyTodoTaskQuery myTodoTaskQuery) {
        startPage();
        List<MyTodoTaskVO> myTodoTaskVOS = flowTaskService.myCompleted(myTodoTaskQuery);
        return getDataTable(myTodoTaskVOS);
    }



    /**
     * 审批同意
     */
    @PostMapping("/agree")
    public AjaxResult agree(@RequestBody TaskHandleParams params) {
        flowTaskService.agree(params);
        return AjaxResult.success();
    }

    /**
     * 审批拒绝
     */
    @PostMapping("/refuse")
    public AjaxResult refuse(@RequestBody TaskHandleParams params) {
        flowTaskService.refuse(params);
        return AjaxResult.success();
    }

    /**
     * 审批转办
     */
    @PostMapping("/transfer")
    public AjaxResult transfer(@RequestBody TaskHandleParams params) {
        flowTaskService.transfer(params);
        return AjaxResult.success();
    }

    /**
     * 审批委派
     */
    @PostMapping("/delegate")
    public AjaxResult delegate(@RequestBody TaskHandleParams params) {
        flowTaskService.delegate(params);
        return AjaxResult.success();
    }



    /**
     * 根据模型ID获取挂载表单数据
     */
    @GetMapping(value = "getStartForm/{id}")
    public AjaxResult getStartForm(@PathVariable("id") Long id) {
        PearlFlowForm form = flowModelService.getStartFormById(id);
        JSONObject jsonObject = JSONObject.parseObject(form.getContent());
        return AjaxResult.success(jsonObject);
    }

    /**
     * 获取流程模型扩展详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(flowModelService.selectFlowModelById(id));
    }

    /**
     * 新增流程模型扩展
     */
    @PostMapping
    public AjaxResult add(@RequestBody FlowModel pearlFlowModel) {
        Long id = flowModelService.insertFlowModel(pearlFlowModel);
        return AjaxResult.success(id);
    }

    /**
     * 发布模型
     */
    @GetMapping("/publish/{id}")
    public AjaxResult publish(@PathVariable("id") Long id) {
        flowModelService.publish(id);
        return AjaxResult.success();
    }

    /**
     * 查看流程图
     */
    @GetMapping("/getXML/{id}")
    public AjaxResult getXML(@PathVariable("id") Long id) throws IOException {
        String xml = flowModelService.getXML(id);
        Map<String, Object> result = new HashMap();
        result.put("xmlData", xml);
        return AjaxResult.success(result);
    }

    /**
     * 修改流程模型扩展
     */
    @PutMapping
    public AjaxResult edit(@RequestBody FlowModel pearlFlowModel) {
        return toAjax(flowModelService.updateFlowModel(pearlFlowModel));
    }

    /**
     * 修改流程模型扩展
     */
    @PostMapping("/suspensionState")
    public AjaxResult suspension(@RequestBody SuspensionModelParams params) {
        flowModelService.suspension(params);
        return AjaxResult.success();
    }

    /**
     * 删除流程模型扩展
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(flowModelService.deleteFlowModelByIds(ids));
    }
}
