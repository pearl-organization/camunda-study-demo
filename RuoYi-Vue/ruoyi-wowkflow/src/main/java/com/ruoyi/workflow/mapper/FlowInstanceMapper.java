package com.ruoyi.workflow.mapper;

import com.ruoyi.workflow.pojo.entity.FlowInstance;
import com.ruoyi.workflow.pojo.param.MyStartInstanceParams;
import com.ruoyi.workflow.pojo.vo.MyStartInstanceVO;

import java.util.List;

/**
 * 自定义流程实例Mapper接口
 *
 * @author ruoyi
 * @date 2023-12-19
 */
public interface FlowInstanceMapper {
    /**
     * 查询自定义流程实例
     *
     * @param id 自定义流程实例主键
     * @return 自定义流程实例
     */
    FlowInstance selectFlowInstanceById(Long id);

    /**
     * 查询自定义流程实例
     *
     * @param insId 自定义流程实例主键
     * @return 自定义流程实例
     */
    FlowInstance selectByInstanceId(String insId);

    /**
     * 查询自定义流程实例列表
     *
     * @param FlowInstance 自定义流程实例
     * @return 自定义流程实例集合
     */
    List<FlowInstance> selectFlowInstanceList(FlowInstance FlowInstance);

    /**
     * 新增自定义流程实例
     *
     * @param FlowInstance 自定义流程实例
     * @return 结果
     */
    int insertFlowInstance(FlowInstance FlowInstance);

    /**
     * 修改自定义流程实例
     *
     * @param FlowInstance 自定义流程实例
     * @return 结果
     */
    int updateFlowInstance(FlowInstance FlowInstance);

    /**
     * 删除自定义流程实例
     *
     * @param id 自定义流程实例主键
     * @return 结果
     */
    int deleteFlowInstanceById(Long id);

    /**
     * 批量删除自定义流程实例
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteFlowInstanceByIds(Long[] ids);

    /**
     * 我发起的流程实例
     *
     * @param params 参数
     * @return 结果
     */
    List<MyStartInstanceVO> selectMyFlowInstanceList(MyStartInstanceParams params);
}
