package com.ruoyi.workflow.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @author TD
 * @version 1.0
 * @date 2024/5/30
 */
@Data
public class MyStartInstanceVO {

    /**
     * ID
     */
    private Long id;

    /**
     * 流程模型ID
     */
    private Long modelId;

    /**
     * 表单ID
     */
    private Long formId;
    /**
     * 流程模型ID
     */
    private String modelName;

    /**
     * 发起人
     */
    private Long initiator;

    /**
     * 业务主题
     */
    private String title;

    /**
     * 流程实例状态
     */
    private Integer state;

    /**
     * 完成日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date completeDate;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
