package com.ruoyi.workflow.listener;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.workflow.constant.VariablesConstants;
import com.ruoyi.workflow.enums.FlowInstanceStateEnum;
import com.ruoyi.workflow.mapper.FlowInstanceMapper;
import com.ruoyi.workflow.pojo.entity.FlowInstance;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.impl.persistence.entity.ExecutionEntity;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.impl.BpmnModelConstants;
import org.camunda.bpm.model.bpmn.instance.UserTask;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 结束流程
 *
 * @author TD
 * @version 1.0
 * @date 2024/1/2
 */
@Slf4j
public class EndProcessExecutionListener implements ExecutionListener {

    public void notify(DelegateExecution delegateExecution) throws Exception {
        // 1. 获取流程信息
        String processInstanceId = delegateExecution.getProcessInstanceId();// 流程实例ID

        // 2. 更新定义流程信息
        FlowInstanceMapper flowInstanceMapper = SpringUtil.getBean(FlowInstanceMapper.class);
        FlowInstance flowInstance = flowInstanceMapper.selectByInstanceId(processInstanceId);
        flowInstance.setState(FlowInstanceStateEnum.PASSED.getCode());
        flowInstance.setUpdateTime(new Date());
        flowInstance.setCompleteDate(new Date());
        flowInstanceMapper.updateFlowInstance(flowInstance);
    }
}
