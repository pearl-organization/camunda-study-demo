package com.ruoyi.workflow.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.workflow.pojo.entity.PearlFlowRecord;
import com.ruoyi.workflow.service.IPearlFlowRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 流程流传记录Controller
 *
 * @author ruoyi
 * @date 2024-01-11
 */
@RestController
@RequestMapping("/workflow/record")
public class FlowRecordController extends BaseController {
    @Autowired
    private IPearlFlowRecordService pearlFlowRecordService;

    /**
     * 查询流程流传记录列表
     */
    @GetMapping("/list")
    public AjaxResult list(PearlFlowRecord pearlFlowRecord) {
        List<PearlFlowRecord> list = pearlFlowRecordService.selectPearlFlowRecordList(pearlFlowRecord);
        return AjaxResult.success(list);
    }

    /**
     * 导出流程流传记录列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, PearlFlowRecord pearlFlowRecord) {
        List<PearlFlowRecord> list = pearlFlowRecordService.selectPearlFlowRecordList(pearlFlowRecord);
        ExcelUtil<PearlFlowRecord> util = new ExcelUtil<PearlFlowRecord>(PearlFlowRecord.class);
        util.exportExcel(response, list, "流程流传记录数据");
    }

    /**
     * 获取流程流传记录详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(pearlFlowRecordService.selectPearlFlowRecordById(id));
    }

    /**
     * 新增流程流传记录
     */
    @PostMapping
    public AjaxResult add(@RequestBody PearlFlowRecord pearlFlowRecord) {
        return toAjax(pearlFlowRecordService.insertPearlFlowRecord(pearlFlowRecord));
    }

    /**
     * 修改流程流传记录
     */
    @PutMapping
    public AjaxResult edit(@RequestBody PearlFlowRecord pearlFlowRecord) {
        return toAjax(pearlFlowRecordService.updatePearlFlowRecord(pearlFlowRecord));
    }

    /**
     * 删除流程流传记录
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(pearlFlowRecordService.deletePearlFlowRecordByIds(ids));
    }
}
