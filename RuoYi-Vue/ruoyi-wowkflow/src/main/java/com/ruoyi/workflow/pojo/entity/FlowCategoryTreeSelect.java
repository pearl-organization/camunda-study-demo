package com.ruoyi.workflow.pojo.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ruoyi.common.core.domain.TreeSelect;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author TD
 * @version 1.0
 * @date 2023/12/11
 */
public class FlowCategoryTreeSelect  implements Serializable {

    public FlowCategoryTreeSelect(FlowCategory flowCategory) {
        this.id = flowCategory.getId();
        this.label = flowCategory.getName();
        this.children = flowCategory.getChildren().stream().map(FlowCategoryTreeSelect::new).collect(Collectors.toList());
    }

    private static final long serialVersionUID = 1L;

    /**
     * 节点ID
     */
    private Long id;

    /**
     * 节点名称
     */
    private String label;

    /**
     * 子节点
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<FlowCategoryTreeSelect> children;

    public FlowCategoryTreeSelect() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<FlowCategoryTreeSelect> getChildren() {
        return children;
    }

    public void setChildren(List<FlowCategoryTreeSelect> children) {
        this.children = children;
    }
}
