package com.ruoyi.workflow.pojo.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * @author TD
 * @version 1.0
 * @date 2023/11/24
 */
@Getter
@Setter
@Schema(description = "列表查询参数 ")
public class ModelQuery {

    @Schema(description = "名称（模糊查询）")
    private String name;

    @Schema(description = "是否只查询最后一个版本")
    private boolean latestVersion;

    @Schema(description = "分类ID")
    private Long categoryId;
}
