package com.ruoyi.workflow.pojo.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 流程单对象 pearl_flow_form
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
public class PearlFlowForm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 表单主键 */
    private Long id;

    /** 表单名称 */
    @Excel(name = "表单名称")
    private String name;

    /** 表单内容 */
    @Excel(name = "表单内容")
    private String content;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("content", getContent())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .toString();
    }
}
