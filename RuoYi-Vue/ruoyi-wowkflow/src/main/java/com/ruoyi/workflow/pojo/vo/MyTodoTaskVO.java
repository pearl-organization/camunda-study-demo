package com.ruoyi.workflow.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.camunda.bpm.engine.form.CamundaFormRef;

import java.util.Date;

/**
 * @author TD
 * @version 1.0
 * @date 2023/12/22
 */
@Data
public class MyTodoTaskVO {

    @Schema(description = "任务ID")
    protected String id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "任务创建时间")
    private Date taskCreateTime;

    @Schema(description = "节点名称")
    private String taskName;

    @Schema(description = "流程模型ID")
    private String modelId;

    @Schema(description = "流程名称")
    private String modelName;

    @Schema(description = "流程版本")
    private Integer modelVersion;

    @Schema(description = "流程实例ID")
    private Long insId;

    @Schema(description = "主题")
    private String title;

    @Schema(description = "发起人")
    private String initiator;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "流程创建时间")
    private Date insCreateTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "截止日期")
    private Date due;






    @Schema(description = "处理人")
    private String assignee;




    @Schema(description = "后续日期")
    private Date followUp;

    @Schema(description = "上次更新任务的日期，每个触发任务更新事件的操作都将更新此属性")
    private Date lastUpdated;

    @Schema(description = "任务的委派状态：PENDING、RESOLVED")
    private String delegationState;

    @Schema(description = "描述")
    private String description;

    @Schema(description = "执行ID")
    private String executionId;

    @Schema(description = "所有者ID")
    private String owner;

    @Schema(description = "父级任务ID")
    private String parentTaskId;

    @Schema(description = "任务优先级")
    private int priority;

    @Schema(description = "流程定义ID")
    private String processDefinitionId;

    @Schema(description = "流程实例ID")
    private String processInstanceId;

    @Schema(description = "任务KEY")
    private String taskDefinitionKey;

    @Schema(description = "CMMN执行ID")
    private String caseExecutionId;

    @Schema(description = "CMMN实例ID")
    private String caseInstanceId;

    @Schema(description = "CMMN定义ID")
    private String caseDefinitionId;

    @Schema(description = "任务是否属于已挂起的流程实例")
    private boolean suspended;

    @Schema(description = "表单KEY")
    private String formKey;

    @Schema(description = "表格特定版本")
    private CamundaFormRef camundaFormRef;

    @Schema(description = "租户ID")
    private String tenantId;
}
