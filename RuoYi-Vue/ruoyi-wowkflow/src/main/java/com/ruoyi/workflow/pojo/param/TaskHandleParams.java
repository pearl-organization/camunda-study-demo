package com.ruoyi.workflow.pojo.param;

import lombok.Data;

/**
 * @author TD
 * @version 1.0
 * @date 2023/12/25
 */
@Data
public class TaskHandleParams {

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 自定义流程实例ID
     */
    private Long id;

    /**
     * 意见
     */
    private String comment;

    /**
     * 动作
     */
    private Integer action;

    /**
     * 转办的用户ID
     */
    private String transferUserId;

    /**
     * 委派的用户ID
     */
    private String delegateUserId;
}
