package com.ruoyi.workflow.convert;

import org.camunda.bpm.model.bpmn.instance.EndEvent;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;
import org.camunda.bpm.model.bpmn.instance.StartEvent;
import org.camunda.bpm.model.bpmn.instance.UserTask;

import java.util.stream.Stream;

/**
 * 流程实例状态
 *
 * @author TD
 * @version 1.0
 * @date 2023/11/29
 */
public enum NodeTypeEnum {
    START_EVENT(1, "开始"),
    USER_TASK(2, "用户任务"),
    END_EVENT(3, "结束"),
    SEQUENCE_FLOW(4, "连线");
    private Integer code;
    private String msg;

    NodeTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static NodeTypeEnum toType(int code) {
        return Stream.of(NodeTypeEnum.values())
                .filter(p -> p.code == code)
                .findAny()
                .orElse(null);
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
