package com.ruoyi.workflow.enums;

/**
 * 流程实例状态
 *

 *
 *
 * @author TD
 * @version 1.0
 * @date 2023/11/29
 */
public enum FlowInstanceStateEnum {
    PROCESSING(1, "审批中"), // 激活状态，正常运行
    PASSED(2, "已通过"), // 正常结束，审批通过

    SUSPENSION(3, "已挂起"), // 实例和任务都被挂起，无法继续执行。
    COMPLETED(3, "已完成"),// 正常运行结束（审批通过）
    REJECTED(4, "已拒绝"), // 被中止（审批不通过）
    REVOKED(5, "已撤销"); // 被主动撤销

    private Integer code;
    private String msg;

    FlowInstanceStateEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
