package com.ruoyi.workflow.util;

import cn.hutool.core.collection.CollUtil;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.impl.BpmnModelConstants;
import org.camunda.bpm.model.bpmn.instance.FlowElement;
import org.camunda.bpm.model.bpmn.instance.LoopCardinality;
import org.camunda.bpm.model.bpmn.instance.MultiInstanceLoopCharacteristics;
import org.camunda.bpm.model.bpmn.instance.UserTask;

import java.io.File;
import java.util.Collection;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/3
 */
public class BpmnUtils {

    public static String aa(FlowElement flowElement) {

/*        flowElement.

                String userType = flowElement.getAttributeValueNs(BpmnModelConstants.CAMUNDA_NS, "userType");
        return userType;*/
        return "";
    }

    public static void main(String[] args) {
        File file = new File("E:\\TD\\project\\study\\camunda-study-demo\\camunda-study01-demo\\src\\main\\resources\\diagrawwwwwwwwwm_2.bpmn");
        BpmnModelInstance bpmnModelInstance = Bpmn.readModelFromFile(file);

        Collection<UserTask> userTasks = bpmnModelInstance.getModelElementsByType(UserTask.class);

        for (UserTask userTask : userTasks) {
            String id = userTask.getAttributeValue("id");
            String incoming = userTask.getAttributeValueNs(BpmnModelConstants.CAMUNDA_NS, "assignee");
            System.out.println("元素名称：" + userTask.getName());
            System.out.println("id：" + id);
            System.out.println("incoming：" + incoming);

            Collection<MultiInstanceLoopCharacteristics> elements = userTask.getChildElementsByType(MultiInstanceLoopCharacteristics.class);
            if (CollUtil.isNotEmpty(elements)){
                for (MultiInstanceLoopCharacteristics element : elements) {
                    LoopCardinality loopCardinality = element.getLoopCardinality();
                }
            }
        }
    }
}
