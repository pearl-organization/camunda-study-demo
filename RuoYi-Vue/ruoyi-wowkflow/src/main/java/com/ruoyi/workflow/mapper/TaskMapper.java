package com.ruoyi.workflow.mapper;

import com.ruoyi.workflow.pojo.query.MyTodoTaskQuery;
import com.ruoyi.workflow.pojo.vo.MyTodoTaskVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 【任务】Mapper接口
 *
 * @author ruoyi
 * @date 2023-12-22
 */
public interface TaskMapper {

    List<MyTodoTaskVO> selectMyTodoTask(@Param("query") MyTodoTaskQuery query);

    List<MyTodoTaskVO> selectMyCompleted(@Param("query") MyTodoTaskQuery query);
}
