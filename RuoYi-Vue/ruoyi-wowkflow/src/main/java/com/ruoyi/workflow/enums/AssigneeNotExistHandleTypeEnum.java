package com.ruoyi.workflow.enums;

/**
 * 处理人不存在时处理方式枚举
 * 审批人为空时:
 * 1. 自动通过
 * 2. 转交给指定人员审批
 * 3. 转交给审批管理员
 * 4. 退回上一节点
 * 5. 跳转到下一节点
 *
 * @author TD
 * @version 1.0
 * @date 2024/1/15
 */
public enum AssigneeNotExistHandleTypeEnum {

    AUTO_PASS(1, "自动通过"),
    TRANSFER_USER(2, "转交给指定人员审批"),
    TRANSFER_PRO_ADMIN(3, "转交给审批管理员"),
    BACK_PREVIOUS_ACTIVITY(4, "退回上一节点"),
    SKIP_PREVIOUS_ACTIVITY(5, "跳转到下一节点");

    private Integer code;
    private String msg;

    AssigneeNotExistHandleTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
