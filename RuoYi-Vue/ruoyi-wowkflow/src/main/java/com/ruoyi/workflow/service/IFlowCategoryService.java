package com.ruoyi.workflow.service;

import com.ruoyi.common.core.domain.TreeSelect;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.workflow.pojo.entity.FlowCategory;
import com.ruoyi.workflow.pojo.entity.FlowCategoryTreeSelect;

import java.util.List;

/**
 * 流程分类Service接口
 * 
 * @author ruoyi
 * @date 2023-12-04
 */
public interface IFlowCategoryService 
{
    /**
     * 查询流程分类
     * 
     * @param id 流程分类主键
     * @return 流程分类
     */
    public FlowCategory selectFlowCategoryById(Long id);

    /**
     * 查询流程分类列表
     * 
     * @param FlowCategory 流程分类
     * @return 流程分类集合
     */
    public List<FlowCategory> selectFlowCategoryList(FlowCategory FlowCategory);

    /**
     * 新增流程分类
     * 
     * @param FlowCategory 流程分类
     * @return 结果
     */
    public int insertFlowCategory(FlowCategory FlowCategory);

    /**
     * 修改流程分类
     * 
     * @param FlowCategory 流程分类
     * @return 结果
     */
    public int updateFlowCategory(FlowCategory FlowCategory);

    /**
     * 批量删除流程分类
     * 
     * @param ids 需要删除的流程分类主键集合
     * @return 结果
     */
    public int deleteFlowCategoryByIds(Long[] ids);

    /**
     * 删除流程分类信息
     * 
     * @param id 流程分类主键
     * @return 结果
     */
    public int deleteFlowCategoryById(Long id);

    /**
     * 查询树结构信息
     *
     * @param flowCategory 分类信息
     * @return 树信息集合
     */
    public List<FlowCategoryTreeSelect> selectTreeList(FlowCategory flowCategory);

}
