package com.ruoyi.workflow.mapper;


import com.ruoyi.workflow.pojo.entity.FlowCategory;

import java.util.List;

/**
 * 流程分类Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-04
 */
public interface FlowCategoryMapper 
{
    /**
     * 查询流程分类
     * 
     * @param id 流程分类主键
     * @return 流程分类
     */
    public FlowCategory selectFlowCategoryById(Long id);

    /**
     * 查询流程分类列表
     * 
     * @param FlowCategory 流程分类
     * @return 流程分类集合
     */
    public List<FlowCategory> selectFlowCategoryList(FlowCategory FlowCategory);

    /**
     * 新增流程分类
     * 
     * @param FlowCategory 流程分类
     * @return 结果
     */
    public int insertFlowCategory(FlowCategory FlowCategory);

    /**
     * 修改流程分类
     * 
     * @param FlowCategory 流程分类
     * @return 结果
     */
    public int updateFlowCategory(FlowCategory FlowCategory);

    /**
     * 删除流程分类
     * 
     * @param id 流程分类主键
     * @return 结果
     */
    public int deleteFlowCategoryById(Long id);

    /**
     * 批量删除流程分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFlowCategoryByIds(Long[] ids);
}
