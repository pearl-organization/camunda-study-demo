package com.ruoyi.workflow.pojo.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 流程模型扩展对象 pearl_flow_model
 * 
 * @author ruoyi
 * @date 2023-12-11
 */
public class FlowModel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 图标 */
    @Excel(name = "图标")
    private String icon;

    /** 流程名称 */
    @Excel(name = "流程名称")
    private String name;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 流程定义ID */
    @Excel(name = "流程定义ID")
    private String defId;

    /** 流程定义KEY */
    @Excel(name = "流程定义KEY")
    private String defKey;

    /** 版本 */
    @Excel(name = "版本")
    private Integer defVersion;

    /** 表单ID */
    @Excel(name = "表单ID")
    private Long formId;

    /** 分类ID */
    @Excel(name = "分类ID")
    private Long categoryId;

    /** 模型编辑状态 */
    @Excel(name = "模型编辑状态")
    private Integer modelState;

    /** 挂起状态 */
    @Excel(name = "挂起状态")
    private Integer suspensionState;

    /** 在编模型 */
    @Excel(name = "在编模型")
    private String editorXml;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 租户ID */
    @Excel(name = "租户ID")
    private String tenantId;

    /** 删除标志 */
    private Integer delFlag;

    /**表单名称 */
    private String formName;

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setDefId(String defId) 
    {
        this.defId = defId;
    }

    public String getDefId() 
    {
        return defId;
    }
    public void setDefKey(String defKey) 
    {
        this.defKey = defKey;
    }

    public String getDefKey() 
    {
        return defKey;
    }

    public Integer getDefVersion() {
        return defVersion;
    }

    public void setDefVersion(Integer defVersion) {
        this.defVersion = defVersion;
    }

    public void setFormId(Long formId)
    {
        this.formId = formId;
    }

    public Long getFormId() 
    {
        return formId;
    }
    public void setCategoryId(Long categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() 
    {
        return categoryId;
    }
    public void setModelState(Integer modelState)
    {
        this.modelState = modelState;
    }

    public Integer getModelState()
    {
        return modelState;
    }
    public void setSuspensionState(Integer suspensionState)
    {
        this.suspensionState = suspensionState;
    }

    public Integer getSuspensionState()
    {
        return suspensionState;
    }
    public void setEditorXml(String editorXml) 
    {
        this.editorXml = editorXml;
    }

    public String getEditorXml() 
    {
        return editorXml;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setTenantId(String tenantId) 
    {
        this.tenantId = tenantId;
    }

    public String getTenantId() 
    {
        return tenantId;
    }
    public void setDelFlag(Integer delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("icon", getIcon())
            .append("name", getName())
            .append("description", getDescription())
            .append("defId", getDefId())
            .append("defKey", getDefKey())
            .append("version", getDefVersion())
            .append("formId", getFormId())
            .append("categoryId", getCategoryId())
            .append("modelState", getModelState())
            .append("suspensionState", getSuspensionState())
            .append("editorXml", getEditorXml())
            .append("publishTime", getPublishTime())
            .append("tenantId", getTenantId())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
