package com.ruoyi.workflow.convert;

import lombok.Data;

import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/12/26
 */
@Data
public class BpmnNodeDTO {

    /**
     * 节点ID
     */
    String id;

    /**
     * 名称
     */
    String name;

    /**
     * 类型
     */
    Integer type;

    /**
     * 是否多实例
     */
    Boolean isMultiInstance;

    /**
     * 用户类型
     */
    Integer assigneeType;

    /**
     * 连线（起点）
     */
    String sequenceFrom;

    /**
     * 连线（结束点）
     */
    String sequenceTo;
}
