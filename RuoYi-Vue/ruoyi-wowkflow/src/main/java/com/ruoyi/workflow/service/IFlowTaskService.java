package com.ruoyi.workflow.service;

import com.ruoyi.workflow.pojo.param.TaskHandleParams;
import com.ruoyi.workflow.pojo.query.MyTodoTaskQuery;
import com.ruoyi.workflow.pojo.vo.MyTodoTaskVO;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 流程模型扩展Service接口
 *
 * @author ruoyi
 * @date 2023-12-11
 */
public interface IFlowTaskService {

    List<MyTodoTaskVO> myTodo(MyTodoTaskQuery query);

    List<MyTodoTaskVO> myCompleted(MyTodoTaskQuery myTodoTaskQuery);

    void agree(TaskHandleParams params);

    void refuse(TaskHandleParams params);

    void transfer(TaskHandleParams params);

    void delegate(TaskHandleParams params);
}
