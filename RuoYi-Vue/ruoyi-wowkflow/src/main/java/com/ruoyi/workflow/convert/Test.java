package com.ruoyi.workflow.convert;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2024/1/2
 */
public class Test {

    public static void main(String[] args) {


        BpmnProcessModelDTO process = new BpmnProcessModelDTO();
        process.setName("测试流程");
        process.setId(123456L);
        process.setAllowRevoked("true");

        List<BpmnNodeDTO> nodeList = new ArrayList<>();

        BpmnNodeDTO startEvent = new BpmnNodeDTO();
        startEvent.setType(NodeTypeEnum.START_EVENT.getCode());
        startEvent.setId("eddedede");


        BpmnNodeDTO userTask = new BpmnNodeDTO();
        userTask.setType(NodeTypeEnum.USER_TASK.getCode());
        userTask.setId("eswdsa");
        userTask.setName("上级领导审批");
        userTask.setAssigneeType(AssigneeTypeEnum.SUPERIOR_LEADER.getCode());

        BpmnNodeDTO endEvent = new BpmnNodeDTO();
        endEvent.setType(NodeTypeEnum.END_EVENT.getCode());
        endEvent.setId("sdscccds");


        BpmnNodeDTO flow1 = new BpmnNodeDTO();
        flow1.setType(NodeTypeEnum.SEQUENCE_FLOW.getCode());
        flow1.setId("bbb");
        flow1.setSequenceFrom("eddedede");
        flow1.setSequenceTo("eswdsa");

        BpmnNodeDTO flow2 = new BpmnNodeDTO();
        flow2.setType(NodeTypeEnum.SEQUENCE_FLOW.getCode());
        flow2.setId("aaa");
        flow2.setSequenceFrom("eswdsa");
        flow2.setSequenceTo("sdscccds");

        nodeList.add(startEvent);
        nodeList.add(userTask);
        nodeList.add(endEvent);
        nodeList.add(flow1);
        nodeList.add(flow2);
        process.setNodeList(nodeList);

        String json = JSONUtil.parseObj(process).toString();
        System.out.println(json);

        ConvertUtils.covert(process);
    }
}
