package com.ruoyi.workflow.pojo.vo;

import cn.hutool.core.collection.CollUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.camunda.bpm.engine.repository.ProcessDefinition;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/9
 */
@Getter
@Setter
@Schema(description = "流程定义展示层对象")
public class ProcessDefinitionVO {

    @Schema(description = "ID")
    private String id;

    @Schema(description = "KEY")
    private String key;

    @Schema(description = "类别")
    private String category;

    @Schema(description = "描述")
    private String description;

    @Schema(description = "名称")
    private String name;

    @Schema(description = "版本")
    private int version;

    @Schema(description = "资源地址")
    private String resource;

    @Schema(description = "部署ID")
    private String deploymentId;

    @Schema(description = "简图")
    private String diagram;

    @Schema(description = "是否暂停")
    private boolean suspended;

    @Schema(description = "租户ID")
    private String tenantId;

    @Schema(description = "版本标记")
    private String versionTag;

    @Schema(description = "历史数据保留时间")
    private Integer historyTimeToLive;

    @Schema(description = "是否开启任务列表")
    private boolean isStartableInTasklist;

    @Schema(description = "历史级别")
    protected Integer historyLevel;

    public static ProcessDefinitionVO fromProcessDefinition(ProcessDefinition definition) {
        ProcessDefinitionVO vo = new ProcessDefinitionVO();
        vo.id = definition.getId();
        vo.key = definition.getKey();
        vo.category = definition.getCategory();
        vo.description = definition.getDescription();
        vo.name = definition.getName();
        vo.version = definition.getVersion();
        vo.resource = definition.getResourceName();
        vo.deploymentId = definition.getDeploymentId();
        vo.diagram = definition.getDiagramResourceName();
        vo.suspended = definition.isSuspended();
        vo.tenantId = definition.getTenantId();
        vo.versionTag = definition.getVersionTag();
        vo.historyTimeToLive = definition.getHistoryTimeToLive();
        vo.isStartableInTasklist = definition.isStartableInTasklist();
        return vo;
    }

    public static List<ProcessDefinitionVO> fromProcessDefinition(List<ProcessDefinition> processDefinitionList) {
        List<ProcessDefinitionVO> result = new ArrayList<>();
        if (CollUtil.isNotEmpty(processDefinitionList)) {
            for (ProcessDefinition processDefinition : processDefinitionList) {
                ProcessDefinitionVO vo = ProcessDefinitionVO.fromProcessDefinition(processDefinition);
                result.add(vo);
            }
        }
        return result;
    }
}
