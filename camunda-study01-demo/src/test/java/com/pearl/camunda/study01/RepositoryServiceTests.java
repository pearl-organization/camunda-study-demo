package com.pearl.camunda.study01;

import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.repository.DeploymentWithDefinitions;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


/**
 * @author TangDan
 * @version 1.0
 * @since 2023/7/17
 */
@SpringBootTest
public class RepositoryServiceTests {

    @Autowired
    private RepositoryService repositoryService;

    @Test
    @DisplayName("简单部署，返回部署对象")
    void deploy() {
        // 1. 简单部署，返回部署对象
        Deployment deploy = repositoryService.createDeployment()
                .name("测试部署") // 部署名称
                .addClasspathResource("leave.bpmn") // Classpath下需要部署的流程文件
                .deploy();// 部署
        System.out.println(deploy.getId() + ":" + deploy.getName());
        // 2. 简单部署，返回当前部署后流程定义的相关信息
        DeploymentWithDefinitions deployWithResult = repositoryService.createDeployment()
                .name("测试部署") // 部署名称
                .addClasspathResource("leave.bpmn") // Classpath下需要部署的流程文件
                .deployWithResult();// 部署
        List<ProcessDefinition> deployedProcessDefinitions = deployWithResult.getDeployedProcessDefinitions();
        for (ProcessDefinition definition : deployedProcessDefinitions) {
            System.out.println(definition.getId()); // ID
            System.out.println(definition.getName());// 名称
            System.out.println(definition.getVersion()); // 版本
            System.out.println(definition.getResourceName()); // 流程文件名
        }
    }

    @Test
    @DisplayName("简单部署，返回当前部署后流程定义的相关信息")
    void deployWithResult() {
        DeploymentWithDefinitions deployWithResult = repositoryService.createDeployment()
                .name("测试部署") // 部署名称
                .addClasspathResource("leave.bpmn") // Classpath下需要部署的流程文件
                .deployWithResult();// 部署
        List<ProcessDefinition> deployedProcessDefinitions = deployWithResult.getDeployedProcessDefinitions();
        for (ProcessDefinition definition : deployedProcessDefinitions) {
            System.out.println(definition.getId()); // ID
            System.out.println(definition.getName());// 名称
            System.out.println(definition.getVersion()); // 版本
            System.out.println(definition.getResourceName()); // 流程文件名
        }
    }
}