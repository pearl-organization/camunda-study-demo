
package com.pearl.camunda.study01.engine;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


/**
 * @author TangDan
 * @version 1.0
 * @since 2023/7/17
 */
@SpringBootTest
public class EngineTests {
    @Autowired
    private ProcessEngine processEngine;

    @Test
    @DisplayName("获取流程引擎")
    void getProcessEngine() {
        String name = processEngine.getName();
        System.out.println(name);
    }
}