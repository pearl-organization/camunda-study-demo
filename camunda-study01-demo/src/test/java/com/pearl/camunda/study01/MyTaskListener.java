package com.pearl.camunda.study01;

import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;

/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 */
@Component
public class MyTaskListener implements TaskListener {
    /**
     * 任务事件侦听器允许对任务事件做出反应（任务已创建、已分配、已完成）
     *
     * @param delegateTask
     */
    @Override
    public void notify(DelegateTask delegateTask) {
        String eventName = delegateTask.getEventName();
        System.out.println("=====================当前事件：" + eventName);


    }
}
