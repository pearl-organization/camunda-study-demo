package com.pearl.camunda.study01;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.task.Task;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author TangDan
 * @version 1.0
 * @since 2023/7/17
 */
@SpringBootTest
public class TaskServiceTests {

    @Autowired
    private TaskService taskService;

    @Test
    @DisplayName("查询待办")
    public void queryTask() {
        // 启动流程后，查询当前流程节点的审批人
        List<Task> list = taskService.createTaskQuery()
                .processInstanceId("f2806cab-2f47-11ee-af4d-00ff045b1bb6")
                .list();
        if (list != null && list.size() > 0) {
            for (Task task : list) {
                System.out.println("id： " + task.getId());
                System.out.println("处理人：" + task.getAssignee());
            }
        }
    }
    @Autowired
    IdentityService identityService;

    /**
     * 完成任务
     */
    @Test
    public void completeTask() {
        // 根据用户找到关联的Task
        identityService.setAuthenticatedUserId("zhangsan");
        Task task = taskService.createTaskQuery()
                .taskId("f2821a5e-2f47-11ee-af4d-00ff045b1bb6")
                .singleResult();
        if (task != null) {
            taskService.complete(task.getId());
            System.out.println("任务审批完成...");
        }
    }
}
