package com.pearl.camunda.study01;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.delegate.TaskListener;

/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 */
@ProcessApplication
public class InvoiceProcessApplication extends ServletProcessApplication {

    public TaskListener getTaskListener() {
        return new TaskListener() {
            public void notify(DelegateTask delegateTask) {
                // handle all Task Events from Invoice Process
                System.out.println("=======================================TaskListener");
            }
        };
    }

    public ExecutionListener getExecutionListener() {
        return new ExecutionListener() {
            public void notify(DelegateExecution execution) throws Exception {
                // handle all Execution Events from Invoice Process
                System.out.println("=======================================ExecutionListener");
            }
        };
    }
}