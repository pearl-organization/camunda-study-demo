package com.pearl.camunda.study01;

/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 */
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.impl.history.event.HistoryEvent;
import org.camunda.bpm.spring.boot.starter.event.ExecutionEvent;
import org.camunda.bpm.spring.boot.starter.event.TaskEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;


@Component
class MyListener {

/*    @EventListener
    public void onTaskEvent(DelegateTask taskDelegate) {
        // handle mutable task event
        System.out.println("=============");
        System.out.println(taskDelegate.getName());
    }*/

    @EventListener
    public void onTaskEvent(TaskEvent taskEvent) {
        String eventName = taskEvent.getEventName();
        if ("assignment".equals(eventName)){
            //  分配任务事件
            System.out.println("分配任务，发送待办信息给消息中心");
            System.out.println("审批人："+taskEvent.getAssignee());
            System.out.println("节点内容："+taskEvent.getName());
            System.out.println("流程模型ID："+taskEvent.getProcessDefinitionId());
            System.out.println("流程模实例ID："+taskEvent.getProcessInstanceId());
            System.out.println("创建时间："+taskEvent.getCreateTime());
        }
    }

    @EventListener
    public void onExecutionEvent(DelegateExecution executionDelegate) {
        // handle mutable execution event
    }

    @EventListener
    public void onExecutionEvent(ExecutionEvent executionEvent) {
        // handle immutable execution event
    }

    @EventListener
    public void onHistoryEvent(HistoryEvent historyEvent) {
        // handle history event
    }

}