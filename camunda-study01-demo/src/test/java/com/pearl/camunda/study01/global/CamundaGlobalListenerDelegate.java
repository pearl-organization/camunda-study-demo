package com.pearl.camunda.study01.global;

/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 */

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 **流程全局事件处理器
 ***
 **/

public class CamundaGlobalListenerDelegate  implements ExecutionListener, TaskListener {
    private static final Logger logger = LoggerFactory.getLogger(CamundaGlobalListenerDelegate.class);

    @Override
    public void notify(DelegateTask delegateTask) {
        logger.info("delegateTask------------------> " + delegateTask.getName()+"----------->"+delegateTask.getEventName()+"----->"+
                delegateTask.getBpmnModelElementInstance().getElementType().getTypeName()+"--->");
    }


    @Override
    public void notify(DelegateExecution execution) throws Exception {
        logger.info("execution------------------> " + execution.getCurrentActivityName()+"----------->"+execution.getEventName()+"----->"+
                execution.getBpmnModelElementInstance().getElementType().getTypeName()+"---->");

    }

}