package com.pearl.camunda.study01.controller;

import cn.hutool.core.collection.CollUtil;
import com.pearl.camunda.study01.common.R;
import com.pearl.camunda.study01.vo.CommentVO;
import com.pearl.camunda.study01.vo.TaskVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.task.Comment;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 */
@RequestMapping("/task")
@RestController
@Tag(name = "任务服务")
public class TaskController {

    @Autowired
    private TaskService taskService;


    @PostMapping("/myTask")
    @Operation(summary = "查询我的待办")
    public R<List<TaskVO>> myTask(String assignee) {
        List<Task> list = taskService.createTaskQuery()
                .taskAssignee(assignee)
                .list();
        List<TaskVO> result = new ArrayList<>();
        if (CollUtil.isNotEmpty(list)) {
            for (Task task : list) {
                TaskVO vo = new TaskVO();
                vo.setId(task.getId());
                vo.setAssignee(task.getAssignee());
                vo.setName(task.getName());
                vo.setCreateTime(task.getCreateTime());
                vo.setProcessDefinitionId(task.getProcessDefinitionId());
                result.add(vo);
            }
        }
        return R.success(result);
    }

    @PostMapping("/deleteTask")
    @Operation(summary = "根据ID删除待办")
    public R<?> deleteTask(String taskId) {
        taskService.deleteTask(taskId);
        return R.success();
    }

    @PostMapping("/complete")
    @Operation(summary = "根据ID完成待办")
    public R<?> complete(String taskId) {
        taskService.complete(taskId);
        return R.success();
    }

    @PostMapping("/completeAndComment")
    @Operation(summary = "根据ID完成待办并添加审批意见")
    @Transactional
    public R<List<TaskVO>> completeAndComment(String taskId, String message) {
        taskService.createComment(taskId, null, message);
        taskService.complete(taskId);
        return R.success();
    }

    @PostMapping("/getTaskComments")
    @Operation(summary = "根据ID获取审批意见")
    public R<List<CommentVO>> getTaskComments(String taskId) {
        List<Comment> taskComments = taskService.getTaskComments(taskId);

        List<CommentVO> list=new ArrayList<>();
        if (CollUtil.isNotEmpty(taskComments)){
            for (Comment taskComment : taskComments) {
                CommentVO vo=new CommentVO();
                vo.setId(taskComment.getId());
                vo.setFullMessage(taskComment.getFullMessage());
                vo.setTaskId(taskComment.getTaskId());
                list.add(vo);
            }
        }
        return R.success(list);
    }
}
