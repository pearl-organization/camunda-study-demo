package com.pearl.camunda.study01.vo;

import lombok.Data;
import org.camunda.bpm.engine.impl.core.variable.scope.VariableStore;
import org.camunda.bpm.engine.impl.persistence.entity.*;

import java.io.Serializable;
import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/2
 */
@Data
public class ProcessInstanceVO  implements Serializable {

    protected boolean shouldQueryForSubprocessInstance = false;
    protected boolean shouldQueryForSubCaseInstance = false;
    protected int cachedEntityState;
    protected int suspensionState;
    protected int revision;
    protected String processDefinitionId;
    protected String activityId;
    protected String activityName;
    protected String processInstanceId;
    protected String parentId;
    protected String superExecutionId;
    protected String rootProcessInstanceId;
    protected String superCaseExecutionId;
}
