package com.pearl.camunda.study01.listiner;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;

import java.util.Map;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/3
 */
public class MyExecutionListener implements ExecutionListener {


    @Override
    public void notify(DelegateExecution execution) throws Exception {
        String eventName = execution.getEventName();
        System.out.println("事件名称：" + eventName);

        String currentActivityId = execution.getCurrentActivityId();
        System.out.println("当前活动ID：" + currentActivityId);

        String currentActivityName = execution.getCurrentActivityName();
        System.out.println("当前活动名称：" + currentActivityName);

        String processInstanceId = execution.getProcessInstanceId();
        System.out.println("流程实例ID：" + processInstanceId);

        String businessKey = execution.getBusinessKey();
        System.out.println("业务键：" + businessKey);

        Map<String, Object> variables = execution.getVariables();



    }
}
