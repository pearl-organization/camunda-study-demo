package com.pearl.camunda.study01.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.pearl.camunda.study01.common.R;
import com.pearl.camunda.study01.vo.ProcessDefinitionVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.camunda.bpm.application.impl.ProcessApplicationReferenceImpl;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.camunda.bpm.engine.repository.*;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.impl.BpmnModelInstanceImpl;
import org.camunda.bpm.spring.boot.starter.SpringBootProcessApplication;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 */
@RequestMapping("/repository")
@RestController
@Tag(name = "仓库服务", description = "仓库服务")
public class RepositoryController {

    @Autowired
    private RepositoryService repositoryService;

    @PostMapping("/deploy")
    @Operation(summary = "BPMN文件部署流程")
    public R<String> deploy(@RequestParam MultipartFile file) throws IOException {
        // 1. 构建部署对象
        DeploymentBuilder builder = repositoryService.createDeployment();
        builder.name("测试部署");
        InputStream inputStream = file.getInputStream();
        builder.addInputStream(file.getOriginalFilename(), inputStream);
        // 2.部署并返回ID
        Deployment deployment = builder.deploy();
        inputStream.close();
        return R.success(deployment.getId());
    }

    @DeleteMapping("/deleteDeployment")
    @Operation(summary = "删除部署")
    public R<String> deleteDeployment(String deploymentId, boolean cascade, boolean skipCustomListeners, boolean skipIoMappings) {
        repositoryService.deleteDeployment(deploymentId, cascade, skipCustomListeners, skipCustomListeners);
        return R.success();
    }

    @DeleteMapping("/deleteProcessDefinition")
    @Operation(summary = "根据ID删除流程定义")
    public R<String> deleteProcessDefinition(String processDefinitionId, boolean cascade, boolean skipCustomListeners, boolean skipIoMappings) {
        repositoryService.deleteProcessDefinition(processDefinitionId, cascade, skipCustomListeners, skipCustomListeners);
        return R.success();
    }

    @DeleteMapping("/deleteProcessDefinitionByKey")
    @Operation(summary = "根据Key删除流程定义")
    public R<String> deleteProcessDefinitionByKey(String key, boolean cascade, boolean skipCustomListeners, boolean skipIoMappings) {
        repositoryService.deleteProcessDefinitions()
                .byKey(key)
                .delete();
        return R.success();
    }

    @DeleteMapping("/deleteProcessDefinitionByIds")
    @Operation(summary = "根据ID集合删除流程定义")
    public R<String> deleteProcessDefinitionByIds(@RequestBody List<String> ids) {
        repositoryService.deleteProcessDefinitions()
                .byIds(ids.toArray(String[]::new)).delete();
        return R.success();
    }

    @GetMapping("/getProcessDefinitionById")
    @Operation(summary = "根据ID查询流程定义")
    public R<ProcessDefinitionVO> getProcessDefinitionById(@RequestParam String processDefinitionId) {
        ProcessDefinition processDefinition = repositoryService.getProcessDefinition(processDefinitionId);
        ProcessDefinitionVO vo = new ProcessDefinitionVO();
        vo.setKey(processDefinition.getKey());
        vo.setVersion(processDefinition.getVersion());
        vo.setHistoryLevel(processDefinition.getHistoryTimeToLive());
        vo.setHistoryTimeToLive(processDefinition.getHistoryTimeToLive());
        vo.setVersion(processDefinition.getVersion());
        return R.success(vo);
    }

    @GetMapping("/query")
    @Operation(summary = "条件查询流程定义")
    public R<List<ProcessDefinitionVO>> query(String name) {
        // 1. 定义条件
        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery();
        query.latestVersion();// 最后一个版本
        List<ProcessDefinition> list = query.list();
        // 2. 对象转换
        List<ProcessDefinitionVO> result = new ArrayList<>();
        if (CollUtil.isNotEmpty(list)) {
            for (ProcessDefinition processDefinition : list) {
                ProcessDefinitionVO vo = new ProcessDefinitionVO();
                vo.setKey(processDefinition.getKey());
                vo.setVersion(processDefinition.getVersion());
                vo.setHistoryLevel(processDefinition.getHistoryTimeToLive());
                vo.setHistoryTimeToLive(processDefinition.getHistoryTimeToLive());
                vo.setVersion(processDefinition.getVersion());
                result.add(vo);
            }
        }
        return R.success(result);
    }

    @PostMapping("/suspendProcessDefinitionByKey")
    @Operation(summary = "根据KEY挂起流程定义")
    public R<?> suspendProcessDefinitionByKey(@RequestParam String processDefinitionKey) {
        repositoryService.suspendProcessDefinitionByKey(processDefinitionKey);
        return R.success();
    }

    @PostMapping("/activateProcessDefinitionByKey")
    @Operation(summary = "根据KEY激活流程定义")
    public R<?> activateProcessDefinitionByKey(@RequestParam String processDefinitionKey) {
        repositoryService.activateProcessDefinitionByKey(processDefinitionKey);
        return R.success();
    }
}
