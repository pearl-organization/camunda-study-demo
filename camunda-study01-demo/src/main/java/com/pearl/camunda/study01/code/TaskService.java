/*
 * Copyright Camunda Services GmbH and/or licensed to Camunda Services GmbH
 * under one or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information regarding copyright
 * ownership. Camunda licenses this file to you under the Apache License,
 * Version 2.0; you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pearl.camunda.study01.code;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.authorization.Permissions;
import org.camunda.bpm.engine.authorization.ProcessDefinitionPermissions;
import org.camunda.bpm.engine.authorization.Resources;
import org.camunda.bpm.engine.authorization.TaskPermissions;
import org.camunda.bpm.engine.exception.NotFoundException;
import org.camunda.bpm.engine.history.UserOperationLogEntry;
import org.camunda.bpm.engine.history.UserOperationLogQuery;
import org.camunda.bpm.engine.task.Attachment;
import org.camunda.bpm.engine.task.Comment;
import org.camunda.bpm.engine.task.DelegationState;
import org.camunda.bpm.engine.task.Event;
import org.camunda.bpm.engine.task.IdentityLink;
import org.camunda.bpm.engine.task.IdentityLinkType;
import org.camunda.bpm.engine.task.NativeTaskQuery;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.camunda.bpm.engine.task.TaskReport;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.value.SerializableValue;
import org.camunda.bpm.engine.variable.value.TypedValue;

/**
 * Service which provides access to {@link Task} and form related operations.
 *
 * @author Tom Baeyens
 * @author Joram Barrez
 * @author Thorben Lindhauer
 */
public interface TaskService {

    /**
     * 此父任务的子任务列表
     */
    List<Task> getSubTasks(String parentTaskId);

    /**
     * 实例化任务报告
     */
    TaskReport createTaskReport();

    /**
     * 报告业务错误的信号，该错误应由流程引擎处理
     *
     */
    void handleBpmnError(String taskId, String errorCode);
    void handleBpmnError(String taskId, String errorCode, String errorMessage);
    void handleBpmnError(String taskId, String errorCode, String errorMessage, Map<String, Object> variables);

    /**
     * 报告升级的信号，该升级应由流程引擎处理
     */
    void handleEscalation(String taskId, String escalationCode);
    void handleEscalation(String taskId, String escalationCode, Map<String, Object> variables);
}
