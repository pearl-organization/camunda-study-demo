package com.pearl.camunda.study01.task;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/3
 */
public class RemittanceServiceTask implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        System.out.println("活动名称" + delegateExecution.getCurrentActivityName());

    }
}
