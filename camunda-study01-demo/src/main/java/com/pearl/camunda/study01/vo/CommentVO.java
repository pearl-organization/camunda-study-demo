package com.pearl.camunda.study01.vo;

import lombok.Data;
import org.camunda.bpm.engine.task.DelegationState;

import java.io.Serializable;
import java.util.Date;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/2
 */
@Data
public class CommentVO implements Serializable {

    protected String id;
    protected String type;
    protected String userId;
    protected Date time;
    protected String taskId;
    protected String processInstanceId;
    protected String action;
    protected String message;
    protected String fullMessage;
    protected String tenantId;
}
