package com.pearl.camunda.study01.doc;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 * http://127.0.0.1:8080/
 */
@Configuration
public class SpringDocConfig {
    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
                .info(new Info().title("工作流")
                        .description("测试 SpringDoc")
                        .version("v1.0.0"));
    }
}
