package com.pearl.camunda.study01.vo;

import lombok.Data;
import org.camunda.bpm.engine.delegate.Expression;
import org.camunda.bpm.engine.impl.db.EnginePersistenceLogger;
import org.camunda.bpm.engine.impl.form.FormDefinition;
import org.camunda.bpm.engine.impl.form.handler.StartFormHandler;
import org.camunda.bpm.engine.impl.persistence.entity.IdentityLinkEntity;
import org.camunda.bpm.engine.impl.task.TaskDefinition;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/1
 */
@Data
public class ProcessDefinitionVO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    protected String key;
    protected int version;
    protected String category;
    protected String deploymentId;
    protected String resourceName;
    protected Integer historyLevel;
    protected String diagramResourceName;
    protected boolean isGraphicalNotationDefined;
    protected boolean hasStartFormKey;
    protected int suspensionState;
    protected String tenantId;
    protected String versionTag;
    protected Integer historyTimeToLive;
    protected boolean isIdentityLinksInitialized;
    protected boolean isStartableInTasklist;
    protected boolean firstVersion;
    protected String previousProcessDefinitionId;
}
