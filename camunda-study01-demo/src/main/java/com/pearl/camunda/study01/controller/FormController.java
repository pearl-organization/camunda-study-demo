package com.pearl.camunda.study01.controller;

import cn.hutool.core.collection.CollUtil;
import com.pearl.camunda.study01.common.R;
import com.pearl.camunda.study01.vo.CommentVO;
import com.pearl.camunda.study01.vo.TaskVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.camunda.bpm.engine.FormService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.StartFormData;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Comment;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 */
@RequestMapping("/form")
@RestController
@Tag(name = "表单服务")
public class FormController {

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private FormService formService;

    @PostMapping("/getStartFormData")
    @Operation(summary = "获取流程开始表单")
    public R< List<FormField>> getStartFormData(String processDefinitionId) {
        // 1. 查看流程
        // 2. 发起流程时，获取当前开启表单
        // 3. 填写表单
        // 4. 发起流程，表述数据提交到变量中
        // 5. 后续表单可以看到提交者数据，进行审核
        // 6. 这些数据都保存在工作流中
        // 7. 用户查看：????
        StartFormData startFormData = formService.getStartFormData(processDefinitionId);
        List<FormField> fieldList = startFormData.getFormFields();
        return R.success(fieldList);
    }



}
