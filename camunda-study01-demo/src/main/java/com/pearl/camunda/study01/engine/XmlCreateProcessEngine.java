package com.pearl.camunda.study01.engine;


import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/1
 */
public class XmlCreateProcessEngine {

    public static void main(String[] args) {
        // camunda.cfg.xml
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        System.out.println(processEngine.getName());
    }
}
