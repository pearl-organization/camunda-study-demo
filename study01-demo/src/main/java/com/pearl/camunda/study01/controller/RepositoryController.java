package com.pearl.camunda.study01.controller;

import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.repository.DeploymentBuilder;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 */
@RequestMapping("/repository")
@RestController
public class RepositoryController {

    @Autowired
    private RepositoryService repositoryService;

    @PostMapping("/deploy")
    public String deploy(@RequestParam MultipartFile file) throws IOException {
        // 1. 构建部署对象
        DeploymentBuilder builder = repositoryService.createDeployment();
        builder.name("测试部署");
        InputStream inputStream = file.getInputStream();
        builder.addInputStream(file.getOriginalFilename(), inputStream);
        // 2.部署并返回ID
        Deployment deployment = builder.deploy();
        inputStream.close();
        return deployment.getId();
    }
}
