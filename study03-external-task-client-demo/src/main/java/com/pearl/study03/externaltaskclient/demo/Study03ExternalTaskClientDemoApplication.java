package com.pearl.study03.externaltaskclient.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Study03ExternalTaskClientDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Study03ExternalTaskClientDemoApplication.class, args);
    }

}
