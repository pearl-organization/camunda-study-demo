package com.pearl.study03.externaltaskclient.demo;

import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskHandler;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.camunda.bpm.engine.variable.Variables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/29
 */
@Configuration
@ExternalTaskSubscription("testTopic")
public class OaExternalTaskHandler implements ExternalTaskHandler {
    protected static Logger LOG = LoggerFactory.getLogger(OaExternalTaskHandler.class);
    /**
     *  处理器实现
     * @param externalTask 外部任务
     * @param externalTaskService 外部任务服务类
     */
    @Override
    public void execute(ExternalTask externalTask, ExternalTaskService externalTaskService) {
        // 外部任务信息
        System.out.println("任务ID：" + externalTask.getId());
        System.out.println("流程变量：" + externalTask.getAllVariables());
        System.out.println("主题名称：" + externalTask.getTopicName());
        System.out.println("业务标识：" + externalTask.getBusinessKey());
        System.out.println("WorkID：" + externalTask.getWorkerId());
        // 外部逻辑处理
        LOG.info("外部逻辑处理.....................");
        // 完成任务，并传递变量
        externalTaskService.complete(externalTask, Variables.putValueTyped("result",  Variables.booleanValue(true)));
    }
}
