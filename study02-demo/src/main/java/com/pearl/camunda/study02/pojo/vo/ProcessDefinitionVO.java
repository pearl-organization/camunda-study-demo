package com.pearl.camunda.study02.pojo.vo;

import cn.hutool.core.collection.CollUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.camunda.bpm.engine.repository.ProcessDefinition;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/9
 */
@Getter
@Setter
@Schema(description = "流程定义展示层对象")
public class ProcessDefinitionVO {

    @Schema(description = "ID")
    protected String id;

    @Schema(description = "KEY")
    protected String key;

    @Schema(description = "类别")
    protected String category;

    @Schema(description = "描述")
    protected String description;

    @Schema(description = "名称")
    protected String name;

    @Schema(description = "版本")
    protected int version;

    @Schema(description = "资源地址")
    protected String resource;

    @Schema(description = "部署ID")
    protected String deploymentId;

    @Schema(description = "简图")
    protected String diagram;

    @Schema(description = "是否暂停")
    protected boolean suspended;

    @Schema(description = "租户ID")
    protected String tenantId;

    @Schema(description = "版本标记")
    protected String versionTag;

    @Schema(description = "历史数据保留时间")
    protected Integer historyTimeToLive;

    @Schema(description = "是否开启任务列表")
    protected boolean isStartableInTasklist;

    public static ProcessDefinitionVO fromProcessDefinition(ProcessDefinition definition) {
        ProcessDefinitionVO vo = new ProcessDefinitionVO();
        vo.id = definition.getId();
        vo.key = definition.getKey();
        vo.category = definition.getCategory();
        vo.description = definition.getDescription();
        vo.name = definition.getName();
        vo.version = definition.getVersion();
        vo.resource = definition.getResourceName();
        vo.deploymentId = definition.getDeploymentId();
        vo.diagram = definition.getDiagramResourceName();
        vo.suspended = definition.isSuspended();
        vo.tenantId = definition.getTenantId();
        vo.versionTag = definition.getVersionTag();
        vo.historyTimeToLive = definition.getHistoryTimeToLive();
        vo.isStartableInTasklist = definition.isStartableInTasklist();
        return vo;
    }

    public static List<ProcessDefinitionVO> fromProcessDefinition(List<ProcessDefinition> processDefinitionList) {
        List<ProcessDefinitionVO> result = new ArrayList<>();
        if (CollUtil.isNotEmpty(processDefinitionList)) {
            for (ProcessDefinition processDefinition : processDefinitionList) {
                ProcessDefinitionVO vo = ProcessDefinitionVO.fromProcessDefinition(processDefinition);
                result.add(vo);
            }
        }
        return result;
    }
}
