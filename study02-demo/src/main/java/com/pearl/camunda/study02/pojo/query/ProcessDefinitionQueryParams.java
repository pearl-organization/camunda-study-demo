package com.pearl.camunda.study02.pojo.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/9
 */
@Getter
@Setter
@Schema(description = "流程定义查询参数")
public class ProcessDefinitionQueryParams {

    @Schema(description = "名称模糊查询")
    private String nameLike;
}
