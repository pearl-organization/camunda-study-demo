package com.pearl.camunda.study02.delegate.global;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/21
 */
@Slf4j
public class PluginGlobalTaskListener implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        log.info("PluginGlobalTaskListener.............");
    }
}
