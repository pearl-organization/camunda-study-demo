package com.pearl.camunda.study02.delegate;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Service;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/18
 */

@Slf4j
@Service("beanExecutionListener")
public class BeanExecutionListener implements ExecutionListener {

    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {
        log.info("事件名称：" + delegateExecution.getEventName());
    }
}
