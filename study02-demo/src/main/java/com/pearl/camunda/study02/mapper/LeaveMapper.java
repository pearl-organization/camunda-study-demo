package com.pearl.camunda.study02.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pearl.camunda.study02.entity.Leave;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pearl
 * @since 2023-07-18
 */
@Mapper
public interface LeaveMapper extends BaseMapper<Leave> {

}
