package com.pearl.camunda.study02.delegate.global;

import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.engine.impl.bpmn.behavior.UserTaskActivityBehavior;
import org.camunda.bpm.engine.impl.bpmn.parser.AbstractBpmnParseListener;
import org.camunda.bpm.engine.impl.pvm.delegate.ActivityBehavior;
import org.camunda.bpm.engine.impl.pvm.process.ActivityImpl;
import org.camunda.bpm.engine.impl.pvm.process.ScopeImpl;
import org.camunda.bpm.engine.impl.task.TaskDefinition;
import org.camunda.bpm.engine.impl.util.xml.Element;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/21
 */
public class PluginsBpmnParseListener extends AbstractBpmnParseListener {

    private static final TaskListener globalTaskListener = new PluginGlobalTaskListener();

    private static final ExecutionListener globalExecutionListener = new PluginGlobalExecutionListener();

    /**
     * @param userTaskElement 表示一个XML元素
     * @param scope           Bpmn范围
     * @param activity        活动
     */
    public void parseUserTask(Element userTaskElement, ScopeImpl scope, ActivityImpl activity) {
        activity.addListener(ExecutionListener.EVENTNAME_START, globalExecutionListener); // 添加执行监听
        UserTaskActivityBehavior behavior= (UserTaskActivityBehavior) activity.getActivityBehavior();
        TaskDefinition taskDefinition = behavior.getTaskDefinition();
        taskDefinition.addTaskListener(TaskListener.EVENTNAME_CREATE, globalTaskListener); // 添加任务监听

    }
}
