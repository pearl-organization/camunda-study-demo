package com.pearl.camunda.study02.delegate.global;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.spring.boot.starter.event.TaskEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/21
 */
@Slf4j
//@Component
public class GlobalTaskListener {

    //@EventListener/*(condition = "#delegateTask.eventName=='create'")*/
    public void onTaskEvent(DelegateTask delegateTask) {
        log.info("事件名称："+ delegateTask.getEventName());
        log.info("任务ID："+ delegateTask.getId());
        log.info("任务名称："+ delegateTask.getName());
        log.info("审批人："+delegateTask.getAssignee());

    }

/*    @EventListener
    public void onTaskEvent(TaskEvent taskEvent) {
        // 处理不可变的任务事件
    }*/

}
