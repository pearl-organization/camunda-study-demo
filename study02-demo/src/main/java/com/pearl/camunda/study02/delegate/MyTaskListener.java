package com.pearl.camunda.study02.delegate;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/18
 */
@Slf4j
public class MyTaskListener implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        // 1. 创建任务时，检查审批人是否为空，为空时，自动完成任务，跳到下一个节点
        String eventName = delegateTask.getEventName();
        String assignee = delegateTask.getAssignee();
        String taskId = delegateTask.getId();
        if (TaskListener.EVENTNAME_CREATE.equals(eventName) && StrUtil.isEmpty(assignee)) {
            TaskService taskService = delegateTask.getProcessEngineServices().getTaskService();
            taskService.complete(taskId);
        }
    }
}
