package com.pearl.camunda.study02;

import com.pearl.camunda.study02.delegate.BeanExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Study02DemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(Study02DemoApplication.class, args);
        BeanExecutionListener bean = run.getBean(BeanExecutionListener.class);
        Object beanExecutionListener = run.getBean("globalTaskListener");
        System.out.println(beanExecutionListener);

    }

}
