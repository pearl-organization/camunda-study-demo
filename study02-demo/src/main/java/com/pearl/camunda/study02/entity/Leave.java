package com.pearl.camunda.study02.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author pearl
 * @since 2023-07-18
 */
@Data
@TableName("sys_leave")
public class Leave implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId()
    private Long id;

    /**
     * 流程实例id
     */
    private String processInstanceId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 请假原因
     */
    private String reason;

    /**
     * 天数
     */
    private Integer day;

    /**
     * 申请人姓名
     */
    private String name;

    /**
     * 流程状态（0：申请中；1：审批中；2：审批通过；3：审批不通过）
     */
    private String status;

    /**
     * 提交日期
     */
    private LocalDateTime submitTime;

    /**
     * 创建日期
     */
    private LocalDateTime createTime;

    /**
     * 修改日期
     */
    private LocalDateTime updateTime;
}
