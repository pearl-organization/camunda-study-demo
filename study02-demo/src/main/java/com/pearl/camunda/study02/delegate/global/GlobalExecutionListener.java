package com.pearl.camunda.study02.delegate.global;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/21
 */
@Slf4j
@Component
public class GlobalExecutionListener {

    @EventListener
    public void onExecutionEvent(DelegateExecution delegateExecution) {
        log.info("事件名称："+ delegateExecution.getEventName());
        log.info("执行ID："+ delegateExecution.getId());
        log.info("活动："+ delegateExecution.getCurrentActivityName());
    }

/*    @EventListener
    public void onExecutionEvent(ExecutionEvent  taskEvent) {
        // 处理不可变的任务事件
    }*/

}
