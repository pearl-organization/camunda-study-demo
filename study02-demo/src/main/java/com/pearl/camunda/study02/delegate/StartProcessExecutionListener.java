package com.pearl.camunda.study02.delegate;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;

import java.util.Map;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/17
 */
@Slf4j
public class StartProcessExecutionListener implements ExecutionListener {

    public void notify(DelegateExecution execution) throws Exception {
        // 获取执行信息
        String eventName = execution.getEventName();
        String currentActivityName = execution.getCurrentActivityName();
        String businessKey = execution.getProcessBusinessKey();
        Map<String, Object> variables = execution.getVariables();

        log.info("事件名称："+eventName);
        log.info("活动名称："+currentActivityName);
        log.info("业务标识："+businessKey);
        log.info("流程变量："+variables);

        // 自定义业务逻辑
        log.info("自定义业务逻辑");
    }
}