package com.pearl.camunda.study02.delegate.global;

import org.camunda.bpm.engine.impl.bpmn.parser.BpmnParseListener;
import org.camunda.bpm.engine.impl.cfg.AbstractProcessEnginePlugin;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/21
 */

@Configuration
public class GlobalListenerPlugin extends AbstractProcessEnginePlugin {
    @Override
    public void preInit(ProcessEngineConfigurationImpl configuration) {
        List<BpmnParseListener> preParseListeners = configuration.getCustomPreBPMNParseListeners();
        preParseListeners.add(new PluginsBpmnParseListener());
        configuration.setCustomPreBPMNParseListeners(preParseListeners);
    }
}