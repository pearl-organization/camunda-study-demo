/*
 * Copyright Camunda Services GmbH and/or licensed to Camunda Services GmbH
 * under one or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information regarding copyright
 * ownership. Camunda licenses this file to you under the Apache License,
 * Version 2.0; you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pearl.camunda.study02.pojo.vo;

import cn.hutool.core.collection.CollUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.camunda.bpm.engine.BadUserRequestException;
import org.camunda.bpm.engine.form.CamundaFormRef;
import org.camunda.bpm.engine.task.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/9
 */
@Getter
@Setter
@Schema(description = "任务展示层对象")
public class TaskVO {

    @Schema(description = "ID")
    protected String id;

    @Schema(description = "名称")
    private String name;

    @Schema(description = "处理人")
    private String assignee;

    @Schema(description = "创建时间")
    private Date created;

    @Schema(description = "截止日期")
    private Date due;

    @Schema(description = "后续日期")
    private Date followUp;

    @Schema(description = "上次更新任务的日期，每个触发任务更新事件的操作都将更新此属性")
    private Date lastUpdated;

    @Schema(description = "任务的委派状态：PENDING、RESOLVED")
    private String delegationState;

    @Schema(description = "描述")
    private String description;

    @Schema(description = "执行ID")
    private String executionId;

    @Schema(description = "所有者ID")
    private String owner;

    @Schema(description = "父级任务ID")
    private String parentTaskId;

    @Schema(description = "任务优先级")
    private int priority;

    @Schema(description = "流程定义ID")
    private String processDefinitionId;

    @Schema(description = "流程实例ID")
    private String processInstanceId;

    @Schema(description = "任务KEY")
    private String taskDefinitionKey;

    @Schema(description = "CMMN执行ID")
    private String caseExecutionId;

    @Schema(description = "CMMN实例ID")
    private String caseInstanceId;

    @Schema(description = "CMMN定义ID")
    private String caseDefinitionId;

    @Schema(description = "任务是否属于已挂起的流程实例")
    private boolean suspended;

    @Schema(description = "表单KEY")
    private String formKey;

    @Schema(description = "表格特定版本")
    private CamundaFormRef camundaFormRef;

    @Schema(description = "租户ID")
    private String tenantId;

    public static TaskVO fromTask(Task task) {
        TaskVO vo = new TaskVO();
        vo.id = task.getId();
        vo.name = task.getName();
        vo.assignee = task.getAssignee();
        vo.created = task.getCreateTime();
        vo.lastUpdated = task.getLastUpdated();
        vo.due = task.getDueDate();
        vo.followUp = task.getFollowUpDate();
        if (task.getDelegationState() != null) {
            vo.delegationState = task.getDelegationState().toString();
        }
        vo.description = task.getDescription();
        vo.executionId = task.getExecutionId();
        vo.owner = task.getOwner();
        vo.parentTaskId = task.getParentTaskId();
        vo.priority = task.getPriority();
        vo.processDefinitionId = task.getProcessDefinitionId();
        vo.processInstanceId = task.getProcessInstanceId();
        vo.taskDefinitionKey = task.getTaskDefinitionKey();
        vo.caseDefinitionId = task.getCaseDefinitionId();
        vo.caseExecutionId = task.getCaseExecutionId();
        vo.caseInstanceId = task.getCaseInstanceId();
        vo.suspended = task.isSuspended();
        vo.tenantId = task.getTenantId();
        try {
            vo.formKey = task.getFormKey();
            vo.camundaFormRef = task.getCamundaFormRef();
        } catch (BadUserRequestException ignored) {
        }
        return vo;
    }

    public static List<TaskVO> fromTask(List<Task> taskList) {
        List<TaskVO> voList = new ArrayList<>();
        if (CollUtil.isNotEmpty(taskList)) {
            for (Task task : taskList) {
                TaskVO taskVO = TaskVO.fromTask(task);
                voList.add(taskVO);
            }
        }
        return voList;
    }
}
