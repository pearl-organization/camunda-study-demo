package com.pearl.camunda.study02.delegate;

import org.camunda.bpm.engine.impl.pvm.delegate.ActivityBehavior;
import org.camunda.bpm.engine.impl.pvm.delegate.ActivityExecution;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/17
 */
public class MyActivityExecution implements ActivityBehavior {

    @Override
    public void execute(ActivityExecution activityExecution) throws Exception {
        String businessKey = activityExecution.getBusinessKey();
    }
}
