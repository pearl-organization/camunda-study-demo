package com.pearl.camunda.study02.delegate;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/15
 */
@Slf4j
public class SendPhoneMsgJavaDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        // 获取变量
        String phone = (String) execution.getVariable("phone"); // 手机号
        String money = (String) execution.getVariable("money"); // 余额
        // 活动名称
        String currentActivityName = execution.getCurrentActivityName();
        // 业务标识
        String businessKey = execution.getProcessBusinessKey();
        // 模拟发送短信
        log.info("手机号："+phone);
        log.info("取款成功，您的银行卡余额为："+money);
    }
}
