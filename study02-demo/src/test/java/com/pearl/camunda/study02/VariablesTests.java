package com.pearl.camunda.study02;

import com.pearl.camunda.study02.pojo.param.Order;
import com.pearl.camunda.study02.query.TaskDTO;
import com.pearl.camunda.study02.query.TaskListService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.FileValue;
import org.camunda.bpm.engine.variable.value.StringValue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class VariablesTests {

    @Autowired
    TaskListService taskListService;

    @Test
    void testCustomQuery() {
        List<TaskDTO> tasksForRegion = taskListService.getTasksForRegion("aaa", "aaa");
    }


    @Autowired
    RuntimeService runtimeService;

    @Test
    @DisplayName("启动流程并绑定全局变量")
    void startProcessInstanceById() {
        String processDefinitionId = "Process_1hnr8wt:14:2235";
        Map<String, Object> variables = new HashMap<>();
        variables.put("name", "wangwu");
        variables.put("day", 3);
        ProcessInstance processInstance = runtimeService.startProcessInstanceById(processDefinitionId, variables);
        System.out.println("流程实例ID：" + processInstance.getId());
    }

    @Test
    @DisplayName("根据流程ID设置变量")
    void setVariableLocal() {
        String processInstanceId = "44721abd-3a74-11ee-bbef-00ff045b1bb6";
        runtimeService.setVariableLocal(processInstanceId, "local", "Hello Local");
    }

    @Test
    @DisplayName("类型值根据流程ID设置变量")
    void setVariable() {
        // 设置
        String processInstanceId = "0a050641-3b14-11ee-a33f-00ff045b1bb6";
        StringValue stringValue = Variables.stringValue("stringValue");
        runtimeService.setVariable(processInstanceId, "stringValue", stringValue);

        // 查询
        StringValue typedValue = runtimeService.getVariableTyped(processInstanceId, "stringValue");
        System.out.println(typedValue.getValue());

        // 多个类型值
        Order order = new Order();
        VariableMap variables =
                Variables.createVariables()
                        .putValueTyped("order", Variables.objectValue(order).create())
                        .putValueTyped("string", Variables.stringValue("a string value"))
                        .putValueTyped("stringTransient", Variables.stringValue("foobar", true));
        runtimeService.setVariablesLocal(processInstanceId, variables);
    }

    @Test
    @DisplayName("根据流程实例ID查询变量")
    void getVariables() {
        String processInstanceId = "f67364c6-3b1a-11ee-b958-00ff045b1bb6";
        Map<String, Object> variables = runtimeService.getVariables(processInstanceId);
        System.out.println(variables);
    }

    @Autowired
    TaskService taskService;

    @Test
    @DisplayName("查询流程实例中的变量")
    void getTaskList() {
        TaskQuery taskQuery = taskService.createTaskQuery();
        List<Task> taskList = taskQuery.processInstanceId("f67364c6-3b1a-11ee-b958-00ff045b1bb6").list();
        for (Task task : taskList) {
            System.out.println("任务ID：" + task.getId()); // 44721abd-3a74-11ee-bbef-00ff045b1bb6
        }
    }

    @Test
    @DisplayName("任务设置变量")
    void taskServiceSetVariable() {
        taskService.setVariableLocal("f675127d-3b1a-11ee-b958-00ff045b1bb6", "taskVariable", "Hello Task");
    }

    @Test
    @DisplayName("查询")
    void taskServiceGetVariables() {
        Map<String, Object> variables = taskService.getVariables("f675127d-3b1a-11ee-b958-00ff045b1bb6");
        System.out.println(variables);
    }

    @Test
    @DisplayName("文件变量")
    void fileVariables() {
        // 设置文件变量
        String processInstanceId = "447045f6-3a74-11ee-bbef-00ff045b1bb6";
        // 文件变量对象
        FileValue typedFileValue = Variables
                .fileValue("camunda.txt") // 文件名称
                .file(new File("\\C:\\Users\\Administrator\\Desktop\\camunda.txt")) // 文件地址
                .mimeType("text/plain") // MIME类型
                .encoding("UTF-8") // 字符集
                .create();
        runtimeService.setVariable(processInstanceId, "fileVariable", typedFileValue);

        // 获取文件变量
        runtimeService.getVariableTyped(processInstanceId, "fileVariable");
        FileValue retrievedTypedFileValue = runtimeService.getVariableTyped(processInstanceId, "fileVariable");
        InputStream fileContent = retrievedTypedFileValue.getValue(); // 文件二进制流
        String fileName = retrievedTypedFileValue.getFilename(); // 文件名称 "addresses.txt"
        String mimeType = retrievedTypedFileValue.getMimeType(); // MIME类型 "text/plain"
        String encoding = retrievedTypedFileValue.getEncoding(); // 字符集 "UTF-8"
        System.out.println("文件名称" + fileName);
        System.out.println("MIME类型" + mimeType);
        System.out.println("字符集" + encoding);
    }


}
