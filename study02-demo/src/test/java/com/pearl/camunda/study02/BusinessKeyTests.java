package com.pearl.camunda.study02;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.pearl.camunda.study02.entity.Leave;
import com.pearl.camunda.study02.mapper.LeaveMapper;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@SpringBootTest
class BusinessKeyTests {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    @Autowired
    LeaveMapper leaveMapper;

    @Test
    @DisplayName("业务系统创建请假单数据")
    void startLeave() {
        Leave leave = new Leave();
        leave.setUserId(123456L);
        leave.setName("郭达");
        leave.setReason("肚子痛");
        leave.setSubmitTime(LocalDateTime.now());
        leave.setDay(1);
        leave.setCreateTime(LocalDateTime.now());
        leave.setSubmitTime(LocalDateTime.now());
        leave.setUpdateTime(LocalDateTime.now());
        leaveMapper.insert(leave);
        System.out.println("请假表数据ID：" + leave.getId());
    }

    @Test
    @DisplayName("业务系统调用工作流启动流程")
    void startProcessInstanceByKey() {
        String processDefinitionKey = "flow_uclscxqj"; // 流程定义KEY
        // 启动流程流程参数
        Map<String, Object> variables = new HashMap<>(); // 变量
        ArrayList<String> objects = new ArrayList<>();
        objects.add("admin");
        variables.put("assigneeList", objects);
        String businessKey = "123456"; //  BusinessKey
        // 启动流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionKey, businessKey, variables);
        // 将返回的流程实例ID更新到业务表中
    }

    @Test
    @DisplayName("流程数据=》业务数据")
    void processInfo() {
        // 查询所有请假流程
        String processDefinitionKey = "Process_1hnr8wt"; // 流程定义KEY
        List<ProcessInstance> processInstances = runtimeService.createProcessInstanceQuery()
                .processDefinitionKey(processDefinitionKey)
                .list();
        // 循环所有的流程实例
        if (CollUtil.isNotEmpty(processInstances)){
            for (ProcessInstance instance : processInstances) {
                String instanceBusinessKey = instance.getBusinessKey(); // 业务KEY
                // 循环查询对应的请假信息
                if (StrUtil.isNotEmpty(instanceBusinessKey)){
                    Leave leave = leaveMapper.selectById(instanceBusinessKey);
                    System.out.println("请假人："+leave.getName());
                    System.out.println("原因："+leave.getReason());
                    System.out.println("请假天数："+leave.getDay());
                    // 查询所有待办
                    List<Task> tasks = taskService.createTaskQuery().processInstanceBusinessKey(instanceBusinessKey).list();
                    if (CollUtil.isNotEmpty(tasks)){
                        for (Task task : tasks) {
                            System.out.println("任务名称："+task.getName());
                            System.out.println("负责人："+task.getAssignee());
                        }
                    }
                }
            }
        }
    }

    @Test
    @DisplayName("业务数据=》流程数据")
    void businessKeyInfo() {
        // 请假单数据
        Leave leave = leaveMapper.selectById(1691645693631426561L);
        System.out.println("请假人："+leave.getName());
        System.out.println("原因："+leave.getReason());
        System.out.println("请假天数："+leave.getDay());
        // 查询对应的流程实例信息
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(leave.getProcessInstanceId())
                .singleResult();
        // 待办任务
        if (ObjectUtil.isNotNull(processInstance)){
            List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
            if (CollUtil.isNotEmpty(tasks)){
                for (Task task : tasks) {
                    System.out.println("任务名称："+task.getName());
                    System.out.println("负责人："+task.getAssignee());
                }
            }
        }
    }
}
