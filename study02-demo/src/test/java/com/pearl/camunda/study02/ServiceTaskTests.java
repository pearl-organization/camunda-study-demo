package com.pearl.camunda.study02;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.pearl.camunda.study02.entity.Leave;
import com.pearl.camunda.study02.mapper.LeaveMapper;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@SpringBootTest
class ServiceTaskTests {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    @Autowired
    LeaveMapper leaveMapper;

    @Test
    @DisplayName("业务系统创建请假单数据")
    void startLeave() {
        Leave leave = new Leave();
        leave.setUserId(123456L);
        leave.setName("郭达");
        leave.setReason("肚子痛");
        leave.setSubmitTime(LocalDateTime.now());
        leave.setDay(1);
        leave.setCreateTime(LocalDateTime.now());
        leave.setSubmitTime(LocalDateTime.now());
        leave.setUpdateTime(LocalDateTime.now());
        leaveMapper.insert(leave);
        System.out.println("请假表数据ID：" + leave.getId());
    }
}
