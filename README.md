# camunda-study-demo

#### 介绍
Camunda 教程源码
#### 模块和教程的对应关系

study01-demo：

- 【7】Spring Boot 集成 Camunda 7.19
- 【8】流程定义部署
- 【9】使用 Rest API 运行流程实例

study02-demo：
- 【11】使用 Java API 运行流程实例
- 【12】创建流程引擎
- 【13】流程引擎 API
- 【15】流程变量
- 【16】排他网关
- 【17】 BusinessKey
- 【18】服务任务
- 【19】表达式语言
- 【20】执行监听器
- 【21】任务监听器
- 【22】全局监听器

study03-demo：
- 【23】身份服务 IdentityService
- 【25】发送任务 & 接收任务
- 【26】连接器 Connector




