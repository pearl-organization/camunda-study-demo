package com.pearl.study03demo.delegate;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/24
 */
@Slf4j
@Component
public class SendTaskJavaDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        log.info("开始发送消息");

        // 1. 获取 RuntimeService
        RuntimeService runtimeService = delegateExecution.getProcessEngineServices().getRuntimeService();
        // 2. 创建消息
        runtimeService.createMessageCorrelation("Message_0gtfd6m") // 消息名称
                .correlate();
    }
}
