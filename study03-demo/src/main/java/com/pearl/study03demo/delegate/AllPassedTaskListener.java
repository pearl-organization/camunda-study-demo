package com.pearl.study03demo.delegate;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.engine.variable.value.IntegerValue;
import org.camunda.bpm.engine.variable.value.TypedValue;
import org.springframework.stereotype.Service;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/29
 */
@Service
@Slf4j
public class AllPassedTaskListener implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        String assignee = delegateTask.getAssignee();
        log.info("当前审批人："+assignee);
        Integer nrOfInstances = (Integer)delegateTask.getVariable("nrOfInstances");
        log.info("当前任务实例总数："+nrOfInstances);
        Integer nrOfCompletedInstances =  (Integer)delegateTask.getVariable("nrOfCompletedInstances");
        log.info("当前已审批完成的任务数："+nrOfCompletedInstances);
    }
}
