package com.pearl.study03demo.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.camunda.bpm.engine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 */
@Tag(name = "流程实例")
@RestController
@RequestMapping("/process")
public class ProcessController {

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private IdentityService identityService;
}
