package com.pearl.study03demo.controller;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/9
 */
@Getter
@Setter
@Schema(description = "启动流程参数")
public class StartProcessInstanceParams {

    @Schema(description = "变量")
    private Map<String, Object>  variables;

    @Schema(description = "流程定义key")
    private String key;
}
