package com.pearl.study03demo.connector;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author TD
 * @version 1.0
 * @date 2023/7/31
 */
@RestController
@RequestMapping("/connector")
public class ConnectorController {

    @GetMapping("/test/{id}")
    public User test(@PathVariable String id){
        System.out.println("ID:"+id);
        return new User("坤坤","123456");
    }
}
