package com.pearl.study03demo.connector;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/24
 */
@Data
@AllArgsConstructor
public class User {

    private  String username;

    private  String password;
}
