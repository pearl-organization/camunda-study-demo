package com.pearl.study03demo.connector;

import org.camunda.connect.plugin.impl.ConnectProcessEnginePlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author TD
 * @version 1.0
 * @date 2023/8/24
 */
@Configuration
public class EnginePluginConfig {

    @Bean
    ConnectProcessEnginePlugin connectProcessEnginePlugin() {
        return new ConnectProcessEnginePlugin();
    }
}
