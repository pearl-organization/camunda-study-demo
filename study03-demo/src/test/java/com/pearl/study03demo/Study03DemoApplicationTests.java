package com.pearl.study03demo;

import cn.hutool.core.collection.CollUtil;
import com.pearl.study03demo.common.R;
import com.pearl.study03demo.controller.StartProcessInstanceParams;
import com.pearl.study03demo.controller.TaskVO;
import io.swagger.v3.oas.annotations.Operation;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.camunda.bpm.engine.variable.Variables;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class Study03DemoApplicationTests {

    @Test
    void contextLoads() {
    }
    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private IdentityService identityService;

    @Autowired
    ExternalTaskService externalTaskService;


    @Test
    @DisplayName("启动流程：会签")
    void multipleUserTasks () {
        // 会签人集合
        List<String> assigneeList=new ArrayList<>();
        assigneeList.add("zhangsan");
        assigneeList.add("lisi");
        assigneeList.add("wangwu");
        Map<String, Object> variables = new HashMap<>();
        variables.put("assigneeList", assigneeList);
        // 启动流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("Process_09hbi8i", variables);
        System.out.println("流程实例ID：" + processInstance.getId());
    }

    @Test
    @DisplayName("消息事件启动流程")
    void startProcessInstanceByMessage() {
        String messageName = "Message_2b87nd0";
        Map<String, Object> variables = new HashMap<>();
        variables.put("name", "wangwu");
        variables.put("day", 3);
        ProcessInstance processInstance = runtimeService.startProcessInstanceByMessage(messageName, variables);
        System.out.println("流程实例ID：" + processInstance.getId());
    }

    @Test
    @DisplayName("信号事件启动流程")
    void signalEventReceived() {
        String messageName = "Signal_2q02g0o";
        Map<String, Object> variables = new HashMap<>();
        variables.put("name", "wangwu");
        variables.put("day", 3);
        runtimeService.signalEventReceived(messageName, variables);
    }

    @Test
    @DisplayName("发送一个信号")
    void sendSignal() {
        String messageName = "Signal_2q02g0o";
        runtimeService.signalEventReceived(messageName);
    }

    @Test
    @DisplayName("条件事件启动流程")
    void createConditionEvaluation() {
        List<ProcessInstance> instances = runtimeService
                .createConditionEvaluation()
                .setVariable("temperature", 24)
                .evaluateStartConditions();
    }



    @Test
    @DisplayName("完成外部任务")
    void completeExternalTask() {
        externalTaskService.complete("a2212587-4242-11ee-822e-00ff045b1bb6","spring-boot-client",new HashMap<>());
    }

    @Test
    @DisplayName("创建用户")
    void saveUser() {
        User user = identityService.newUser("userid");
        user.setFirstName("kunkun");
        user.setEmail("sds@qq.com");
        user.setFirstName("kun");
        user.setLastName("kun");
        user.setPassword("123456");
        identityService.saveUser(user);
    }

    @Test
    @DisplayName("启动流程")
    void startProcessInstanceById() {

        String processDefinitionKey = "Process_0ngsdws";
        Map<String, Object> variables = new HashMap<>();
        variables.put("name", "wangwu");
        variables.put("day", 3);
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionKey, variables);
        System.out.println("流程实例ID：" + processInstance.getId());
    }

    @Test
    @DisplayName("查询我的待办")
    void createTaskQuery() {
        TaskQuery taskQuery = taskService.createTaskQuery().taskAssignee("zhangsan");
        List<Task> taskList = taskQuery.list();
        if (CollUtil.isNotEmpty(taskList)){
            for (Task task : taskList) {
                System.out.println(task.getId());
                System.out.println(task.getName());
                System.out.println(task.getAssignee());
            }

        }
    }
}
